
package me.zlob.cache;

import java.io.*;
import java.time.*;
import java.util.*;

import org.slf4j.*;

import redis.clients.jedis.*;

import me.zlob.util.*;

import static me.zlob.util.Conditions.*;

public class RedisCache<Key, Value> extends AbstractCache<Key, Value> {

    private static final Logger log = LoggerFactory.getLogger(RedisCache.class);

    private static final Duration DEFAULT_EXPIRE = Duration.ofMinutes(10);

    private final JedisPool pool;

    private Duration defaultExpire;

    public RedisCache(JedisPool pool) {
        this(pool, "");
    }

    public RedisCache(JedisPool pool, String prefix) {
        this(pool, prefix, DEFAULT_EXPIRE);
    }

    public RedisCache(JedisPool pool, String prefix, Duration defaultExpire) {
        super(prefix);
        this.pool = checkNotNull(pool, "pool");
        this.defaultExpire = checkNotNull(defaultExpire, "defaultExpire");
    }

    @Override
    public Value get(Key key) {
        try (Jedis cache = getConnection()) {
            Value result = nullable(cache)
                .map(c -> c.get(getKey(key)))
                .map(this::deserialize).orElse(null);

            log.debug("get key('{}','{}') -> {}", key, getKey(key), result == null ? "miss" : "hit");

            return result;
        }
    }

    @Override
    public List<Value> getMulti(List<Key> keys) {
        try (Jedis cache = getConnection()) {
            List<String> objects = cache.mget(getKeys(keys));

            if (keys.size() != objects.size()) {
                log.debug("objects wrong size: expected {}, found {}", keys.size(), objects.size());
            }

            List<Value> result = new ArrayList<>(keys.size());
            for (int i = 1; i < objects.size(); i++) {
                Value object = nullable(objects.get(i))
                    .map(this::deserialize)
                    .orElse(null);
                result.add(object);
            }
            return result;
        }
    }

    @Override
    public boolean put(Key key, Value value) {
        return put(key, value, defaultExpire);
    }

    @Override
    public boolean put(Key key, Value value, Duration expire) {
        try (Jedis cache = getConnection()) {
            boolean result = nullable(cache)
                .map(c -> c.set(getKey(key), serialize(value)))
                .map(s -> existsAndEqual(s, "OK"))
                .orElse(false);

            if (result && cache != null && !expire.equals(NEVER_EXPIRE)) {
                int seconds = (int) expire.getSeconds();
                cache.expire(getKey(key), seconds);
            }

            log.debug("put key('{}', '{}') expire({}s)", key, getKey(key), expire.getSeconds());

            return result;
        }
    }

    @Override
    public boolean exists(Key key) {
        try (Jedis cache = getConnection()) {
            boolean result = nullable(cache)
                .map(c -> c.exists(getKey(key)))
                .orElse(false);

            log.debug("exists key('{}','{}') -> {}", key, getKey(key), result);

            return result;
        }
    }

    @Override
    public void expire(Key key, Duration expire) {
        try (Jedis cache = getConnection()) {
            if (cache == null) {
                return;
            }
            if (expire.equals(NEVER_EXPIRE)) {
                cache.persist(getKey(key));
            } else {
                int seconds = (int) expire.getSeconds();
                cache.expire(getKey(key), seconds);
            }
        }
    }

    @Override
    public boolean delete(Key key) {
        try (Jedis cache = getConnection()) {
            boolean result = nullable(cache)
                .map(c -> c.del(getKey(key)))
                .map(i -> i > 0)
                .orElse(false);

            log.debug("delete key('{}', '{}')", key, getKey(key));

            return result;
        }
    }

    private Jedis getConnection() {
        Jedis redis = null;
        try {
            redis = pool.getResource();
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        return redis;
    }

    private String serialize(Value object) {
        String result = "";
        byte[] bytes = getBytes(object);
        if (bytes != null && bytes.length > 0) {
            char[] chars = IO.bytesToChars(bytes);
            result = new String(chars);
        }
        return result;
    }

    private byte[] getBytes(Value object) {
        byte[] result = null;
        try (
            ByteArrayOutputStream bos = new ByteArrayOutputStream(1024);
            ObjectOutputStream oos = new ObjectOutputStream(bos)
        ) {
            oos.writeObject(object);
            result = bos.toByteArray();
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        return result;
    }

    private Value deserialize(String object) {
        char[] chars = object.toCharArray();
        byte[] bytes = IO.charsToBytes(chars);
        return getObject(bytes);
    }

    private Value getObject(byte[] bytes) {
        Value result = null;
        try (
            ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
            ObjectInputStream ois = new ObjectInputStream(bis)
        ) {
            //noinspection unchecked
            result = (Value) ois.readObject();
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        return result;
    }

    private String[] getKeys(List<Key> keys) {
        return keys.stream()
            .map(this::getKey)
            .toArray(String[]::new);
    }

}
