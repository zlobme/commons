
package me.zlob.cache;

import java.time.*;
import java.util.*;
import java.util.concurrent.*;

import static me.zlob.util.Conditions.*;

public class LocalCache<Key, Value> extends AbstractCache<Key, Value> {

    private final Map<String, Value> objects = new ConcurrentHashMap<>();

    private final Map<Key, Long> expires = new ConcurrentHashMap<>();

    private final ConcurrentLinkedQueue<Key> queue = new ConcurrentLinkedQueue<>();

    private final int threshold;

    public LocalCache() {
        this(10_000);
    }

    public LocalCache(int threshold) {
        super("cache");
        this.threshold = threshold;
    }

    @Override
    public Value get(Key key) {
        queue.remove(key);
        queue.add(key);
        return isExpired(key) ? null : objects.get(getKey(key));
    }

    @Override
    public boolean put(Key key, Value value) {
        return put(key, value, NEVER_EXPIRE);
    }

    @Override
    public boolean put(Key key, Value value, Duration expire) {
        String k = getKey(key);
        if (objects.containsKey(k)) {
            queue.remove(key);
        }

        shrinkQueue();
        queue.add(key);

        objects.put(k, value);
        expire(key, expire);
        return true;
    }

    @Override
    public boolean exists(Key key) {
        return objects.containsKey(getKey(key));
    }

    @Override
    public List<Value> getMulti(List<Key> keys) {
        List<Value> result = Collections.emptyList();
        if (existsNotEmpty(keys)) {
            result = new ArrayList<>();
            for (Key key : keys) {
                result.add(get(key));
            }
        }
        return result;
    }

    @Override
    public void expire(Key key, Duration expire) {
        if (expire.toMillis() > 0) {
            long time = getNow()
                .plus(expire)
                .toMillis();
            expires.put(key, time);
        } else {
            expires.remove(key);
        }
    }

    @Override
    public boolean delete(Key key) {
        objects.remove(getKey(key));
        expires.remove(key);
        queue.remove(key);
        return true;
    }

    private Duration getNow() {
        return Duration.ofNanos(System.nanoTime());
    }

    private void shrinkQueue() {
        while (queue.size() >= threshold) {
            Key key = queue.poll();
            if (key != null) {
                objects.remove(getKey(key));
            }
        }
    }

    private boolean isExpired(Key key) {
        boolean result = false;
        Long expire = expires.get(key);
        if (existsNotEmpty(expire)) {
            long time = getNow().toMillis();
            if (expire <= time) {
                objects.remove(getKey(key));
                expires.remove(key);
                result = true;
            }
        }
        return result;
    }

}
