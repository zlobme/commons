
package me.zlob.cache;

import java.time.*;
import java.util.*;

public interface Cache<Key, Value> {

    public static final Duration NEVER_EXPIRE = Duration.ZERO;

    public String getPrefix();

    public Cache setPrefix(String prefix);

    public boolean isShared();

    public Cache setShared(boolean shared);

    public boolean isUseHash();

    public Cache setUseHash(boolean useHash);

    public Value get(Key key);

    public List<Value> getMulti(List<Key> keys);

    public boolean put(Key key, Value value);

    public boolean put(Key key, Value value, Duration expire);

    public boolean exists(Key key);

    public void expire(Key key, Duration expire);

    public boolean delete(Key key);

}
