
package me.zlob.cache;

import lombok.*;
import lombok.experimental.*;

import static me.zlob.util.Conditions.*;
import static me.zlob.util.Util.*;

@Accessors(chain = true)
public abstract class AbstractCache<Key, Value> implements Cache<Key, Value> {

    @Getter
    @Setter
    private String prefix;

    @Getter
    @Setter
    private boolean useHash = false;

    @Getter
    @Setter
    private boolean shared = true;

    public AbstractCache(String prefix) {
        this.prefix = checkNotNull(prefix, "prefix");
    }

    protected String getKey(Key key) {
        String customPrefix = isNeedPrefix() ? prefix : "cache";
        String combinedKey = customPrefix + "_" + key.toString();
        return useHash ? md5(combinedKey) : combinedKey;
    }

    private boolean isNeedPrefix() {
        return !shared && existsNotEmpty(prefix);
    }

}
