
package me.zlob.nosql.mongo;

import java.io.*;

import org.bson.types.*;

public abstract class AbstractBean implements Serializable {

    private ObjectId id;

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public AbstractBean() {
        this.id = new ObjectId();
    }

    public AbstractBean(ObjectId id) {
        this.id = id;
    }

}
