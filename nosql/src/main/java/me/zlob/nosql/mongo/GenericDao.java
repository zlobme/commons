
package me.zlob.nosql.mongo;

import java.util.*;

import org.bson.types.*;

import me.zlob.collections.*;

import static me.zlob.util.Conditions.*;

/**
 * @author zlob
 */
public class GenericDao implements AutoCloseable {

    private final Map<String, AbstractDao<AbstractBean>> daos;

    public GenericDao(Map<String, AbstractDao<AbstractBean>> daos) {
        this.daos = checkNotNull(daos, "daos");
    }

    public AbstractBean get(ObjectId id, Class clazz) {
        String name = nullable(clazz)
            .map(Class::getSimpleName)
            .orElse(null);
        return get(id, name);
    }

    private AbstractBean get(ObjectId id, String clazz) {
        return nullable(clazz)
            .map(daos::get)
            .map(dao -> dao.get(id))
            .orElse(null);
    }

    public List<AbstractBean> get(Set<ObjectId> ids, Class clazz) {
        String name = nullable(clazz)
            .map(Class::getSimpleName)
            .orElse(null);
        return get(ids, name);
    }

    private List<AbstractBean> get(Set<ObjectId> ids, String clazz) {
        return nullable(clazz)
            .map(daos::get)
            .map(dao -> dao.get(ids))
            .orElse(null);
    }

    public Map<String, List<AbstractBean>> get(Batch<ObjectId> batch) {
        Map<String, List<AbstractBean>> result = new HashMap<>();
        for (String clazz : (Set<String>) batch.getClasses()) {
            List<AbstractBean> beans = nullable(daos.get(clazz))
                .map(dao -> dao.get(batch.getIds(clazz)))
                .orElse(Collections.emptyList());
            result.put(clazz, beans);
        }
        return result;
    }

    @Override
    public void close() {
        daos.values().forEach(AbstractDao::close);
    }

}
