
package me.zlob.nosql.mongo;

import java.util.*;
import java.util.stream.*;

import org.slf4j.*;

import com.mongodb.*;
import com.mongodb.bulk.BulkWriteResult;
import com.mongodb.client.*;
import com.mongodb.client.model.*;
import com.mongodb.client.result.*;
import org.bson.*;
import org.bson.types.*;

import me.zlob.util.*;

import static me.zlob.util.Conditions.*;

public abstract class AbstractDao<Type extends AbstractBean> implements AutoCloseable {

    private static final Logger log = LoggerFactory.getLogger(AbstractDao.class);

    private final MongoClient client;

    private final String db;

    private final String collection;

    public AbstractDao(MongoClient client, String db, String collection) {
        this.client = checkNotNull(client, "client");
        this.db = checkArgNotEmpty(db, "db");
        this.collection = checkArgNotEmpty(collection, "collection");
    }

    protected MongoDatabase getDb() {
        return client.getDatabase(db);
    }

    protected MongoCollection<Document> getCollection() {
        return getDb().getCollection(collection);
    }

    protected MongoCollection<Document> getCollection(String collection) {
        return getDb().getCollection(collection);
    }

    protected abstract Type convert(Document doc);

    protected abstract Document convert(Type bean);

    protected List<Type> convertDocs(List<Document> docs) {
        return docs.stream()
            .map(this::convert)
            .collect(Collectors.toList());
    }

    protected List<Document> convertBeans(List<Type> beans) {
        return beans.stream()
            .map(this::convert)
            .collect(Collectors.toList());
    }

    protected long getNextUid(String name) {
        MongoCollection counters = getCollection("counters");
        Document query = new Document("_id", name);
        Document update = new Document("$inc", new Document("uid", 1));

        FindOneAndUpdateOptions options = new FindOneAndUpdateOptions()
            .returnDocument(ReturnDocument.BEFORE)
            .upsert(true);

        Document response = (Document) counters.findOneAndUpdate(query, update, options);
        return nullable(response)
            .map(r -> (Number) r.get("uid"))
            .map(Number::longValue).orElse(0L);
    }

    public ObjectId put(Type bean) {
        try {
            getCollection().insertOne(convert(bean));
        } catch (MongoException ex) {
            log.error(ex.getMessage(), ex);
        }
        return bean.getId();
    }

    public boolean put(List<Type> beans) {
        List<WriteModel<Document>> documents = beans.stream()
            .map(this::convert)
            .map(InsertOneModel::new)
            .collect(Collectors.toList());

        BulkWriteOptions options = new BulkWriteOptions().ordered(false);
        BulkWriteResult bulkWrite = getCollection().bulkWrite(documents, options);

        return nullable(bulkWrite)
            .map(BulkWriteResult::getInsertedCount)
            .map(i -> i == beans.size())
            .orElse(false);
    }

    public boolean update(Type bean) {
        return update(bean, false);
    }

    public boolean update(Type bean, boolean upsert) {
        Document query = new Document("_id", bean.getId());

        UpdateOptions options = new UpdateOptions().upsert(upsert);
        UpdateResult updateOne = getCollection().updateOne(query, convert(bean), options);

        return nullable(updateOne)
            .map(UpdateResult::getModifiedCount)
            .map(i -> i == 1)
            .orElse(false);
    }

    public Type get(ObjectId id) {
        Object result = getCollection().find(new Document("_id", id)).first();
        return nullable(result)
            .map(d -> (Document) d)
            .map(this::convert).orElse(null);
    }

    public List<Type> get(Set<ObjectId> ids) {
        Document in = new Document("$in", ids);
        Document query = new Document("_id", in);
        List<Document> docs = getCollection().find(query).into(new ArrayList<>());
        return docs.stream()
            .map(this::convert)
            .collect(Collectors.toList());
    }

    public List<Type> get(Page page) {
        return get(new Document(), page);
    }

    public List<Type> get(Document query, Page page) {
        Collection<Document> docs = getCollection().find(query)
            .skip(page.offset())
            .limit(page.getOnPage())
            .into(new ArrayList<>());
        //page.setCount(docs.size()); wrong count
        return docs.stream()
            .map(this::convert)
            .collect(Collectors.toList());
    }

    public List<Type> getAll() {
        List<Document> docs = getCollection().find().into(new ArrayList<>());
        return docs.stream()
            .map(this::convert)
            .collect(Collectors.toList());
    }

    public void delete(Type bean) {
        delete(bean.getId());
    }

    public void delete(ObjectId id) {
        try {
            getCollection().deleteOne(new Document("_id", id));
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
    }

    public void deleteAll() {
        if ("test".equals(db)) {
            try {
                getCollection().deleteOne(new Document());
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
            }
        }
    }

    public long count() {
        return getCollection().count();
    }

    public List<Type> sort(List<Type> items, Comparator<Type> comparator) {
        return items.stream().sorted(comparator).collect(Collectors.toList());
    }

    @Override
    public void close() {
        try {
            if (client != null) {
                client.close();
            }
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
    }

}
