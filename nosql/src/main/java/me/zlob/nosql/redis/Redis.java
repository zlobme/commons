
package me.zlob.nosql.redis;

import java.util.*;

import static java.util.Collections.*;

import org.slf4j.*;

import redis.clients.jedis.BinaryClient.*;
import redis.clients.jedis.*;
import redis.clients.jedis.params.geo.*;
import redis.clients.jedis.params.sortedset.*;

import static me.zlob.util.Conditions.*;

public class Redis implements JedisCommands {

    private static final Logger log = LoggerFactory.getLogger(Redis.class);

    private final JedisPool pool;

    public Redis(JedisPool pool) {
        this.pool = checkNotNull(pool, "pool");
    }

    public Jedis getConnection() {
        Jedis redis = null;
        try {
            redis = pool.getResource();
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        return redis;
    }

    public void releaseConnection(Jedis redis) {
        if (redis != null) {
            redis.close();
        }
    }

    public Long zunionstore(String dstkey, ZParams params, String... sets) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.zunionstore(dstkey, params, sets);
        }
    }

    public Long zunionstore(String dstkey, String... sets) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.zunionstore(dstkey, sets);
        }
    }

    @Override
    public Double zscore(String key, String member) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0.0D : jedis.zscore(key, member);
        }
    }

    @Override
    public Long zrevrank(String key, String member) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.zrevrank(key, member);
        }
    }

    @Override
    public Set<Tuple> zrevrangeWithScores(String key, long start, long end) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptySet() : jedis.zrevrangeWithScores(key, start, end);
        }
    }

    @Override
    public Set<String> zrevrange(String key, long start, long end) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptySet() : jedis.zrevrange(key, start, end);
        }
    }

    @Override
    public Long zremrangeByScore(String key, double start, double end) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.zremrangeByScore(key, start, end);
        }
    }

    @Override
    public Long zremrangeByRank(String key, long start, long end) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.zremrangeByRank(key, start, end);
        }
    }

    @Override
    public Long zrem(String key, String... member) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.zrem(key, member);
        }
    }

    @Override
    public Long zrank(String key, String member) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.zrank(key, member);
        }
    }

    @Override
    public Set<Tuple> zrangeWithScores(String key, long start, long end) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptySet() : jedis.zrangeWithScores(key, start, end);
        }
    }

    @Override
    public Set<Tuple> zrangeByScoreWithScores(String key, double min, double max, int offset, int count) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptySet() : jedis.zrangeByScoreWithScores(key, min, max, offset, count);
        }
    }

    @Override
    public Set<Tuple> zrangeByScoreWithScores(String key, double min, double max) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptySet() : jedis.zrangeByScoreWithScores(key, min, max);
        }
    }

    @Override
    public Set<String> zrangeByScore(String key, double min, double max, int offset, int count) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptySet() : jedis.zrangeByScore(key, min, max, offset, count);
        }
    }

    @Override
    public Set<String> zrangeByScore(String key, String min, String max) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptySet() : jedis.zrangeByScore(key, min, max);
        }
    }

    @Override
    public Set<String> zrangeByScore(String key, double min, double max) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptySet() : jedis.zrangeByScore(key, min, max);
        }
    }

    @Override
    public Set<String> zrange(String key, long start, long end) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptySet() : jedis.zrange(key, start, end);
        }
    }

    public Long zinterstore(String dstkey, ZParams params, String... sets) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.zinterstore(dstkey, params, sets);
        }
    }

    public Long zinterstore(String dstkey, String... sets) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.zinterstore(dstkey, sets);
        }
    }

    @Override
    public Double zincrby(String key, double score, String member) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0.0D : jedis.zincrby(key, score, member);
        }
    }

    @Override
    public Long zcount(String key, double min, double max) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.zcount(key, min, max);
        }
    }

    @Override
    public Long zcard(String key) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.zcard(key);
        }
    }

    @Override
    public Long zadd(String key, double score, String member) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.zadd(key, score, member);
        }
    }

    public String watch(String... keys) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? "" : jedis.watch(keys);
        }
    }

    @Override
    public String type(String key) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? "" : jedis.type(key);
        }
    }

    @Override
    public Long ttl(String key) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.ttl(key);
        }
    }

    public Long sunionstore(String dstkey, String... keys) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.sunionstore(dstkey, keys);
        }
    }

    public Set<String> sunion(String... keys) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptySet() : jedis.sunion(keys);
        }
    }

    @Override
    public String substr(String key, int start, int end) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? "" : jedis.substr(key, start, end);
        }
    }

    public void subscribe(JedisPubSub jedisPubSub, String... channels) {
        try (Jedis jedis = getConnection()) {
            nullable(jedis).ifPresent(j -> j.subscribe(jedisPubSub, channels));
        }
    }

    @Override
    public Long strlen(String key) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.strlen(key);
        }
    }

    @Override
    public Long srem(String key, String... member) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.srem(key, member);
        }
    }

    @Override
    public String srandmember(String key) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? "" : jedis.srandmember(key);
        }
    }

    @Override
    public String spop(String key) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? "" : jedis.spop(key);
        }
    }

    public Long sort(String key, String dstkey) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.sort(key, dstkey);
        }
    }

    public Long sort(String key, SortingParams sortingParameters, String dstkey) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.sort(key, sortingParameters, dstkey);
        }
    }

    @Override
    public List<String> sort(String key, SortingParams sortingParameters) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptyList() : jedis.sort(key, sortingParameters);
        }
    }

    @Override
    public List<String> sort(String key) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptyList() : jedis.sort(key);
        }
    }

    public Long smove(String srckey, String dstkey, String member) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.smove(srckey, dstkey, member);
        }
    }

    @Override
    public Set<String> smembers(String key) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptySet() : jedis.smembers(key);
        }
    }

    @Override
    public Boolean sismember(String key, String member) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? false : jedis.sismember(key, member);
        }
    }

    public Long sinterstore(String dstkey, String... keys) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.sinterstore(dstkey, keys);
        }
    }

    public Set<String> sinter(String... keys) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptySet() : jedis.sinter(keys);
        }
    }

    @Override
    public Long setnx(String key, String value) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.setnx(key, value);
        }
    }

    @Override
    public String setex(String key, int seconds, String value) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? "" : jedis.setex(key, seconds, value);
        }
    }

    @Override
    public Boolean setbit(String key, long offset, boolean value) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? false : jedis.setbit(key, offset, true);
        }
    }

    @Override
    public String set(String key, String value) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? "" : jedis.set(key, value);
        }
    }

    public String select(int index) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? "" : jedis.select(index);
        }
    }

    public Long sdiffstore(String dstkey, String... keys) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.sdiffstore(dstkey, keys);
        }
    }

    public Set<String> sdiff(String... keys) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptySet() : jedis.sdiff(keys);
        }
    }

    @Override
    public Long scard(String key) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.scard(key);
        }
    }

    @Override
    public Long sadd(String key, String... member) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.sadd(key, member);
        }
    }

    public Long rpushx(String key, String string) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.rpushx(key, string);
        }
    }

    @Override
    public Long rpush(String key, String... string) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.rpush(key, string);
        }
    }

    public String rpoplpush(String srckey, String dstkey) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? "" : jedis.rpoplpush(srckey, dstkey);
        }
    }

    @Override
    public String rpop(String key) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? "" : jedis.rpop(key);
        }
    }

    public Long renamenx(String oldkey, String newkey) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.renamenx(oldkey, newkey);
        }
    }

    public String rename(String oldkey, String newkey) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? "" : jedis.rename(oldkey, newkey);
        }
    }

    public String randomKey() {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? "" : jedis.randomKey();
        }
    }

    public Long publish(String channel, String message) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.publish(channel, message);
        }
    }

    public void psubscribe(JedisPubSub jedisPubSub, String... patterns) {
        try (Jedis jedis = getConnection()) {
            nullable(jedis).ifPresent(j -> j.psubscribe(jedisPubSub, patterns));
        }
    }

    @Override
    public Long persist(String key) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.persist(key);
        }
    }

    public List<Object> multi(TransactionBlock jedisTransaction) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptyList() : jedis.multi(jedisTransaction);
        }
    }

    public Transaction multi() {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? null : jedis.multi();
        }
    }

    public Long msetnx(String... keysvalues) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.msetnx(keysvalues);
        }
    }

    public String mset(String... keysvalues) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? "" : jedis.mset(keysvalues);
        }
    }

    @Override
    public Long move(String key, int dbIndex) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.move(key, dbIndex);
        }
    }

    public List<String> mget(String... keys) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptyList() : jedis.mget(keys);
        }
    }

    public String ltrim(String key, int start, int end) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? "" : jedis.ltrim(key, start, end);
        }
    }

    public String lset(String key, int index, String value) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? "" : jedis.lset(key, index, value);
        }
    }

    public Long lrem(String key, int count, String value) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.lrem(key, count, value);
        }
    }

    public List<String> lrange(String key, int start, int end) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptyList() : jedis.lrange(key, start, end);
        }
    }

    public Long lpushx(String key, String string) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.lpushx(key, string);
        }
    }

    @Override
    public Long lpush(String key, String... string) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.lpush(key, string);
        }
    }

    @Override
    public String lpop(String key) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? "" : jedis.lpop(key);
        }
    }

    @Override
    public Long llen(String key) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.llen(key);
        }
    }

    @Override
    public Long linsert(String key, LIST_POSITION where, String pivot, String value) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.linsert(key, where, pivot, value);
        }
    }

    public String lindex(String key, int index) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? "" : jedis.lindex(key, index);
        }
    }

    public Set<String> keys(String pattern) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptySet() : jedis.keys(pattern);
        }
    }

    @Override
    public Long incrBy(String key, long integer) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.incrBy(key, integer);
        }
    }

    @Override
    public Long incr(String key) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.incr(key);
        }
    }

    @Override
    public List<String> hvals(String key) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptyList() : jedis.hvals(key);
        }
    }

    @Override
    public Long hsetnx(String key, String field, String value) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.hsetnx(key, field, value);
        }
    }

    @Override
    public Long hset(String key, String field, String value) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.hset(key, field, value);
        }
    }

    @Override
    public String hmset(String key, Map<String, String> hash) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? "" : jedis.hmset(key, hash);
        }
    }

    @Override
    public List<String> hmget(String key, String... fields) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptyList() : jedis.hmget(key, fields);
        }
    }

    @Override
    public Long hlen(String key) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.hlen(key);
        }
    }

    @Override
    public Set<String> hkeys(String key) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptySet() : jedis.hkeys(key);
        }
    }

    @Override
    public Long hincrBy(String key, String field, long value) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.hincrBy(key, field, value);
        }
    }

    @Override
    public Map<String, String> hgetAll(String key) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptyMap() : jedis.hgetAll(key);
        }
    }

    @Override
    public String hget(String key, String field) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? "" : jedis.hget(key, field);
        }
    }

    @Override
    public Boolean hexists(String key, String field) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? false : jedis.hexists(key, field);
        }
    }

    @Override
    public Long hdel(String key, String... field) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.hdel(key, field);
        }
    }

    @Override
    public Boolean getbit(String key, long offset) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? false : jedis.getbit(key, offset);
        }
    }

    @Override
    public String getSet(String key, String value) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? "" : jedis.getSet(key, value);
        }
    }

    @Override
    public String get(String key) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? "" : jedis.get(key);
        }
    }

    public String flushDB() {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? "" : jedis.flushDB();
        }
    }

    public String flushAll() {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? "" : jedis.flushAll();
        }
    }

    @Override
    public Long expireAt(String key, long unixTime) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.expireAt(key, unixTime);
        }
    }

    @Override
    public Long expire(String key, int seconds) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.expire(key, seconds);
        }
    }

    @Override
    public Boolean exists(String key) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? false : jedis.exists(key);
        }
    }

    @Override
    public String echo(String string) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? "" : jedis.echo(string);
        }
    }

    public Long del(String... keys) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.del(keys);
        }
    }

    @Override
    public Long decrBy(String key, long integer) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.decrBy(key, integer);
        }
    }

    @Override
    public Long decr(String key) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.decr(key);
        }
    }

    public Long dbSize() {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.dbSize();
        }
    }

    public String brpoplpush(String source, String destination, int timeout) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? "" : jedis.brpoplpush(source, destination, timeout);
        }
    }

    public List<String> brpop(int timeout, String... keys) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptyList() : jedis.brpop(timeout, keys);
        }
    }

    public List<String> blpop(int timeout, String... keys) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptyList() : jedis.blpop(timeout, keys);
        }
    }

    public String auth(String password) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? "" : jedis.auth(password);
        }
    }

    @Override
    public Long append(String key, String value) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.append(key, value);
        }
    }

    @Override
    public Long setrange(String key, long offset, String value) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.setrange(key, offset, value);
        }
    }

    @Override
    public String getrange(String key, long startOffset, long endOffset) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? "" : jedis.getrange(key, startOffset, endOffset);
        }
    }

    @Override
    public List<String> lrange(String key, long start, long end) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptyList() : jedis.lrange(key, start, end);
        }
    }

    @Override
    public String ltrim(String key, long start, long end) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? "" : jedis.ltrim(key, start, end);
        }
    }

    @Override
    public String lindex(String key, long index) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? "" : jedis.lindex(key, index);
        }
    }

    @Override
    public String lset(String key, long index, String value) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? "" : jedis.lset(key, index, value);
        }
    }

    @Override
    public Long lrem(String key, long count, String value) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.lrem(key, count, value);
        }
    }

    @Override
    public Set<String> zrevrangeByScore(String key, double max, double min) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptySet() : jedis.zrevrangeByScore(key, max, min);
        }
    }

    @Override
    public Set<String> zrevrangeByScore(String key, double max, double min, int offset, int count) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptySet() : jedis.zrevrangeByScore(key, max, min, offset, count);
        }
    }

    @Override
    public Set<Tuple> zrevrangeByScoreWithScores(String key, double max, double min) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptySet() : jedis.zrevrangeByScoreWithScores(key, max, min);
        }
    }

    @Override
    public Set<Tuple> zrevrangeByScoreWithScores(String key, double max, double min, int offset, int count) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptySet() : jedis.zrevrangeByScoreWithScores(key, max, min, offset, count);
        }
    }

    @Override
    public Boolean setbit(String key, long offset, String value) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? false : jedis.setbit(key, offset, value);
        }
    }

    @Override
    public Long zadd(String key, Map<String, Double> scoreMembers) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.zadd(key, scoreMembers);
        }
    }

    @Override
    public Long zcount(String key, String min, String max) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.zcount(key, min, max);
        }
    }

    @Override
    public Set<String> zrevrangeByScore(String key, String max, String min) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptySet() : jedis.zrevrangeByScore(key, max, min);
        }
    }

    @Override
    public Set<String> zrangeByScore(String key, String min, String max, int offset, int count) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptySet() : jedis.zrangeByScore(key, max, min, offset, count);
        }
    }

    @Override
    public Set<String> zrevrangeByScore(String key, String max, String min, int offset, int count) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptySet() : jedis.zrevrangeByScore(key, max, min, offset, count);
        }
    }

    @Override
    public Set<Tuple> zrangeByScoreWithScores(String key, String min, String max) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptySet() : jedis.zrangeByScoreWithScores(key, max, min);
        }
    }

    @Override
    public Set<Tuple> zrevrangeByScoreWithScores(String key, String max, String min) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptySet() : jedis.zrevrangeByScoreWithScores(key, max, min);
        }
    }

    @Override
    public Set<Tuple> zrangeByScoreWithScores(String key, String min, String max, int offset, int count) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptySet() : jedis.zrangeByScoreWithScores(key, max, min, offset, count);
        }
    }

    @Override
    public Set<Tuple> zrevrangeByScoreWithScores(String key, String max, String min, int offset, int count) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptySet() : jedis.zrevrangeByScoreWithScores(key, max, min, offset, count);
        }
    }

    @Override
    public Long zremrangeByScore(String key, String start, String end) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.zremrangeByScore(key, start, end);
        }
    }

    @Override
    public Long lpushx(String key, String... string) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.lpushx(key, string);
        }
    }

    @Override
    public Long rpushx(String key, String... string) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.rpushx(key, string);
        }
    }

    @Override
    public List<String> blpop(String arg) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptyList() : jedis.blpop(arg);
        }
    }

    @Override
    public List<String> brpop(String arg) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptyList() : jedis.brpop(arg);
        }
    }

    @Override
    public Long del(String key) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.del(key);
        }
    }

    @Override
    public Long bitcount(String key) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.bitcount(key);
        }
    }

    @Override
    public Long bitcount(String key, long start, long end) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.bitcount(key, start, end);
        }
    }

    @Override
    public String set(String key, String value, String nxxx, String expx, long time) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? "" : jedis.set(key, value, nxxx, expx, time);
        }
    }

    @Override
    public List<String> srandmember(String key, int count) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptyList() : jedis.srandmember(key, count);
        }
    }

    @Override
    public Long zlexcount(String key, String min, String max) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.zlexcount(key, min, max);
        }
    }

    @Override
    public Set<String> zrangeByLex(String key, String min, String max) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptySet() : jedis.zrangeByLex(key, min, max);
        }
    }

    @Override
    public Set<String> zrangeByLex(String key, String min, String max, int offset, int count) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptySet() : jedis.zrangeByLex(key, min, max, offset, count);
        }
    }

    @Override
    public Long zremrangeByLex(String key, String min, String max) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.zremrangeByLex(key, min, max);
        }
    }

    @Override
    public List<String> blpop(int timeout, String key) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptyList() : jedis.blpop(timeout, key);
        }
    }

    @Override
    public List<String> brpop(int timeout, String key) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptyList() : jedis.brpop(timeout, key);
        }
    }

    @Override
    public Long pfadd(String key, String... elements) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.pfadd(key, elements);
        }
    }

    @Override
    public long pfcount(String key) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.pfcount(key);
        }
    }

    @Override
    public ScanResult<Map.Entry<String, String>> hscan(String key, int cursor) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? null : jedis.hscan(key, cursor);
        }
    }

    @Override
    public ScanResult<String> sscan(String key, int cursor) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? null : jedis.sscan(key, cursor);
        }
    }

    @Override
    public ScanResult<Tuple> zscan(String key, int cursor) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? null : jedis.zscan(key, cursor);
        }
    }

    @Override
    public ScanResult<Map.Entry<String, String>> hscan(String key, String cursor) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? null : jedis.hscan(key, cursor);
        }
    }

    @Override
    public ScanResult<String> sscan(String key, String cursor) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? null : jedis.sscan(key, cursor);
        }
    }

    @Override
    public ScanResult<Tuple> zscan(String key, String cursor) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? null : jedis.zscan(key, cursor);
        }
    }

    @Override
    public Long pexpire(String key, long milliseconds) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.pexpire(key, milliseconds);
        }
    }

    @Override
    public Long pexpireAt(String key, long millisecondsTimestamp) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.pexpireAt(key, millisecondsTimestamp);
        }
    }

    @Override
    public String set(String key, String value, String nxxx) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? "" : jedis.set(key, value, nxxx);
        }
    }

    @Override
    public Long pttl(String key) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.pttl(key);
        }
    }

    @Override
    public String psetex(String key, long milliseconds, String value) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? "" : jedis.psetex(key, milliseconds, value);
        }
    }

    @Override
    public Double incrByFloat(String key, double value) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0.0D : jedis.incrByFloat(key, value);
        }
    }

    @Override
    public Double hincrByFloat(String key, String field, double value) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0.0D : jedis.hincrByFloat(key, field, value);
        }
    }

    @Override
    public Set<String> spop(String key, long count) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptySet() : jedis.spop(key, count);
        }
    }

    @Override
    public Long zadd(String key, double score, String member, ZAddParams params) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.zadd(key, score, member, params);
        }
    }

    @Override
    public Long zadd(String key, Map<String, Double> scoreMembers, ZAddParams params) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.zadd(key, scoreMembers, params);
        }
    }

    @Override
    public Double zincrby(String key, double score, String member, ZIncrByParams params) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0.0D : jedis.zincrby(key, score, member, params);
        }
    }

    @Override
    public Set<String> zrevrangeByLex(String key, String max, String min) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptySet() : jedis.zrevrangeByLex(key, max, min);
        }
    }

    @Override
    public Set<String> zrevrangeByLex(String key, String max, String min, int offset, int count) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptySet() : jedis.zrevrangeByLex(key, max, min, offset, count);
        }
    }

    @Override
    public Long bitpos(String key, boolean value) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.bitpos(key, value);
        }
    }

    @Override
    public Long bitpos(String key, boolean value, BitPosParams params) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.bitpos(key, value, params);
        }
    }

    @Override
    public ScanResult<Map.Entry<String, String>> hscan(String key, String cursor, ScanParams params) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? null : jedis.hscan(key, cursor, params);
        }
    }

    @Override
    public ScanResult<String> sscan(String key, String cursor, ScanParams params) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? null : jedis.sscan(key, cursor, params);
        }
    }

    @Override
    public ScanResult<Tuple> zscan(String key, String cursor, ScanParams params) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? null : jedis.zscan(key, cursor, params);
        }
    }

    @Override
    public Long geoadd(String key, double longitude, double latitude, String member) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.geoadd(key, longitude, latitude, member);
        }
    }

    @Override
    public Long geoadd(String key, Map<String, GeoCoordinate> memberCoordinateMap) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0L : jedis.geoadd(key, memberCoordinateMap);
        }
    }

    @Override
    public Double geodist(String key, String member1, String member2) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0.0D : jedis.geodist(key, member1, member2);
        }
    }

    @Override
    public Double geodist(String key, String member1, String member2, GeoUnit unit) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? 0.0D : jedis.geodist(key, member1, member2, unit);
        }
    }

    @Override
    public List<String> geohash(String key, String... members) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptyList() : jedis.geohash(key, members);
        }
    }

    @Override
    public List<GeoCoordinate> geopos(String key, String... members) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptyList() : jedis.geopos(key, members);
        }
    }

    @Override
    public List<GeoRadiusResponse> georadius(String key,
                                             double longitude, double latitude,
                                             double radius, GeoUnit unit) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptyList() : jedis.georadius(key, longitude, latitude, radius, unit);
        }
    }

    @Override
    public List<GeoRadiusResponse> georadius(String key,
                                             double longitude, double latitude,
                                             double radius, GeoUnit unit, GeoRadiusParam param) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptyList() : jedis.georadius(key, longitude, latitude, radius, unit, param);
        }
    }

    @Override
    public List<GeoRadiusResponse> georadiusByMember(String key, String member, double radius, GeoUnit unit) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptyList() : jedis.georadiusByMember(key, member, radius, unit);
        }
    }

    @Override
    public List<GeoRadiusResponse> georadiusByMember(String key, String member,
                                                     double radius, GeoUnit unit, GeoRadiusParam param) {
        try (Jedis jedis = getConnection()) {
            return jedis == null ? emptyList() : jedis.georadiusByMember(key, member, radius, unit, param);
        }
    }

}
