
package me.zlob.cache;

import java.time.*;
import java.util.*;

import redis.clients.jedis.*;

import org.junit.*;
import static org.junit.Assert.*;

import me.zlob.nosql.redis.*;

import static me.zlob.properties.PropertiesProcessors.*;
import static me.zlob.util.Util.*;

public class RedisCacheIT {

    private final String resource = "me/zlob/cache/redis.properties";

    private JedisPool pool;

    private String keysPrefix;

    private Map<String, String> properties =
        fromClasspath(resource)
            .joinWith(fromEnvironment()
                .andThen(filterByKey("zcom.redis"))
                .andThen(replaceKeyPrefix("zcom.redis", "redis")))
            .loadAsMap();

    private RedisCache getCache() {
        return new RedisCache(pool, keysPrefix);
    }

    @Before
    public void init() {
        String host = properties.get("redis.host");
        int port = parseInt(properties.get("redis.port"));
        pool = new JedisPool(host, port);
        keysPrefix = properties.get("redis.prefix");
        new Redis(pool).flushDB();
    }

    @Test
    public void shared_key_gen_with_prefix_test() {
        RedisCache cache = getCache();

        cache.setUseHash(false);
        cache.setShared(true);
        cache.put("key1", 1, Duration.ofSeconds(1));

        assertEquals(1, cache.get("key1"));

        cache.setShared(false);
        cache.setPrefix("test");

        assertEquals(null, cache.get("key1"));
    }

    @Test
    public void shared_key_gen_with_empty_prefix_test() {
        RedisCache cache = getCache();

        cache.setShared(true);
        cache.put("key2", 1, Duration.ofSeconds(1));

        assertEquals(1, cache.get("key2"));

        cache.setShared(false);
        cache.setPrefix("");

        assertEquals(1, cache.get("key2"));
    }

    @Test
    public void encoding_test() {
        String value = "не ASCII символы";

        RedisCache cache = getCache();
        cache.put("non_ascii", value, Duration.ofSeconds(1));

        assertEquals(value, cache.get("non_ascii"));
    }

    @Test
    public void calc_test() {
        RedisCache cache = getCache();

        cache.put("value1", 3, Duration.ofSeconds(1));
        cache.put("value2", 4, Duration.ofSeconds(1));

        Integer i1 = (Integer) cache.get("value1");
        Integer i2 = (Integer) cache.get("value2");

        Integer result = i1 + i2;

        assertEquals((Integer) 7, result);
    }

    @Test
    public void is_exists_key() {
        RedisCache cache = getCache();

        assertFalse(cache.exists("value"));
        assertTrue(cache.put("value", "hello", Duration.ofSeconds(1)));
        assertTrue(cache.exists("value"));
    }

}
