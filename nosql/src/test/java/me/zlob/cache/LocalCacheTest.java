
package me.zlob.cache;

import java.time.*;

import org.junit.*;

import static org.junit.Assert.*;

public class LocalCacheTest {

    private static final long DEFAULT_DELAY = 10;

    private static final Duration DEFAULT_DURATION = Duration.ofMillis(DEFAULT_DELAY);

    private LocalCache getCache() {
        return new LocalCache(100);
    }

    @Test
    public void shared_key_gen_with_prefix_test() {
        LocalCache cache = getCache();

        cache.setUseHash(false);
        cache.setShared(true);
        cache.put("key1", 1, Duration.ofMillis(50));

        assertEquals(1, cache.get("key1"));

        cache.setShared(false);
        cache.setPrefix("test");

        assertEquals(null, cache.get("key1"));
    }

    @Test
    public void shared_key_gen_with_empty_prefix_test() {
        LocalCache cache = getCache();

        cache.setShared(true);
        cache.put("key2", 1, DEFAULT_DURATION);

        assertEquals(1, cache.get("key2"));

        cache.setShared(false);
        cache.setPrefix("");

        assertEquals(1, cache.get("key2"));
    }

    @Test
    public void encoding_test() {
        String value = "не ASCII символы";

        LocalCache cache = getCache();
        cache.put("non_ascii", value, DEFAULT_DURATION);

        assertEquals(value, cache.get("non_ascii"));
    }

    @Test
    public void calc_test() {
        LocalCache cache = getCache();

        cache.put("value1", 3, DEFAULT_DURATION);
        cache.put("value2", 4, DEFAULT_DURATION);

        Integer result = (Integer) cache.get("value1")
                         + (Integer) cache.get("value2");

        assertEquals((Integer) 7, result);
    }

    @Test
    public void is_exists_key() {
        LocalCache cache = getCache();

        assertFalse(cache.exists("value"));
        assertTrue(cache.put("value", "hello", DEFAULT_DURATION));
        assertTrue(cache.exists("value"));
    }

}
