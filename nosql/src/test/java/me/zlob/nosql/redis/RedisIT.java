
package me.zlob.nosql.redis;

import java.util.*;

import redis.clients.jedis.*;

import org.junit.*;
import static org.junit.Assert.*;

import static me.zlob.properties.PropertiesProcessors.*;
import static me.zlob.util.Util.*;

public class RedisIT {

    private final String resource = "me/zlob/nosql/redis/redis.properties";

    private JedisPool pool;

    private Map<String, String> properties = fromClasspath(resource)
        .joinWith(fromEnvironment()
            .andThen(filterByKey("zcom.redis"))
            .andThen(replaceKeyPrefix("zcom.redis", "redis")))
        .loadAsMap();

    @Before
    public void init() {
        String host = properties.get("redis.host");
        int port = parseInt(properties.get("redis.port"));
        pool = new JedisPool(host, port);
    }

    @Test
    public void get_non_exist_key_test() {
        Redis redis = new Redis(pool);
        assertNull(redis.get("blabla"));
    }

}
