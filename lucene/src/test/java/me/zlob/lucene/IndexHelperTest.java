
package me.zlob.lucene;

import java.util.*;

import org.junit.*;
import static org.junit.Assert.*;

import static me.zlob.lucene.IndexHelper.*;

public class IndexHelperTest {

    @Test
    public void build_tokens_test() {
        String text = "Carl stole corals from Clara and Clara stole a clarinet from Carl";
        Set<String> expected = new LinkedHashSet<>(Arrays.asList("carl", "stole", "coral", "clara", "clarinet"));
        assertEquals(expected, buildTokensUnique(text, true));
    }

    @Test
    public void build_tokens_string_test() {
        String text = "Carl stole corals from Clara and Clara stole a clarinet from Carl";
        String expected = "carl stole coral clara clara stole clarinet carl";
        assertEquals(expected, buildTokensString(text));
    }

}
