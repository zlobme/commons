
package me.zlob.lucene;

import java.io.*;
import java.util.*;
import org.junit.*;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import com.google.gson.*;

import me.zlob.util.*;

public class IndexFactoryTest {

    private static Gson gson = GsonFactory.getGson(true);

    private static IndexFactory indexFactory = new IndexFactory("/tmp/lucene/", Arrays.asList("test"));

    @Test
    @Ignore
    public void flow_test() {
        String version = indexFactory.createVersion("test");
        System.out.println("new version: " + version);
        System.out.println("versions: " + gson.toJson(indexFactory.getVersions("test")));

        String toDeleteVersion = indexFactory.getPreviousVersion("test");
        System.out.println("to delete version: " + toDeleteVersion);

        boolean isMake = indexFactory.makeCurrentVersion("test", version);
        System.out.println("make current version: " + isMake);
        String currVersion = indexFactory.getCurrentVersion("test");
        System.out.println("current version: " + currVersion);
        System.out.println("is new version are current: " + version.equals(currVersion));

        String prevVersion = indexFactory.getPreviousVersion("test");
        System.out.println("previous version: " + prevVersion);

        boolean isDelete = indexFactory.deleteVersion("test", toDeleteVersion);
        System.out.println("delete prev version: " + isDelete);
    }

    @Test
    @Ignore
    public void get_current_version_test() {
        String newVersion = indexFactory.createVersion("test");
        String currentVersion = indexFactory.getCurrentVersion("test");

        assertTrue(indexFactory.makeCurrentVersion("test", newVersion));
        assertEquals(currentVersion, indexFactory.getPreviousVersion("test"));
        assertEquals(newVersion, indexFactory.getCurrentVersion("test"));
    }

}
