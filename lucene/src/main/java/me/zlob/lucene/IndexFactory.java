
package me.zlob.lucene;

import java.io.*;
import java.nio.file.*;
import java.util.*;

import org.slf4j.*;

import static me.zlob.util.Conditions.*;

public class IndexFactory {

    private static final Logger log = LoggerFactory.getLogger(IndexFactory.class);

    private static final String CURRENT = "current";

    private static final String PREVIOUS = "previous";

    private String root;

    private List<String> names;

    private Map<String, IndexHandler> handlers = new HashMap();

    private String getPath(String name) {
        String result = root + "/" + name;
        try {
            File file = new File(result);
            if (file.exists()) {
                result = file.getCanonicalPath();
            }
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        return result;
    }

    private String getPath(String name, String version) {
        return getPath(name, version, true);
    }

    private String getPath(String name, String version, boolean create) {
        String result = root + "/" + name + "/" + version;
        try {
            File file = new File(result);
            if (file.exists() && create) {
                result = file.getCanonicalPath();
            }
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        return result;
    }

    private String getVersion(String path) {
        String result = "";
        if (existsNotEmpty(path)) {
            int pos = path.lastIndexOf("/");
            if (pos > 0 && pos < path.length() - 1) {
                String version = path.substring(pos + 1);
                try {
                    Long.parseLong(version);
                    result = version;
                } catch (Exception ex) {
                }
            }
        }
        return result;
    }

    public IndexFactory(String root, List<String> names) {
        this.root = root.endsWith("/") ? root.substring(0, root.length() - 1) : root;
        this.names = names;
    }

    public List<String> getVersions(String name) {
        List<String> result = Collections.EMPTY_LIST;
        String path = getPath(name, "");
        if (names.contains(name) && existsNotEmpty(path)) {
            result = new ArrayList();
            try {
                File folder = new File(path);
                for (final File file : folder.listFiles()) {
                    if (file.isDirectory()
                        && (!file.getName().equals(".")
                            && !file.getName().equals("..")
                            && !file.getName().equals(CURRENT)
                            && !file.getName().equals(PREVIOUS))) {
                        result.add(file.getName());
                    }
                }
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
            }
        }
        return result;
    }

    public String getCurrentVersion(String name) {
        String result = "";
        if (names.contains(name)) {
            result = getVersion(getPath(name, CURRENT));
        }
        return result;
    }

    public String getPreviousVersion(String name) {
        String result = "";
        if (names.contains(name)) {
            result = getVersion(getPath(name, PREVIOUS));
        }
        return result;
    }

    public String createVersion(String name) {
        String result = "";
        if (names.contains(name)) {
            File dir = new File(getPath(name));
            if (!dir.exists()) {
                dir.mkdir();
            }
            Date date = new Date();
            try {
                File version = new File(getPath(name, Long.toString(date.getTime())));
                if (!version.exists()) {
                    version.mkdir();
                }
                result = Long.toString(date.getTime());
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
            }
        }
        return result;
    }

    public boolean makeCurrentVersion(String name, String version) {
        boolean result = false;
        if (names.contains(name) && existsNotEmpty(version)) {
            String currentPath = getPath(name, "") + "/" + CURRENT;
            String previousPath = getPath(name, "") + "/" + PREVIOUS;
            try {
                File previousLink = new File(previousPath);
                previousLink.delete();

                File currentLink = new File(currentPath);
                if (currentLink.exists()) {
                    File oldFolder = new File(getPath(name, CURRENT));
                    Files.createSymbolicLink(previousLink.toPath(), oldFolder.toPath());
                } else {
                    currentLink.delete();
                }

                File newFolder = new File(getPath(name, version));
                if (currentLink.exists() && newFolder.exists()) {
                    currentLink.delete();
                }
                if (newFolder.exists()) {
                    Files.createSymbolicLink(currentLink.toPath(), newFolder.toPath());
                    handlers.remove(name + ":" + CURRENT);
                    result = true;
                }
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
            }
        }
        return result;
    }

    public boolean deleteVersion(String name, String version) {
        boolean result = false;
        if (names.contains(name) && existsNotEmpty(version)) {
            String path = getPath(name, version);
            if (!path.equals(getPath(name, CURRENT))) {
                try {
                    File folder = new File(path);
                    if (folder.exists() && folder.isDirectory()) {
                        IndexHandler handler = handlers.get(name + ":" + version);
                        if (handler != null) {
                            handler.close();
                        }
                        for (File file : folder.listFiles()) {
                            file.delete();
                        }
                        result = folder.delete();
                        handlers.remove(name + ":" + version);
                    }
                } catch (Exception ex) {
                    log.error(ex.getMessage(), ex);
                }
            }
        }
        return result;
    }

    public IndexHandler getHandler(String name) {
        return getHandler(name, CURRENT);
    }

    public IndexHandler getHandler(String name, String version) {
        return getHandler(name, version, true);
    }

    public IndexHandler getHandler(String name, String version, boolean create) {
        IndexHandler handler = null;
        if (names.contains(name)) {
            handler = handlers.get(name + ":" + version);
            if (handler == null) {
                String path = getPath(name, version);
                if (!path.endsWith(CURRENT) && !path.endsWith(PREVIOUS)) {
                    handler = new IndexHandler(getPath(name, version));
                    handlers.put(name + ":" + version, handler);
                }
            }
        }
        return handler;
    }

    public void close(String name) {
        IndexHandler handler = handlers.get(name);
        if (handler != null) {
            handler.close();
            handlers.remove(name);
        }
    }

    public void close() {
        for (IndexHandler handler : handlers.values()) {
            handler.close();
        }
    }

}
