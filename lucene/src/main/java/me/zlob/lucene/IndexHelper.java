
package me.zlob.lucene;

import java.io.*;
import java.util.*;

import org.slf4j.*;

import org.apache.lucene.analysis.*;
import org.apache.lucene.analysis.core.*;
import org.apache.lucene.analysis.miscellaneous.*;
import org.apache.lucene.analysis.snowball.*;
import org.apache.lucene.analysis.standard.*;
import org.apache.lucene.analysis.tokenattributes.*;
import org.apache.lucene.analysis.util.*;
import org.apache.lucene.document.*;
import org.apache.lucene.facet.*;
import org.apache.lucene.facet.taxonomy.*;
import org.apache.lucene.index.*;
import org.apache.lucene.queryparser.classic.*;
import org.apache.lucene.search.*;
import org.tartarus.snowball.ext.*;

import static org.apache.lucene.search.SortField.*;

import me.zlob.functional.*;
import me.zlob.util.*;

import static me.zlob.util.Conditions.*;

public class IndexHelper {

    private static final Logger log = LoggerFactory.getLogger(IndexHandler.class);

    private static final CharArraySet stopWords = new CharArraySet(StopWords.getInstance(), true);

    private static final Analyzer analyzer = new ClassicAnalyzer(stopWords);

    private static final FacetsConfig config = new FacetsConfig();

    private final Object revisionLock = new Object();

    private IndexHandler handler;

    public static TokenStream buildTokensStream(String text) {
        return buildTokensStream(text, "text");
    }

    public static TokenStream buildTokensStream(String text, String field) {
        TokenStream result = null;
        try {
            result = analyzer.tokenStream(field, text);
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }

        if (result != null) {
            result = new SnowballFilter(result, new RussianStemmer());
            result = new SnowballFilter(result, new EnglishStemmer());
            result = new LowerCaseFilter(result);
            result = new LengthFilter(result, 2, 30);
        }
        return result;
    }

    public static List<String> buildTokens(TokenStream tokenStream) {
        List<String> result = new LinkedList();
        try (TokenStream ts = tokenStream) {
            ts.reset();
            while (ts.incrementToken()) {
                result.add(ts.addAttribute(CharTermAttribute.class).toString());
            }
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        return result;
    }

    public static List<String> buildTokens(String text) {
        return buildTokens(buildTokensStream(text));
    }

    public static Set<String> buildTokensUnique(String text) {
        return buildTokensUnique(buildTokensStream(text), false);
    }

    public static Set<String> buildTokensUnique(String text, boolean saveOrder) {
        return buildTokensUnique(buildTokensStream(text), saveOrder);
    }

    public static Set<String> buildTokensUnique(TokenStream ts) {
        return buildTokensUnique(ts, false);
    }

    public static Set<String> buildTokensUnique(TokenStream ts, boolean saveOrder) {
        return saveOrder ? new LinkedHashSet(buildTokens(ts)) : new TreeSet(buildTokens(ts));
    }

    public static String buildTokensString(String text) {
        return String.join(" ", buildTokens(text));
    }

    public static String buildTokensUniqueString(String text) {
        return String.join(" ", buildTokensUnique(text));
    }

    public static StringReader buildTokensStringReader(String text) {
        return new StringReader(String.join(" ", buildTokens(text)));
    }

    public static Query buildQuery(String queryText, String[] fields) {
        Query query = null;
        if (existsNotEmpty(queryText)) {
            try {
                query = new MultiFieldQueryParser(fields, analyzer).parse(queryText);
            } catch (ParseException | RuntimeException ex) {
                log.error(ex.getMessage(), ex);
            }
        }
        return query;
    }

    public static Query buildQuery(String queryText, List<String> fields) {
        return buildQuery(queryText, fields.toArray(new String[fields.size()]));
    }

    public static Sort buildSort(List<Map<String, String>> sortInfo) {
        if (existsNotEmpty(sortInfo)) {
            int i = 0;
            SortField[] sorts = new SortField[sortInfo.size() + 1];
            sorts[i++] = SortField.FIELD_SCORE;
            for (Map<String, String> map : sortInfo) {
                if (map.containsKey("field") && map.containsKey("type") && map.containsKey("order")) {
                    Type type = Type.STRING;
                    if (existsAndEqual(map.get("type"), "int")) {
                        type = Type.INT;
                    } else if (existsAndEqual(map.get("type"), "long")) {
                        type = Type.LONG;
                    } else if (existsAndEqual(map.get("type"), "float")) {
                        type = Type.FLOAT;
                    } else if (existsAndEqual(map.get("type"), "double")) {
                        type = Type.DOUBLE;
                    }
                    sorts[i++] = new SortField(map.get("field"), type, existsAndEqual(map.get("order"), "desc"));
                }
            }
            return new Sort(sorts);
        }
        return null;
    }

    private List<Integer> getDocumentsOnPage(Page page, TopDocs documents) {
        List<Integer> result = Collections.EMPTY_LIST;
        if (documents != null && documents.totalHits > 0) {
            page.setCount(documents.totalHits);
            result = new ArrayList<>();
            int limit = page.limit();
            for (int i = page.offset(); i < limit; i++) {
                result.add(documents.scoreDocs[i].doc);
            }
        }
        return result;
    }

    private String getId(Document doc) {
        return getField("id", doc);
    }

    private String getField(String name, Document doc) {
        String result = "";
        if (doc != null) {
            result = doc.get(name);
        }
        return result;
    }

    private boolean containsFacet(Document document) {
        if (document != null && !document.getFields().isEmpty()) {
            for (IndexableField field : document.getFields()) {
                if (field instanceof FacetField) {
                    return true;
                }
            }
        }
        return false;
    }

    private Document processTaxonomy(Document doc) {
        Document result = doc;
        if (containsFacet(doc)) {
            TaxonomyWriter taxonomy = handler.getTaxonomyWriter();
            if (taxonomy != null) {
                try {
                    result = config.build(taxonomy, doc);
                } catch (Exception ex) {
                    log.error(ex.getMessage(), ex);
                }
            }
        }
        return result;
    }

    private List<Document> processTaxonomy(List<Document> docs) {
        List<Document> result = docs;
        if (existsNotEmpty(docs) && containsFacet(docs.get(0))) {
            TaxonomyWriter taxonomy = handler.getTaxonomyWriter();
            if (taxonomy != null) {
                result = new ArrayList<>(docs.size());
                for (Document doc : docs) {
                    try {
                        result.add(config.build(taxonomy, doc));
                    } catch (Exception ex) {
                        log.error(ex.getMessage(), ex);
                    }
                }
            }
        }
        return result;
    }

    public IndexHelper(IndexHandler handler) {
        this.handler = handler;
    }

    public Document read(int id) {
        Document document = null;
        IndexReader reader = handler.getReader();
        if (reader != null) {
            try {
                document = reader.document(id);
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
            }
        }
        return document;
    }

    public List<Document> read(List<Integer> ids) {
        List<Document> documents = Collections.EMPTY_LIST;
        IndexReader reader = handler.getReader();
        if (reader != null) {
            documents = new ArrayList<>(ids.size());
            for (Integer id : ids) {
                try {
                    documents.add(reader.document(id));
                } catch (Exception ex) {
                    log.error(ex.getMessage(), ex);
                }
            }
        }
        return documents;
    }

    public boolean write(Document doc) {
        boolean result = false;
        IndexWriter writer = handler.getWriter();
        if (writer != null) {
            try {
                writer.addDocument(processTaxonomy(doc));
                result = true;
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
            }
        }
        return result;
    }

    public boolean write(List<Document> docs) {
        boolean result = false;
        IndexWriter writer = handler.getWriter();
        if (writer != null) {
            try {
                writer.addDocuments(processTaxonomy(docs));
                result = true;
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
            }
        }
        return result;
    }

    public boolean update(Document doc) {
        boolean result = false;
        IndexWriter writer = handler.getWriter();
        if (writer != null) {
            try {
                writer.updateDocument(new Term("id", getId(doc)), processTaxonomy(doc));
                result = true;
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
            }
        }
        return result;
    }

    public boolean update(List<Document> docs) {
        boolean result = false;
        IndexWriter writer = handler.getWriter();
        if (writer != null) {
            result = true;
            for (Document doc : docs) {
                try {
                    writer.updateDocument(new Term("id", getId(doc)), processTaxonomy(doc));
                    result &= true;
                } catch (Exception ex) {
                    result = false;
                    log.error(ex.getMessage(), ex);
                }
            }
        }
        return result;
    }

    public boolean delete(Document doc) {
        boolean result = false;
        IndexWriter writer = handler.getWriter();
        if (writer != null) {
            try {
                writer.deleteDocuments(new Term("id", getId(doc)));
                result = true;
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
            }
        }
        return result;
    }

    public boolean delete(List<Document> docs) {
        boolean result = false;
        IndexWriter writer = handler.getWriter();
        if (writer != null) {
            result = true;
            for (Document doc : docs) {
                try {
                    writer.deleteDocuments(new Term("id", getId(doc)));
                    result &= true;
                } catch (Exception ex) {
                    result = false;
                    log.error(ex.getMessage(), ex);
                }
            }
        }
        return result;
    }

    public void delete(String field, String value) {
        IndexWriter writer = handler.getWriter();
        if (writer != null) {
            try {
                writer.deleteDocuments(new Term(field, value));
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
            }
        }
    }

    public boolean deleteById(long id) {
        boolean result = false;
        IndexWriter writer = handler.getWriter();
        if (writer != null) {
            try {
                writer.deleteDocuments(new Term("id", Long.toString(id)));
                result = true;
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
            }
        }
        return result;
    }

    public boolean deleteById(List<Long> ids) {
        boolean result = false;
        IndexWriter writer = handler.getWriter();
        if (writer != null) {
            for (Long id : ids) {
                try {
                    writer.deleteDocuments(new Term("id", Long.toString(id)));
                    result = true;
                } catch (Exception ex) {
                    log.error(ex.getMessage(), ex);
                }
            }
        }
        return result;
    }

    public List<Integer> search(Page page, Query query) {
        return search(page, query, null, Sort.RELEVANCE, 10000);
    }

    public List<Integer> search(Page page, Query query, Query filter) {
        return search(page, query, filter, Sort.RELEVANCE, 10000);
    }

    public List<Integer> search(Page page, Query query, Sort sort) {
        return search(page, query, null, sort, 10000);
    }

    public List<Integer> search(Page page, Query query, Query filter, Sort sort, int max) {
        List<Integer> documents = Collections.EMPTY_LIST;
        IndexSearcher searcher = handler.getSearcher();
        if (searcher != null) {
            TopDocs docs = null;
            if (page != null && query != null) {
                try {
                    if (filter != null) {
                        BooleanQuery.Builder builder = new BooleanQuery.Builder();
                        builder.add(query, BooleanClause.Occur.MUST);
                        builder.add(filter, BooleanClause.Occur.FILTER);
                        query = builder.build();
                    }
                    if (sort != null) {
                        docs = searcher.search(query, max, sort);
                    } else {
                        docs = searcher.search(query, max);
                    }
                } catch (Exception ex) {
                    log.error(ex.getMessage(), ex);
                }
            }
            if (docs != null) {
                documents = getDocumentsOnPage(page, docs);
            }
        }
        return documents;
    }

    public List<Tuple<String, Integer>> facet(Query query, Query filter) {
        return null;
    }

    public List<Tuple<String, Integer>> facet(String dimension, Query query, Query filter) {
        IndexSearcher searcher = handler.getSearcher();
        if (searcher != null) {
            TopDocs docs = null;
            if (query != null) {
                try {
                    if (filter != null) {
                        BooleanQuery.Builder builder = new BooleanQuery.Builder();
                        builder.add(query, BooleanClause.Occur.MUST);
                        builder.add(filter, BooleanClause.Occur.FILTER);
                        query = builder.build();
                    }
                    FacetsCollector facetsCollector = new FacetsCollector();
                    searcher.search(query, facetsCollector);
                    Facets facetsCounts = new FastTaxonomyFacetCounts(dimension, handler.getTaxonomyReader(), config, facetsCollector);
                    FacetResult topChildren = facetsCounts.getTopChildren(1000, "contact_facet");
                } catch (Exception ex) {
                    log.error(ex.getMessage(), ex);
                }
            }
        }
        return null;
    }

    public int facet(String dimension, String path, Query query, Query filter) {
        return 0;
    }

    public String getRevision() {
        String result = "0";
        synchronized (revisionLock) {
            try {
                IndexWriter writer = handler.getWriter();
                IndexDeletionPolicy indexDeletionPolicy = writer.getConfig().getIndexDeletionPolicy();
                if (indexDeletionPolicy instanceof SnapshotDeletionPolicy) {
                    IndexCommit snapshot = ((SnapshotDeletionPolicy) indexDeletionPolicy).snapshot();
                    result = Long.toString(snapshot.getGeneration());
                }
            } catch (IllegalStateException ex) {
                //log.error("no index commits");
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
            } finally {
                handler.closeWriter();
            }
        }
        return result;
    }

    public boolean merge() {
        boolean result = true;
        IndexWriter writer = handler.getWriter();
        try {
            MergePolicy.OneMerge nextMerge;
            while ((nextMerge = writer.getNextMerge()) != null) {
                writer.merge(nextMerge);
            }
            writer.commit();
        } catch (Exception ex) {
            result = false;
            log.error(ex.getMessage(), ex);
        }
        return result;
    }

    public boolean commit() {
        boolean result = true;
        try {
            handler.getWriter().commit();
            handler.getTaxonomyWriter(false).commit();
        } catch (Exception ex) {
            result = false;
            log.error(ex.getMessage(), ex);
        }
        return result;
    }

    public boolean rollback() {
        boolean result = true;
        try {
            handler.getWriter().rollback();
        } catch (Exception ex) {
            result = false;
            log.error(ex.getMessage(), ex);
        }
        return result;
    }

    public void close() {
        handler.close();
    }

}
