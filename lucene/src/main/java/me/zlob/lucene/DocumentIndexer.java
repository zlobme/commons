
package me.zlob.lucene;

import java.util.*;

import org.apache.lucene.document.*;

import me.zlob.annotations.*;

public interface DocumentIndexer extends Indexer<Map<String, Object>, Document> {

}
