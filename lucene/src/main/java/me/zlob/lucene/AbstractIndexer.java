
package me.zlob.lucene;

import org.apache.lucene.document.*;
import org.apache.lucene.index.*;

public abstract class AbstractIndexer implements Indexer<Map<String, Object>, Document> {

    private static final FieldType LONG_FIELD_TYPE;

    private static final FieldType FLOAT_FIELD_TYPE;

    static {
        LONG_FIELD_TYPE = new FieldType();
        LONG_FIELD_TYPE.setTokenized(false);
        LONG_FIELD_TYPE.setOmitNorms(true);
        LONG_FIELD_TYPE.setIndexOptions(IndexOptions.DOCS);
        LONG_FIELD_TYPE.setNumericType(FieldType.LegacyNumericType.LONG);
        LONG_FIELD_TYPE.setStored(true);
        LONG_FIELD_TYPE.setDocValuesType(DocValuesType.NUMERIC);
        LONG_FIELD_TYPE.freeze();

        FLOAT_FIELD_TYPE = new FieldType();
        FLOAT_FIELD_TYPE.setTokenized(false);
        FLOAT_FIELD_TYPE.setOmitNorms(true);
        FLOAT_FIELD_TYPE.setIndexOptions(IndexOptions.DOCS);
        FLOAT_FIELD_TYPE.setNumericType(FieldType.LegacyNumericType.FLOAT);
        FLOAT_FIELD_TYPE.setStored(true);
        FLOAT_FIELD_TYPE.setDocValuesType(DocValuesType.NUMERIC);
        FLOAT_FIELD_TYPE.freeze();
    }

    public static FieldType getLongFieldType() {
        return LONG_FIELD_TYPE;
    }

    public static FieldType getFloatFieldType() {
        return FLOAT_FIELD_TYPE;
    }

}
