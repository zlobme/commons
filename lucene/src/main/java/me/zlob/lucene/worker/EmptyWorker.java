
package me.zlob.lucene.worker;

import java.util.*;

public class EmptyWorker implements Worker {

    @Override
    public boolean canExecute(Map<String, Object> command) {
        return true;
    }

    @Override
    public Map<String, Object> execute(Map<String, Object> command) {
        return Collections.EMPTY_MAP;
    }

    @Override
    public void close() {
    }

}
