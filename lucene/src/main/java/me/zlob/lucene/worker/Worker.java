
package me.zlob.lucene.worker;

import java.util.*;

public interface Worker {

    public boolean canExecute(Map<String, Object> command);

    public Map<String, Object> execute(Map<String, Object> command);

    public void close();

}
