
package me.zlob.lucene.worker;

import java.util.*;

import static me.zlob.util.Conditions.*;

public class PingWorker implements Worker {

    @Override
    public boolean canExecute(Map<String, Object> command) {
        return command.containsKey("type")
               && existsAndEqual((String) command.get("type"), "ping");
    }

    @Override
    public Map<String, Object> execute(Map<String, Object> command) {
        Map<String, Object> result = new LinkedHashMap<>();
        result.put("message", "pong");
        result.put("date", new Date());
        return result;
    }

    @Override
    public void close() {
    }

}
