
package me.zlob.lucene.worker;

import java.util.*;

import org.slf4j.*;

import org.apache.lucene.document.*;
import org.apache.lucene.search.*;

import me.zlob.lucene.*;
import me.zlob.util.*;

import static me.zlob.lucene.IndexHelper.*;
import static me.zlob.util.Conditions.*;

public class SearchWorker implements Worker {

    private static final Logger log = LoggerFactory.getLogger(SearchWorker.class);

    private IndexFactory indexFactory;

    private boolean validate(Map<String, Number> pageInfo) {
        return existsNotEmpty(pageInfo)
               && existsNotEmpty(pageInfo.get("page"))
               && existsNotEmpty(pageInfo.get("onPage"));
    }

    private Map<String, Object> search(Map<String, Object> command) {
        Map<String, Object> result = new LinkedHashMap<>();
        IndexHandler handler = indexFactory.getHandler((String) command.get("index"));
        if (handler != null) {
            Page page = new Page(1, 20);
            if (command.get("page") != null) {
                if (command.get("page") instanceof Page) {
                    page = (Page) command.get("page");
                } else if (command.get("page") instanceof Map) {
                    Map<String, Number> pageInfo = (Map<String, Number>) command.get("page");
                    if (validate(pageInfo)) {
                        page = new Page(pageInfo.get("page").intValue(), pageInfo.get("onPage").intValue());
                    }
                }
            }

            IndexHelper helper = new IndexHelper(handler);

            Query query = buildQuery((String) command.get("query"), (List<String>) command.get("fields"));
            Query filter = buildQuery((String) command.get("filter"), (List<String>) command.get("fields"));
            Sort sort = buildSort((List<Map<String, String>>) command.get("sort"));

            List<Integer> documentsIds = helper.search(page, query, filter, sort, 10000);
            List<Document> documents = helper.read(documentsIds);
            List<Long> ids = new ArrayList<>(documents.size());
            for (Document doc : documents) {
                ids.add(Long.parseLong(doc.get("id")));
            }
            result.put("count", page.getCount());
            result.put("pages", page.pages());
            result.put("docs", ids);
        } else {
            result.put("error", "index '" + command.get("index") + "' not found");
        }
        return result;
    }

    public SearchWorker(IndexFactory indexFactory) {
        this.indexFactory = indexFactory;
    }

    @Override
    public boolean canExecute(Map<String, Object> command) {
        if (command.containsKey("type")) {
            if (existsAndEqual((String) command.get("type"), "search")
                && existsNotEmpty((String) command.get("index"))) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Map<String, Object> execute(Map<String, Object> command) {
        if (command.get("type").equals("search")) {
            return search(command);
        } else {
            Map<String, Object> result = new LinkedHashMap<>();
            result.put("error", "command '" + command.get("type") + "' not found");
            return result;
        }
    }

    @Override
    public void close() {
        indexFactory.close();
    }

}
