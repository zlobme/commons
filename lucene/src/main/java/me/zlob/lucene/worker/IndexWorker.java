
package me.zlob.lucene.worker;

import java.io.*;
import java.nio.file.*;
import java.util.*;

import org.slf4j.*;

import org.apache.lucene.document.*;

import me.zlob.lucene.*;
import me.zlob.util.*;

import static me.zlob.util.Conditions.*;
import static me.zlob.util.Time.*;

public class IndexWorker implements Worker {

    private static final Logger log = LoggerFactory.getLogger(IndexWorker.class);

    private IndexFactory indexFactory;

    private Map<String, DocumentIndexer> indexers;

    private int batchSize = 1000;

    public IndexWorker(IndexFactory indexFactory, Map<String, DocumentIndexer> indexers) {
        this.indexFactory = indexFactory;
        this.indexers = indexers;
    }

    public IndexWorker(IndexFactory indexFactory, Map<String, DocumentIndexer> indexers, int batchSize) {
        this.indexFactory = indexFactory;
        this.indexers = indexers;
        this.batchSize = batchSize;
    }

    private String getVersion(String path) {
        String version = "";
        if (path.endsWith("/")) {
            path = path.substring(0, path.length() - 1);
        }
        int index = path.lastIndexOf("/");
        if (index > 0 && (index + 1) < path.length()) {
            version = path.substring(index + 1);
        }
        return version.length() == 13 ? version : "";
    }

    private Date getCreateDate(String path) {
        Date result = parseDate("01.01.2000");
        String timestamp = getVersion(path);
        if (existsNotEmpty(timestamp)) {
            try {
                result = new Date(Long.parseLong(timestamp));
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
            }
        }
        return result;
    }

    private Date getUpdateDate(String path) {
        long time = 0;
        try {
            time = Files.getLastModifiedTime(new File(path).toPath()).toMillis();
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        return new Date(time);
    }

    private List<Document> convert(DocumentIndexer indexer, List<Map<String, Object>> documents) {
        List<Document> result = new ArrayList<>();
        for (Map<String, Object> document : documents) {
            result.add(indexer.index(document));
        }
        return result;
    }

    private Map<String, Object> addBatch(IndexHandler handler,
                                         DocumentIndexer indexer,
                                         List<Map<String, Object>> documents,
                                         boolean commit) {
        Map<String, Object> result = new LinkedHashMap<>();
        IndexHelper helper = new IndexHelper(handler);
        long docsCount = 0;
        long batchCount = 0;
        long batchErrorsCount = 0;
        Page page = new Page(0, batchSize).setCount(documents.size());
        while (page.next().getPage() <= page.pages()) {
            List<Map<String, Object>> batch = documents.subList(page.offset(), page.limit());
            boolean success = helper.write(convert(indexer, batch));
            batchCount++;
            docsCount += batch.size();
            if (commit) {
                if (success) {
                    helper.commit();
                } else {
                    helper.rollback();
                }
            }
            if (!success) {
                batchErrorsCount++;
            }
        }
        result.put("docs", docsCount);
        result.put("batches", batchCount);
        result.put("batchesErrors", batchErrorsCount);
        return result;
    }

    private Map<String, Object> updateBatch(IndexHandler handler,
                                            DocumentIndexer indexer,
                                            List<Map<String, Object>> documents,
                                            boolean commit) {
        Map<String, Object> result = new LinkedHashMap<>();
        IndexHelper helper = new IndexHelper(handler);
        long docsCount = 0;
        long batchCount = 0;
        long batchErrorsCount = 0;
        Page page = new Page(0, batchSize).setCount(documents.size());
        while (page.next().getPage() <= page.pages()) {
            List<Map<String, Object>> batch = documents.subList(page.offset(), page.limit());
            boolean success = helper.update(convert(indexer, batch));
            batchCount++;
            docsCount += batch.size();
            if (commit) {
                if (success) {
                    helper.commit();
                } else {
                    helper.rollback();
                }
            }
            if (!success) {
                batchErrorsCount++;
            }
        }
        result.put("docs", docsCount);
        result.put("batches", batchCount);
        result.put("batchesErrors", batchErrorsCount);
        return result;
    }

    private Map<String, Object> deleteDocs(IndexHandler handler, List<Map<String, Object>> documents, boolean commit) {
        Map<String, Object> result = new LinkedHashMap<>();
        IndexHelper helper = new IndexHelper(handler);
        long docsCount = 0;
        long batchCount = 0;
        long batchErrorsCount = 0;
        Page page = new Page(0, batchSize).setCount(documents.size());
        while (page.next().getPage() <= page.pages()) {
            List<Map<String, Object>> batch = documents.subList(page.offset(), page.limit());
            List<Long> ids = new ArrayList<>();
            for (Map<String, Object> doc : batch) {
                ids.add(((Number) doc.get("id")).longValue());
            }
            boolean success = helper.deleteById(ids);
            batchCount++;
            docsCount += batch.size();
            if (commit) {
                if (success) {
                    helper.commit();
                } else {
                    helper.rollback();
                }
            }
            if (!success) {
                batchErrorsCount++;
            }
        }
        result.put("docs", docsCount);
        result.put("batches", batchCount);
        result.put("batchesErrors", batchErrorsCount);
        return result;
    }

    private Map<String, Object> checkIndex(Map<String, Object> command) {
        Map<String, Object> result = new LinkedHashMap<>();
        String index = (String) command.get("index");
        String version = (String) command.get("version");
        IndexHandler handler = indexFactory.getHandler(index);
        if (handler != null) {
            List<String> versions = indexFactory.getVersions(index);
            String currentVersion = getVersion(handler.getPath());
            result.put("versions", versions);
            Map<String, Object> current = new LinkedHashMap<>();
            current.put("version", currentVersion);
            current.put("revision", new IndexHelper(handler).getRevision() + "");
            current.put("created", formatDateTime(getCreateDate(handler.getPath())));
            current.put("updated", formatDateTime(getUpdateDate(handler.getPath())));
            result.put("current", current);
            if (existsNotEmpty(version)) {
                if (version.equals(currentVersion)) {
                    result.put("version", current);
                } else if (versions.contains(version)) {
                    IndexHandler versionHandler = indexFactory.getHandler(index, version, false);
                    if (versionHandler != null) {
                        Map<String, Object> versionInfo = new LinkedHashMap<>();
                        versionInfo.put("version", getVersion(versionHandler.getPath()));
                        versionInfo.put("revision", new IndexHelper(versionHandler).getRevision() + "");
                        versionInfo.put("created", formatDateTime(getCreateDate(versionHandler.getPath())));
                        versionInfo.put("updated", formatDateTime(getUpdateDate(versionHandler.getPath())));
                        result.put("version", versionInfo);
                    }
                }
            }
        } else {
            result.put("error", "index '" + command.get("index") + "' not found");
        }
        return result;
    }

    private Map<String, Object> createIndex(Map<String, Object> command) {
        Map<String, Object> result = new LinkedHashMap<>();
        String index = (String) command.get("index");
        String newVersion = indexFactory.createVersion(index);
        if (existsNotEmpty(newVersion)) {
            result.put("index", index);
            result.put("version", newVersion);
        } else {
            result.put("error", "index '" + command.get("index") + "' not found");
        }
        return result;
    }

    private Map<String, Object> switchIndex(Map<String, Object> command) {
        Map<String, Object> result = new LinkedHashMap<>();
        String index = (String) command.get("index");
        String newVersion = (String) command.get("version");
        if (existsNotEmpty(newVersion)) {
            String currentVersion = indexFactory.getCurrentVersion(index);
            String oldPreviousVersion = indexFactory.getPreviousVersion(index);
            if (!newVersion.equals(currentVersion)) {
                IndexHandler handler = indexFactory.getHandler(index, newVersion);
                if (handler != null) {
                    boolean oldDeleted = false;
                    boolean switchVersion = indexFactory.makeCurrentVersion(index, newVersion);
                    if (switchVersion) {
                        oldDeleted = indexFactory.deleteVersion(index, oldPreviousVersion);
                    }
                    result.put("index", index);
                    result.put("version", getVersion(handler.getPath()));
                    result.put("previous", currentVersion);
                    result.put("cleanup", oldDeleted);

                    handler.closeWriter();
                } else {
                    result.put("error", "index '" + command.get("index") + "' not found");
                }
            } else {
                result.put("index", index);
                result.put("version", newVersion);
                result.put("previous", oldPreviousVersion);
            }
        } else {
            result.put("error", "index '" + command.get("index") + "' not found");
        }
        return result;
    }

    private Map<String, Object> mergeIndex(Map<String, Object> command) {
        Map<String, Object> result = new LinkedHashMap<>();
        String index = (String) command.get("index");
        String version = (String) command.get("version");
        IndexHandler handler = indexFactory.getHandler(index, version);
        if (handler != null) {
            IndexHelper helper = new IndexHelper(handler);
            helper.merge();
            result.put("merge", "done");
        } else {
            result.put("error", "index '" + command.get("index") + "' not found");
        }
        return result;
    }

    private Map<String, Object> addDocument(Map<String, Object> command) {
        Map<String, Object> result = new LinkedHashMap<>();
        String index = (String) command.get("index");
        String version = (String) command.get("version");
        boolean commit = (boolean) command.getOrDefault("commit", true);

        IndexHandler handler;
        if (existsNotEmpty(version)) {
            handler = indexFactory.getHandler(index, version);
        } else {
            handler = indexFactory.getHandler(index);
        }

        DocumentIndexer indexer = indexers.get(index);
        if (handler != null && indexer != null) {
            List<Map<String, Object>> documents = (List<Map<String, Object>>) command.get("documents");
            if (existsNotEmpty(documents)) {
                result.putAll(addBatch(handler, indexer, documents, commit));
            }
        } else if (handler == null && existsNotEmpty(version)) {
            result.put("error", "index '" + command.get("index") + "(" + version + ")" + "' not found");
        } else if (handler == null) {
            result.put("error", "index '" + command.get("index") + "' not found");
        } else if (indexer == null) {
            result.put("error", "indexer for index '" + command.get("index") + "' not found");
        }
        return result;
    }

    private Map<String, Object> updateDocument(Map<String, Object> command) {
        Map<String, Object> result = new LinkedHashMap<>();
        String index = (String) command.get("index");
        String version = (String) command.get("version");
        boolean commit = (boolean) command.getOrDefault("commit", true);

        IndexHandler handler;
        if (existsNotEmpty(version)) {
            handler = indexFactory.getHandler(index, version);
        } else {
            handler = indexFactory.getHandler(index);
        }

        DocumentIndexer indexer = indexers.get(index);
        if (handler != null && indexer != null) {
            List<Map<String, Object>> documents = (List<Map<String, Object>>) command.get("documents");
            if (existsNotEmpty(documents)) {
                result.putAll(updateBatch(handler, indexer, documents, commit));
            }
        } else if (handler == null && existsNotEmpty(version)) {
            result.put("error", "index '" + command.get("index") + "(" + version + ")" + "' not found");
        } else if (handler == null) {
            result.put("error", "index '" + command.get("index") + "' not found");
        } else if (indexer == null) {
            result.put("error", "indexer for index '" + command.get("index") + "' not found");
        }
        return result;
    }

    private Map<String, Object> deleteDocument(Map<String, Object> command) {
        Map<String, Object> result = new LinkedHashMap<>();
        String index = (String) command.get("index");
        String version = (String) command.get("version");
        boolean commit = (boolean) command.getOrDefault("commit", true);

        IndexHandler handler;
        if (existsNotEmpty(version)) {
            handler = indexFactory.getHandler(index, version);
        } else {
            handler = indexFactory.getHandler(index);
        }

        DocumentIndexer indexer = indexers.get(index);
        if (handler != null && indexer != null) {
            List<Map<String, Object>> documents = (List<Map<String, Object>>) command.get("documents");
            if (existsNotEmpty(documents)) {
                result.putAll(deleteDocs(handler, documents, commit));
            }
        } else if (handler == null && existsNotEmpty(version)) {
            result.put("error", "index '" + command.get("index") + "(" + version + ")" + "' not found");
        } else if (handler == null) {
            result.put("error", "index '" + command.get("index") + "' not found");
        } else {
            result.put("error", "indexer for index '" + command.get("index") + "' not found");
        }
        return result;
    }

    private Map<String, Object> commitIndex(Map<String, Object> command) {
        Map<String, Object> result = new LinkedHashMap<>();
        String index = (String) command.get("index");
        String version = (String) command.get("version");

        IndexHandler handler;
        if (existsNotEmpty(version)) {
            handler = indexFactory.getHandler(index, version);
        } else {
            handler = indexFactory.getHandler(index);
        }

        if (handler != null) {
            new IndexHelper(handler).commit();
        } else if (existsNotEmpty(version)) {
            result.put("error", "index '" + command.get("index") + "(" + version + ")" + "' not found");
        } else {
            result.put("error", "index '" + command.get("index") + "' not found");
        }
        return result;
    }

    @Override
    public boolean canExecute(Map<String, Object> command) {
        if (command.containsKey("type")) {
            if (existsAndEqual((String) command.get("type"), "check")
                && existsNotEmpty((String) command.get("index"))) {
                return true;
            } else if (existsAndEqual((String) command.get("type"), "create")
                       && existsNotEmpty((String) command.get("index"))) {
                return true;
            } else if (existsAndEqual((String) command.get("type"), "switch")
                       && existsNotEmpty((String) command.get("index"))
                       && existsNotEmpty((String) command.get("version"))) {
                return true;
            } else if (existsAndEqual((String) command.get("type"), "merge")
                       && existsNotEmpty((String) command.get("index"))
                       && existsNotEmpty((String) command.get("version"))) {
                return true;
            } else if (existsAndEqual((String) command.get("type"), "add")
                       && existsNotEmpty((String) command.get("index"))
                       && existsNotEmpty((List) command.get("documents"))) {
                return true;
            } else if (existsAndEqual((String) command.get("type"), "update")
                       && existsNotEmpty((String) command.get("index"))
                       && existsNotEmpty((List) command.get("documents"))) {
                return true;
            } else if (existsAndEqual((String) command.get("type"), "delete")
                       && existsNotEmpty((String) command.get("index"))
                       && existsNotEmpty((List) command.get("documents"))) {
                return true;
            } else if (existsAndEqual((String) command.get("type"), "commit")
                       && existsNotEmpty((String) command.get("index"))) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Map<String, Object> execute(Map<String, Object> command) {
        if (command.get("type").equals("check")) {
            return checkIndex(command);
        } else if (command.get("type").equals("create")) {
            return createIndex(command);
        } else if (command.get("type").equals("switch")) {
            return switchIndex(command);
        } else if (command.get("type").equals("merge")) {
            return mergeIndex(command);
        } else if (command.get("type").equals("add")) {
            return addDocument(command);
        } else if (command.get("type").equals("update")) {
            return updateDocument(command);
        } else if (command.get("type").equals("delete")) {
            return deleteDocument(command);
        } else if (command.get("type").equals("commit")) {
            return commitIndex(command);
        } else {
            Map<String, Object> result = new LinkedHashMap<>();
            result.put("error", "command '" + command.get("type") + "' not found");
            return result;
        }
    }

    @Override
    public void close() {
        indexFactory.close();
    }

}
