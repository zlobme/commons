
package me.zlob.lucene.worker;

import java.util.*;

public class CombinedWorker implements Worker {

    private List<Worker> workers;

    public CombinedWorker(List<Worker> workers) {
        this.workers = workers;
    }

    @Override
    public boolean canExecute(Map<String, Object> command) {
        for (Worker worker : workers) {
            if (worker != null && worker.canExecute(command)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Map<String, Object> execute(Map<String, Object> command) {
        for (Worker worker : workers) {
            if (worker != null && worker.canExecute(command)) {
                return worker.execute(command);
            }
        }
        Map<String, Object> result = new LinkedHashMap<>();
        result.put("error", "command '" + command.get("type") + "' not found");
        return result;
    }

    @Override
    public void close() {
        for (Worker worker : workers) {
            worker.close();
        }
    }

}
