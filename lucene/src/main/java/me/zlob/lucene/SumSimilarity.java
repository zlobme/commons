
package me.zlob.lucene;

import org.apache.lucene.index.*;
import org.apache.lucene.search.similarities.*;

public class SumSimilarity extends ClassicSimilarity {

    @Override
    public float queryNorm(float sumOfSquaredWeights) {
        return 1;
    }

    @Override
    public float sloppyFreq(int distance) {
        return 1;
    }

    @Override
    public float tf(float freq) {
        return 1;
    }

    @Override
    public float idf(long docFreq, long numDocs) {
        return 1;
    }

    @Override
    public float coord(int overlap, int maxOverlap) {
        return 1;
    }

    @Override
    public float lengthNorm(FieldInvertState state) {
        return 1;
    }

}
