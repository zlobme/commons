
package me.zlob.lucene;

import java.io.*;
import java.nio.file.*;

import org.slf4j.*;

import org.apache.lucene.analysis.standard.*;
import org.apache.lucene.facet.taxonomy.*;
import org.apache.lucene.facet.taxonomy.directory.*;
import org.apache.lucene.index.*;
import org.apache.lucene.search.*;
import org.apache.lucene.search.similarities.*;
import org.apache.lucene.store.*;

public class IndexHandler {

    private static final Logger log = LoggerFactory.getLogger(IndexHandler.class);

    private String path;

    private IndexReader reader;

    private IndexWriter writer;

    private TaxonomyReader taxonomyReader;

    private TaxonomyWriter taxonomyWriter;

    private volatile boolean chechUpdates = false;

    public static void closeReader(IndexReader reader) {
        if (reader != null) {
            try {
                reader.close();
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
            }
        }
    }

    public static void closeReaderDelayed(IndexReader reader, long after) {
        if (reader != null) {
            final IndexReader closingReader = reader;
            new Thread(() -> {
                try {
                    Thread.sleep(after);
                    closeReader(closingReader);
                    log.info("old reader closed");
                } catch (Exception ex) {
                    log.error(ex.getMessage(), ex);
                }
            }).start();
        }
    }

    public static void closeWriter(IndexWriter writer) {
        if (writer != null) {
            try {
                writer.close();
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
            }
        }
    }

    public static void closeReader(TaxonomyReader reader) {
        if (reader != null) {
            try {
                reader.close();
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
            }
        }
    }

    public static void closeReaderDelayed(TaxonomyReader reader, long after) {
        if (reader != null) {
            final TaxonomyReader closingReader = reader;
            new Thread(() -> {
                try {
                    Thread.sleep(after);
                    closeReader(closingReader);
                    log.info("old reader closed");
                } catch (Exception ex) {
                    log.error(ex.getMessage(), ex);
                }
            }).start();
        }
    }

    public static void closeWriter(TaxonomyWriter writer) {
        if (writer != null) {
            try {
                writer.close();
            } catch (NoSuchFileException ex) {
                //log.error("taxonomy directory doesn't exists");
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
            }
        }
    }

    public static void closeSearcher(IndexSearcher searcher) {
        if (searcher != null) {
            closeReader(searcher.getIndexReader());
        }
    }

    public IndexHandler(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public IndexReader getReader() {
        if (reader == null) {
            try {
                Directory directory = new MMapDirectory(new File(path).toPath());
                reader = DirectoryReader.open(directory);
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
            }
        } else if (chechUpdates) {
            try {
                long start = System.currentTimeMillis();
                IndexReader newReader = DirectoryReader.openIfChanged((DirectoryReader) reader);
                long stop = System.currentTimeMillis();
                log.debug("reopen reader: " + (stop - start) + "ms");
                if (newReader != null) {
                    if (newReader != reader) {
                        closeReaderDelayed(reader, 5000);
                        reader = newReader;
                        log.debug("reader reopened");
                        closeTaxonomyReader();
                        log.debug("taxonomy reader closed");
                    }
                }
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
            }
            chechUpdates = false;
        }
        return reader;
    }

    public IndexWriter getWriter() {
        return getWriter(false, false);
    }

    public IndexWriter getWriter(boolean reopen) {
        return getWriter(reopen, false);
    }

    public IndexWriter getWriter(boolean reopen, boolean createNew) {
        if (reopen) {
            closeWriter();
        }
        if (writer == null || !writer.isOpen()) {
            try {
                Directory directory = new MMapDirectory(new File(path).toPath());
                IndexWriterConfig config = new IndexWriterConfig(new StandardAnalyzer());
                if (createNew) {
                    config.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
                } else {
                    config.setOpenMode(IndexWriterConfig.OpenMode.CREATE_OR_APPEND);
                }
                config.setIndexDeletionPolicy(new SnapshotDeletionPolicy(new KeepOnlyLastCommitDeletionPolicy()));
                writer = new IndexWriter(directory, config);
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
            }
        }
        chechUpdates = true;
        return writer;
    }

    public TaxonomyReader getTaxonomyReader() {
        if (taxonomyReader == null) {
            try {
                String taxonomyPath = (path.endsWith("/") ? path + "taxonomy" : path + "/taxonomy");
                Directory directory = new MMapDirectory(new File(taxonomyPath).toPath());
                taxonomyReader = new DirectoryTaxonomyReader(directory);
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
            }
        }
        return taxonomyReader;
    }

    public TaxonomyWriter getTaxonomyWriter() {
        return getTaxonomyWriter(false, false);
    }

    public TaxonomyWriter getTaxonomyWriter(boolean reopen) {
        return getTaxonomyWriter(reopen, false);
    }

    public TaxonomyWriter getTaxonomyWriter(boolean reopen, boolean createNew) {
        if (reopen) {
            closeTaxonomyWriter();
        }
        if (taxonomyWriter == null) {
            try {
                String taxonomyPath = (path.endsWith("/") ? path + "taxonomy" : path + "/taxonomy");
                Directory directory = new MMapDirectory(new File(taxonomyPath).toPath());

                IndexWriterConfig.OpenMode openMode;
                if (createNew) {
                    openMode = IndexWriterConfig.OpenMode.CREATE;
                } else {
                    openMode = IndexWriterConfig.OpenMode.CREATE_OR_APPEND;
                }

                taxonomyWriter = new DirectoryTaxonomyWriter(directory, openMode);
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
            }
        }
        return taxonomyWriter;
    }

    public IndexSearcher getSearcher() {
        return getSearcher(new BoostSimilariry());
    }

    public IndexSearcher getSearcher(Similarity similarity) {
        IndexSearcher result = null;
        IndexReader readerInstance = getReader();
        if (readerInstance != null) {
            result = new IndexSearcher(readerInstance);
            if (similarity != null) {
                result.setSimilarity(similarity);
            }
        }
        return result;
    }

    public void close() {
        closeReader();
        closeWriter();
        closeTaxonomyWriter();
    }

    public void closeReader() {
        closeReader(reader);
        reader = null;
    }

    public void closeWriter() {
        closeWriter(writer);
        writer = null;
    }

    public void closeTaxonomyReader() {
        closeReader(taxonomyReader);
        taxonomyReader = null;
    }

    public void closeTaxonomyWriter() {
        closeWriter(taxonomyWriter);
        taxonomyWriter = null;
    }

}
