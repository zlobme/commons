
package me.zlob.lucene.client;

import java.util.*;

public interface IndexClient {

    public Map<String, Object> request(Map<String, Object> command);

}
