
package me.zlob.lucene.client;

import java.util.*;

import com.google.gson.*;

import me.zlob.util.*;

import static me.zlob.util.Conditions.existsNotEmpty;
import static me.zlob.util.Util.*;

public class RemoteIndexClient implements IndexClient {

    private static final Gson gson = GsonFactory.getGson();

    private String url;

    private String toJson(Map<String, Object> object) {
        return gson.toJson(object);
    }

    private Map<String, Object> fromJson(String json) {
        return (Map<String, Object>) gson.fromJson(json, Object.class);
    }

    public RemoteIndexClient(String url) {
        this.url = url;
    }

    @Override
    public Map<String, Object> request(Map<String, Object> command) {
        String responce = IO.fetch(url, toJson(command));
        return (existsNotEmpty(responce) ? fromJson(responce) : Collections.EMPTY_MAP);
    }

}
