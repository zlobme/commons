
package me.zlob.lucene.client;

import java.util.*;

import org.slf4j.*;

import me.zlob.lucene.worker.*;

public class LocalIndexClient implements IndexClient {

    private static final Logger log = LoggerFactory.getLogger(LocalIndexClient.class);

    private List<Worker> workers;

    public LocalIndexClient(List<Worker> workers) {
        this.workers = workers;
    }

    @Override
    public Map<String, Object> request(Map<String, Object> command) {
        Map<String, Object> result = new LinkedHashMap();
        for (Worker worker : workers) {
            if (worker.canExecute(command)) {
                log.debug("execute");
                try {
                    result = worker.execute(command);
                } catch (Exception ex) {
                    log.error(ex.getMessage(), ex);
                    result.put("error", "some errors happens");
                    result.put("type", ex.getClass().getSimpleName());
                    result.put("message", ex.getMessage());
                    if (command.get("debug") != null) {
                        String cause = getCause(ex.getStackTrace());
                        result.put("line", cause);
                    }
                }
                break;
            }
        }
        return result;
    }

    private String getCause(StackTraceElement[] stackTrace) {
        return Arrays.stream(stackTrace)
            .filter(line -> line.getClassName().startsWith("me.")
                            || line.getClassName().startsWith("ru."))
            .map(StackTraceElement::toString)
            .findFirst().orElse("");
    }

}
