
package me.zlob.lucene;

import java.io.*;

import org.apache.lucene.util.*;
import org.apache.lucene.index.*;
import org.apache.lucene.search.*;
import org.apache.lucene.search.similarities.*;

public class BoostSimilariry extends Similarity {

    @Override
    public long computeNorm(FieldInvertState state) {
        return new Float(state.getBoost()).longValue();
    }

    @Override
    public SimScorer simScorer(SimWeight weight, LeafReaderContext context) throws IOException {
        return new SimScorer() {
            @Override
            public float score(int doc, float freq) {
                return 1;
            }

            @Override
            public float computeSlopFactor(int distance) {
                return 1;
            }

            @Override
            public float computePayloadFactor(int doc, int start, int end, BytesRef payload) {
                return 1;
            }

        };
    }


    @Override
    public SimWeight computeWeight(CollectionStatistics collectionStats, TermStatistics... termStats) {
        return new SimWeight() {
            @Override
            public float getValueForNormalization() {
                return 1;
            }

            @Override
            public void normalize(float queryNorm, float boost) {
                queryNorm += boost;
            }

        };
    }

}
