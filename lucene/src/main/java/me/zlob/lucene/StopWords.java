
package me.zlob.lucene;

import java.util.*;

public class StopWords {

    private static final Set<String> instance = new HashSet();

    static {
        instance.add("без");
        instance.add("в");
        instance.add("вместо");
        instance.add("вне");
        instance.add("во");
        instance.add("для");
        instance.add("до");
        instance.add("за");
        instance.add("из");
        instance.add("к");
        instance.add("ко");
        instance.add("кроме");
        instance.add("между");
        instance.add("над");
        instance.add("ниже");
        instance.add("о");
        instance.add("ооо");
        instance.add("об");
        instance.add("обо");
        instance.add("около");
        instance.add("от");
        instance.add("по");
        instance.add("под");
        instance.add("подо");
        instance.add("после");
        instance.add("при");
        instance.add("про");
        instance.add("с");
        instance.add("со");
        instance.add("среди");
        instance.add("у");
        instance.add("и");
        instance.add("ип");
        instance.add("на");
        instance.add("лев");
        instance.add("лево");
        instance.add("прав");
        instance.add("право");
        instance.add("перед");
        instance.add("передн");
        instance.add("зад");
        instance.add("задн");
        instance.add("верх");
        instance.add("верхн");
        instance.add("низ");
        instance.add("нижн");
        instance.add("a");
        instance.add("am");
        instance.add("an");
        instance.add("and");
        instance.add("are");
        instance.add("as");
        instance.add("at");
        instance.add("be");
        instance.add("been");
        instance.add("but");
        instance.add("by");
        instance.add("can");
        instance.add("co");
        instance.add("do");
        instance.add("done");
        instance.add("due");
        instance.add("eg");
        instance.add("etc");
        instance.add("from");
        instance.add("had");
        instance.add("has");
        instance.add("have");
        instance.add("i");
        instance.add("ie");
        instance.add("if");
        instance.add("in");
        instance.add("inc");
        instance.add("is");
        instance.add("no");
        instance.add("of");
        instance.add("on");
        instance.add("or");
        instance.add("re");
        instance.add("so");
        instance.add("the");
        instance.add("to");
    }

    public static Set<String> getInstance() {
        return instance;
    }

}
