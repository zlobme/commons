
package me.zlob.jdbc;

import java.sql.*;

import org.slf4j.*;

import lombok.experimental.*;

@UtilityClass
public class JdbcResourceHelper {

    private static final Logger log = LoggerFactory.getLogger(JdbcResourceHelper.class);

    public static boolean isClosed(ResultSet resultSet) {
        boolean result = false;
        try {
            result = resultSet == null || resultSet.isClosed();
        } catch (SQLException ex) {
            log.error(ex.getMessage(), ex);
        }
        return result;
    }

    public static boolean isClosed(Statement state) {
        boolean result = false;
        try {
            result = state == null || state.isClosed();
        } catch (SQLException ex) {
            log.error(ex.getMessage(), ex);
        }
        return result;
    }

    public static boolean isClosed(Connection connection) {
        boolean result = false;
        try {
            result = connection == null || connection.isClosed();
        } catch (SQLException ex) {
            log.error(ex.getMessage(), ex);
        }
        return result;
    }

    public static void close(ResultSet resultSet) {
        if (resultSet != null && !isClosed(resultSet)) {
            try {
                resultSet.close();
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
            }
        }
    }

    public static void close(Statement state) {
        if (state != null && !isClosed(state)) {
            try {
                state.close();
            } catch (SQLException ex) {
                log.error(ex.getMessage(), ex);
            }
        }
    }

    public static void close(Connection connection) {
        if (connection != null && !isClosed(connection)) {
            try {
                connection.close();
            } catch (SQLException ex) {
                log.error(ex.getMessage(), ex);
            }
        }
    }

    public static void closeAll(ResultSet resultSet) {
        if (resultSet != null && !isClosed(resultSet)) {
            Statement statement = getStatement(resultSet);
            close(resultSet);
            closeAll(statement);
        }
    }

    private static Statement getStatement(ResultSet resultSet) {
        Statement state = null;
        if (resultSet != null && !isClosed(resultSet)) {
            try {
                state = resultSet.getStatement();
            } catch (SQLException ex) {
                log.error(ex.getMessage(), ex);
            }
        }
        return state;
    }

    public static void closeAll(Statement statement) {
        if (statement != null && !isClosed(statement)) {
            Connection connection = getConnection(statement);
            close(statement);
            close(connection);
        }
    }

    private static Connection getConnection(Statement state) {
        Connection connection = null;
        if (state != null && !isClosed(state)) {
            try {
                connection = state.getConnection();
            } catch (SQLException ex) {
                log.error(ex.getMessage(), ex);
            }
        }
        return connection;
    }

}
