
package me.zlob.jdbc;

import java.sql.*;

import me.zlob.util.*;

public class ConnectionResource extends Resource<Connection> {

    public static Resource wrap(Connection origin) {
        return new ConnectionResource(origin);
    }

    public ConnectionResource(Connection origin) {
        super(origin);
    }

    @Override
    public void close() {
        JdbcResourceHelper.close(getOrigin());
    }

}
