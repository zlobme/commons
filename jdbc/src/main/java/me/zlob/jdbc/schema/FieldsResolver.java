
package me.zlob.jdbc.schema;

import java.lang.reflect.*;
import java.util.*;
import java.util.function.*;
import java.util.stream.*;

import org.slf4j.*;

import me.zlob.collections.*;
import me.zlob.jdbc.orm.*;

public class FieldsResolver {

    private static final Logger log = LoggerFactory.getLogger(FieldsResolver.class);

    private static final Map<String, Function<Object, String>> MAPPINGS;

    static {
        MAPPINGS = new HashMap<>();
        MAPPINGS.put("byte", Object::toString);
        MAPPINGS.put("short", Object::toString);
        MAPPINGS.put("int", Object::toString);
        MAPPINGS.put("long", Object::toString);
        MAPPINGS.put("float", Object::toString);
        MAPPINGS.put("double", Object::toString);
        MAPPINGS.put("boolean", Object::toString);
        MAPPINGS.put("char", Object::toString);
        MAPPINGS.put("java.lang.String", Object::toString);
        MAPPINGS.put("java.lang.Byte", Object::toString);
        MAPPINGS.put("java.lang.Short", Object::toString);
        MAPPINGS.put("java.lang.Integer", Object::toString);
        MAPPINGS.put("java.lang.Long", Object::toString);
        MAPPINGS.put("java.lang.Float", Object::toString);
        MAPPINGS.put("java.lang.Double", Object::toString);
        MAPPINGS.put("java.lang.Boolean", Object::toString);
        MAPPINGS.put("java.lang.Character", Object::toString);
        MAPPINGS.put("java.lang.Enum", origin -> ((Enum) origin).name());
    }

    public boolean isEntity(Class<?> clazz) {
        Class<?> iface = Arrays.stream(clazz.getInterfaces())
            .filter(Entity.class::equals)
            .findFirst().orElse(null);
        return iface != null;
    }

    public Map<String, TableField> getFields(Class<?> clazz) {
        Field[] declaredFields = clazz.getDeclaredFields();

        Map<String, TableField> fields = Arrays.stream(declaredFields)
            .map(f -> new TableField(f.getName(), f.getType().getName()))
            .collect(Collectors.toMap(TableField::getName, Containers::same));

        for (TableField field : fields.values()) {
            log.debug("%-15{}: {}", field.getName(), field.getType());
        }
        return fields;
    }

    private String determineType(Field field) {
        String result = field.getType().getName();
        if (!MAPPINGS.keySet().contains(result)) {
            Class<?> clazz = field.getType();
            while (!clazz.getTypeName().equals("java.lang.Object")) {
                clazz = clazz.getSuperclass();
            }
        }
        return result;
    }

}
