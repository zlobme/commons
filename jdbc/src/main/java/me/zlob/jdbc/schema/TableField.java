
package me.zlob.jdbc.schema;

import lombok.*;

public class TableField {

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private String type;

    public TableField() {
    }

    public TableField(String name, String type) {
        this.name = name;
        this.type = type;
    }

}
