package me.zlob.jdbc.pool;

import java.sql.*;

import lombok.*;
import lombok.experimental.Delegate;

import static me.zlob.util.Conditions.*;

public class DelegatedResultSet implements ResultSet {

    @Delegate
    @Getter(AccessLevel.PROTECTED)
    private ResultSet origin;

    public DelegatedResultSet(ResultSet origin) {
        this.origin = checkNotNull(origin, "origin");
    }

}
