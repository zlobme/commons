package me.zlob.jdbc.pool;

import java.sql.*;

import lombok.*;
import lombok.experimental.Delegate;

import static me.zlob.util.Conditions.*;

public class DelegatedStatement implements Statement {

    @Delegate
    @Getter(AccessLevel.PROTECTED)
    private Statement origin;

    public DelegatedStatement(Statement origin) {
        this.origin = checkNotNull(origin, "origin");
    }

}
