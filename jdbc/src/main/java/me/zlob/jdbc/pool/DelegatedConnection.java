package me.zlob.jdbc.pool;

import java.sql.*;

import lombok.*;
import lombok.experimental.Delegate;

import static me.zlob.util.Conditions.*;

public class DelegatedConnection implements Connection {

    @Delegate
    @Getter(AccessLevel.PROTECTED)
    private Connection origin;

    public DelegatedConnection(Connection origin) {
        this.origin = checkNotNull(origin, "origin");
    }

}
