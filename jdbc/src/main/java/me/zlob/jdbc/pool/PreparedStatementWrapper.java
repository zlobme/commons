
package me.zlob.jdbc.pool;

import java.sql.*;

import static me.zlob.util.Conditions.*;

public class PreparedStatementWrapper extends DelegatedPreparedStatement {

    private PooledDataSource pooledDataSource;

    public PreparedStatementWrapper(PooledDataSource pooledDataSource, PreparedStatement origin) {
        super(origin);
        this.pooledDataSource = checkNotNull(pooledDataSource, "pooledDataSource");
    }

    @Override
    public Connection getConnection() throws SQLException {
        return new ConnectionWrapper(pooledDataSource, getOrigin().getConnection());
    }

    @Override
    public ResultSet executeQuery(String sql) throws SQLException {
        return new ResultSetWrapper(pooledDataSource, getOrigin().executeQuery(sql));
    }

    @Override
    public ResultSet getResultSet() throws SQLException {
        return new ResultSetWrapper(pooledDataSource, getOrigin().getResultSet());
    }

    @Override
    public ResultSet getGeneratedKeys() throws SQLException {
        return new ResultSetWrapper(pooledDataSource, getOrigin().getGeneratedKeys());
    }

    @Override
    public ResultSet executeQuery() throws SQLException {
        return new ResultSetWrapper(pooledDataSource, getOrigin().executeQuery());
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        return getOrigin().isWrapperFor(iface);
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        return getOrigin().unwrap(iface);
    }

}
