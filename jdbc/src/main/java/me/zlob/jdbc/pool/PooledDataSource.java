
package me.zlob.jdbc.pool;

import java.sql.*;
import java.time.*;
import java.util.*;
import java.util.concurrent.*;

import javax.sql.*;

import org.slf4j.*;

import static me.zlob.jdbc.JdbcResourceHelper.*;
import static me.zlob.util.Conditions.*;

public class PooledDataSource extends DelegatedDataSource {

    private static final Logger log = LoggerFactory.getLogger(PooledDataSource.class);

    private static final Duration DEFAULT_EXPIRE = Duration.ofSeconds(1800);

    private Duration expire;

    private Map<Connection, Long> locked = new ConcurrentHashMap<>();

    private Map<Connection, Long> unlocked = new ConcurrentHashMap<>();

    public PooledDataSource(DataSource origin) {
        this(origin, DEFAULT_EXPIRE);
    }

    public PooledDataSource(DataSource origin, int expireSeconds) {
        this(origin, Duration.ofSeconds(expireSeconds));
    }

    public PooledDataSource(DataSource origin, Duration expire) {
        super(origin);
        this.expire = checkNotNull(expire, "expire");
    }

    public int getActiveCount() {
        return locked.size();
    }

    public int getIdleCount() {
        return unlocked.size();
    }

    @Override
    public Connection getConnection() throws SQLException {
        return checkOut();
    }

    @Override
    public Connection getConnection(String username, String password) throws SQLException {
        log.info("Get connection for user '{}' ignored, used default user instead.", username);
        return checkOut();
    }

    private synchronized Connection checkOut() throws SQLException {
        Connection result = null;
        long now = now();
        if (unlocked.size() > 0) {
            for (Connection connection : unlocked.keySet()) {
                result = connection;
                Duration duration = Duration.ofNanos(now - unlocked.get(result));
                if (expire.minus(duration).isNegative()) {
                    unlocked.remove(result);
                    expire(result);
                    result = null;
                } else {
                    if (validate(result)) {
                        unlocked.remove(result);
                        locked.put(result, now);
                        result = new ConnectionWrapper(this, result);
                        break;
                    } else {
                        unlocked.remove(result);
                        expire(result);
                        result = null;
                    }
                }
            }
        }
        if (result == null) {
            Connection newConnection = create();
            locked.put(newConnection, now);
            result = new ConnectionWrapper(this, newConnection);
        }
        return result;
    }

    synchronized void checkIn(Connection connection) {
        locked.remove(connection);
        unlocked.put(connection, now());
    }

    private Connection create() throws SQLException {
        return getOrigin().getConnection();
    }

    private void expire(Connection connection) throws SQLException {
        if (connection != null && !isClosed(connection)) {
            connection.close();
        }
    }

    private boolean validate(Connection connection) throws SQLException {
        return connection != null && !connection.isClosed();
    }

    private long now() {
        return System.nanoTime();
    }

}
