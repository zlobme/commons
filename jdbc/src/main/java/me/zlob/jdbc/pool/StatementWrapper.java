
package me.zlob.jdbc.pool;

import java.sql.*;

import static me.zlob.util.Conditions.*;

public class StatementWrapper extends DelegatedStatement {

    private PooledDataSource pooledDataSource;

    public StatementWrapper(PooledDataSource pooledDataSource, Statement origin) {
        super(origin);
        this.pooledDataSource = checkNotNull(pooledDataSource, "pooledDataSource");
    }

    @Override
    public Connection getConnection() throws SQLException {
        return new ConnectionWrapper(pooledDataSource, getOrigin().getConnection());
    }

    @Override
    public ResultSet executeQuery(String sql) throws SQLException {
        return new ResultSetWrapper(pooledDataSource, getOrigin().executeQuery(sql));
    }

    @Override
    public ResultSet getResultSet() throws SQLException {
        return new ResultSetWrapper(pooledDataSource, getOrigin().getResultSet());
    }

    @Override
    public ResultSet getGeneratedKeys() throws SQLException {
        return new ResultSetWrapper(pooledDataSource, getOrigin().getGeneratedKeys());
    }

}
