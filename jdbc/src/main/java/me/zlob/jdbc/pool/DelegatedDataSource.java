package me.zlob.jdbc.pool;

import javax.sql.*;

import lombok.*;
import lombok.experimental.Delegate;

import static me.zlob.util.Conditions.*;

public class DelegatedDataSource implements DataSource {

    @Delegate
    @Getter(AccessLevel.PROTECTED)
    private DataSource origin;

    public DelegatedDataSource(DataSource origin) {
        this.origin = checkNotNull(origin, "origin");
    }

}
