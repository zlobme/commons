package me.zlob.jdbc.pool;

import java.sql.*;

import lombok.*;
import lombok.experimental.Delegate;

import static me.zlob.util.Conditions.*;

public abstract class DelegatedPreparedStatement implements PreparedStatement {

    @Delegate
    @Getter(AccessLevel.PROTECTED)
    private PreparedStatement origin;

    public DelegatedPreparedStatement(PreparedStatement origin) {
        this.origin = checkNotNull(origin, "origin");
    }

}
