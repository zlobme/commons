
package me.zlob.jdbc.pool;

import java.sql.*;

import static me.zlob.util.Conditions.*;

public class ResultSetWrapper extends DelegatedResultSet {

    private PooledDataSource pooledDataSource;

    public ResultSetWrapper(PooledDataSource pooledDataSource, ResultSet origin) {
        super(origin);
        this.pooledDataSource = checkNotNull(pooledDataSource, "pooledDataSource");
    }

    @Override
    public Statement getStatement() throws SQLException {
        return new StatementWrapper(pooledDataSource, getOrigin().getStatement());
    }

}
