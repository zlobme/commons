
package me.zlob.jdbc;

import java.sql.*;

@FunctionalInterface
public interface StatementInitializer {

    public void init(PreparedStatement statement) throws SQLException;

}
