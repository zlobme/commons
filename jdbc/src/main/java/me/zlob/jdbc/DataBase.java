
package me.zlob.jdbc;

import java.sql.*;
import java.util.*;

import javax.sql.*;

import static java.sql.Statement.*;

import org.slf4j.*;

import lombok.*;

import me.zlob.jdbc.row.*;

import static me.zlob.jdbc.JdbcResourceHelper.*;
import static me.zlob.util.Conditions.*;

public class DataBase {

    private static final Logger log = LoggerFactory.getLogger(DataBase.class);

    private static final int DEFAULT_MAX_RETRIES = 10;

    @Getter
    private DataSource dataSource;

    private int maxRetries;

    public DataBase(DataSource dataSource) {
        this(dataSource, DEFAULT_MAX_RETRIES);
    }

    public DataBase(DataSource dataSource, int maxRetries) {
        this.dataSource = checkNotNull(dataSource, "dataSource");
        this.maxRetries = maxRetries;
    }

    public boolean query(String sql) {
        Connection connection = getConnection();
        boolean result = connection != null && query(connection, sql);
        close(connection);
        return result;
    }

    private boolean query(Connection connection, String sql) {
        Statement statement = createStatement(connection);
        boolean result = statement != null && query(statement, sql);
        closeAll(statement);
        return result;
    }

    private boolean query(Statement state, String sql) {
        boolean result = false;
        int retries = 0;
        SQLException exception = null;
        while (!result && retries++ < maxRetries) {
            try {
                state.execute(sql);
                result = true;
            } catch (SQLException ex) {
                exception = ex;
            }
        }
        if (exception != null && (!result || retries == maxRetries)) {
            log.error(exception.getMessage(), exception);
        }
        return result;
    }

    public boolean query(PreparedStatement state) {
        boolean result = false;
        int retries = 0;
        SQLException exception = null;
        while (!result && retries++ < maxRetries) {
            try {
                state.execute();
                result = true;
            } catch (SQLException ex) {
                exception = ex;
            }
        }
        if (exception != null && (!result || retries == maxRetries)) {
            log.error(exception.getMessage(), exception);
        }
        return result;
    }

    public boolean query(PreparedStatement state, StatementInitializer init) {
        return query(state, init, false);
    }

    public boolean queryBatch(PreparedStatement state, StatementInitializer init) {
        return query(state, init, true);
    }

    private boolean query(PreparedStatement state, StatementInitializer init, boolean batch) {
        boolean result = false;
        int retries = 0;
        SQLException exception = null;
        while (!result && retries++ < maxRetries) {
            try {
                init.init(state);
                if (batch) {
                    state.executeBatch();
                } else {
                    state.execute();
                }
                result = true;
            } catch (SQLException ex) {
                exception = ex;
            } finally {
                clearStatement(state);
                if (batch) {
                    clearBatch(state);
                }
            }
        }
        if (exception != null && (!result || retries == maxRetries)) {
            log.error(exception.getMessage(), exception);
        }
        return result;
    }

    public <Type> Type selectOne(String sql, RowGetter<Type> mapper) {
        ResultSet resultSet = select(sql);
        Type result = mapper.getObject(resultSet);
        closeAll(resultSet);
        return result;
    }

    public <Type> List<Type> selectMany(String sql, RowGetter<Type> mapper) {
        ResultSet resultSet = select(sql);
        List<Type> result = mapper.getObjects(resultSet);
        closeAll(resultSet);
        return result;
    }

    public <Type> Type selectOne(PreparedStatement state, RowGetter<Type> mapper) {
        ResultSet resultSet = select(state);
        Type result = mapper.getObject(resultSet);
        close(resultSet);
        return result;
    }

    public <Type> Type selectOne(PreparedStatement state, RowGetter<Type> mapper, StatementInitializer init) {
        ResultSet resultSet = select(state, init);
        Type result = mapper.getObject(resultSet);
        close(resultSet);
        return result;
    }

    public <Type> List<Type> selectMany(PreparedStatement state, RowGetter<Type> mapper) {
        ResultSet resultSet = select(state);
        List<Type> result = mapper.getObjects(resultSet);
        close(resultSet);
        return result;
    }

    public <Type> List<Type> selectMany(PreparedStatement state, RowGetter<Type> mapper, StatementInitializer init) {
        ResultSet resultSet = select(state, init);
        List<Type> result = mapper.getObjects(resultSet);
        close(resultSet);
        return result;
    }

    public ResultSet select(String sql) {
        return select(getConnection(), sql);
    }

    public ResultSet select(Connection connection, String sql) {
        return nullable(createStatement(connection))
            .map(state -> select(state, sql))
            .orElse(null);
    }

    public ResultSet select(Statement state, String sql) {
        ResultSet result = null;
        int retries = 0;
        boolean executed = false;
        SQLException exception = null;
        while (!executed && retries++ < maxRetries) {
            try {
                result = state.executeQuery(sql);
                executed = true;
            } catch (SQLException ex) {
                exception = ex;
            }
        }
        if (exception != null && (!executed || retries == maxRetries)) {
            log.error(exception.getMessage(), exception);
        }
        return result;
    }

    public ResultSet select(PreparedStatement state) {
        ResultSet result = null;
        int retries = 0;
        boolean executed = false;
        SQLException exception = null;
        while (!executed && retries++ < maxRetries) {
            try {
                result = state.executeQuery();
                executed = true;
            } catch (SQLException ex) {
                exception = ex;
            }
        }
        if (exception != null && (!executed || retries == maxRetries)) {
            log.error(exception.getMessage(), exception);
        }
        return result;
    }

    public ResultSet select(PreparedStatement state, StatementInitializer init) {
        ResultSet result = null;
        int retries = 0;
        boolean executed = false;
        SQLException exception = null;
        while (!executed && retries++ < maxRetries) {
            try {
                init.init(state);
                result = state.executeQuery();
                executed = true;
            } catch (SQLException ex) {
                exception = ex;
            } finally {
                clearStatement(state);
            }
        }
        if (exception != null && (!executed || retries == maxRetries)) {
            log.error(exception.getMessage(), exception);
        }
        return result;
    }

    public Connection getConnection() {
        Connection result = null;
        int retries = 0;
        while (result == null && retries++ < maxRetries) {
            try {
                result = dataSource.getConnection();
            } catch (SQLException ex) {
                log.error(ex.getMessage(), ex);
            }
        }
        return result;
    }

    public PreparedStatement prepareStatement(String sql) {
        return nullable(getConnection())
            .map(c -> prepareStatement(c, sql))
            .orElse(null);
    }

    private PreparedStatement prepareStatement(Connection connection, String sql) {
        PreparedStatement result = null;
        if (connection != null) {
            try {
                result = connection.prepareStatement(sql, RETURN_GENERATED_KEYS);
            } catch (SQLException ex) {
                log.error(ex.getMessage(), ex);
            }
        }
        return result;
    }

    private Statement createStatement(Connection connection) {
        Statement result = null;
        if (connection != null) {
            try {
                result = connection.createStatement();
            } catch (SQLException ex) {
                log.error(ex.getMessage(), ex);
            }
        }
        return result;
    }

    private PreparedStatement createStatement(Connection connection, String sql) {
        PreparedStatement result = null;
        if (connection != null) {
            try {
                result = connection.prepareStatement(sql, RETURN_GENERATED_KEYS);
            } catch (SQLException ex) {
                log.error(ex.getMessage(), ex);
            }
        }
        return result;
    }

    private void clearStatement(PreparedStatement state) {
        if (state != null) {
            try {
                state.clearParameters();
            } catch (SQLException ex) {
                log.error(ex.getMessage(), ex);
            }
        }
    }

    private void clearBatch(PreparedStatement state) {
        if (state != null) {
            try {
                state.clearBatch();
            } catch (SQLException ex) {
                log.error(ex.getMessage(), ex);
            }
        }
    }

    public static Integer getRowCount(ResultSet resultSet) {
        int result = 0;
        try {
            if (resultSet != null && resultSet.last()) {
                result = resultSet.getRow();
                resultSet.beforeFirst();
            }
        } catch (SQLException ex) {
            log.error(ex.getMessage(), ex);
        }
        return result;
    }

    public static <Id> Id getGeneratedKey(PreparedStatement state) {
        Id result = null;
        try (ResultSet rs = state.getGeneratedKeys()) {
            if (rs != null && rs.first()) {
                result = (Id) rs.getObject(1);
            }
        } catch (SQLException ex) {
            log.error(ex.getMessage(), ex);
        }
        return result;
    }

    public static <Id> List<Id> getGeneratedKeys(PreparedStatement state) {
        List<Id> result = Collections.emptyList();
        try (ResultSet rs = state.getGeneratedKeys()) {
            result = new ArrayList<>();
            if (rs != null) {
                rs.beforeFirst();
                while (rs.next()) {
                    Id id = (Id) rs.getObject(1);
                    result.add(id);
                }
            }
        } catch (SQLException ex) {
            log.error(ex.getMessage(), ex);
        }
        return result;
    }

}
