
package me.zlob.jdbc.orm;

import java.util.*;

import me.zlob.collections.*;

import static me.zlob.collections.Containers.*;

public class RawGenericDao<Id, Type extends Entity<Id>> implements GenericDao<Id, Type> {

    private Map<String, EntityDao> daos;

    public RawGenericDao(Map<String, EntityDao> daos) {
        this.daos = daos;
    }

    @Override
    public Type select(Id id, Class clazz) {
        return select(id, clazz.getSimpleName());
    }

    @Override
    public Type select(Id id, String clazz) {
        Type result = null;
        EntityDao<Id, Type> dao = daos.get(clazz);
        if (dao != null) {
            result = dao.selectById(id);
        }
        return result;
    }

    @Override
    public List<Type> select(Set<Id> ids, Class clazz) {
        return select(ids, clazz.getSimpleName());
    }

    @Override
    public List<Type> select(Set<Id> ids, String clazz) {
        List<Type> result = null;
        EntityDao<Id, Type> dao = daos.get(clazz);
        if (dao != null) {
            result = dao.selectById(toList(ids));
        }
        return result;
    }

    @Override
    public Map<String, List<Type>> get(Batch<Id> batch) {
        Map<String, List<Type>> result = new HashMap<>();
        for (String key : batch.getClasses()) {
            List<Type> beans = select(batch.getIds(key), key);
            result.put(key, beans);
        }
        return result;
    }

    private Id getKey(Type entity) {
        return entity.getId();
    }

}
