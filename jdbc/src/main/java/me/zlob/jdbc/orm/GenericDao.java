
package me.zlob.jdbc.orm;

import java.util.*;

import me.zlob.collections.*;

public interface GenericDao<Id, Type extends Entity<Id>> {

    public Type select(Id id, Class clazz);

    public Type select(Id id, String clazz);

    public List<Type> select(Set<Id> ids, Class clazz);

    public List<Type> select(Set<Id> ids, String clazz);

    public Map<String, List<Type>> get(Batch<Id> batch);

}
