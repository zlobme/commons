
package me.zlob.jdbc.orm;

import java.sql.*;
import java.util.*;

import org.slf4j.*;

import me.zlob.jdbc.*;
import me.zlob.jdbc.row.*;
import me.zlob.util.*;

import static me.zlob.jdbc.DataBase.*;
import static me.zlob.jdbc.JdbcResourceHelper.*;
import static me.zlob.util.Conditions.*;
import static me.zlob.util.Exceptions.*;

public abstract class AbstractEntityDao<Id, Type extends Entity<Id>> implements EntityDao<Id, Type> {

    private static final Logger log = LoggerFactory.getLogger(AbstractEntityDao.class);

    private static final int DEFAULT_BATCH_SIZE = 1000;

    private DataBase db;

    private RowGetter<Type> getter;

    private EntitySetter<Id, Type> setter;

    private Map<String, String> templates;

    private int batchSize;

    public AbstractEntityDao(
        DataBase db,
        RowGetter<Type> getter,
        EntitySetter<Id, Type> setter,
        Map<String, String> templates
    ) {
        this(db, getter, setter, templates, DEFAULT_BATCH_SIZE);
    }

    public AbstractEntityDao(
        DataBase db,
        RowGetter<Type> getter,
        EntitySetter<Id, Type> setter,
        Map<String, String> templates,
        int batchSize
    ) {
        this.db = checkNotNull(db, "db");
        this.getter = checkNotNull(getter, "getter");
        this.setter = checkNotNull(setter, "setter");
        this.templates = checkArgNotEmpty(templates, "templates");
        this.batchSize = batchSize;
    }

    @Override
    public boolean insert(Type entity) {
        return entity.getId() != null
            ? insertWithId(entity)
            : insertWithoutId(entity);
    }

    private boolean insertWithId(Type entity) {
        PreparedStatement state = getStatement("insert-with-id");
        try {
            return db.query(state, setter.withIdFirst(entity));
        } finally {
            closeAll(state);
        }
    }

    private boolean insertWithoutId(Type entity) {
        PreparedStatement state = getStatement("insert-without-id");
        try {
            boolean result = db.query(state, setter.withoutId(entity));
            if (result) {
                Id id = getGeneratedKey(state);
                result = id != null;
                if (result) {
                    entity.setId(id);
                }
            }
            return result;
        } finally {
            closeAll(state);
        }
    }

    @Override
    public boolean insert(List<Type> entities) {
        List<Type> withIds = new ArrayList<>();
        List<Type> withoutIds = new ArrayList<>();

        for (Type entity : entities) {
            if (entity.getId() != null) {
                withIds.add(entity);
            } else {
                withoutIds.add(entity);
            }
        }

        boolean result = true;
        if (existsNotEmpty(withIds)) {
            result = insertWithId(withIds);
        }
        if (existsNotEmpty(withoutIds)) {
            result &= insertWithoutId(withoutIds);
        }
        return result;
    }

    private boolean insertWithId(List<Type> entities) {
        PreparedStatement state = getStatement("insert-with-id");
        try {
            boolean result = true;
            Page page = new Page(0, batchSize).setCount(entities.size());
            while (page.next().getPage() <= page.pages()) {
                List<Type> batch = entities.subList(page.offset(), page.limit());
                result &= insertWithId(state, batch);
            }
            return result;
        } finally {
            closeAll(state);
        }
    }

    private boolean insertWithId(PreparedStatement state, List<Type> entities) {
        return db.queryBatch(state, setter.withIdFirst(entities));
    }

    private boolean insertWithoutId(List<Type> entities) {
        PreparedStatement state = getStatement("insert-without-id");
        try {
            boolean result = true;
            Page page = new Page(0, batchSize).setCount(entities.size());
            while (page.next().getPage() <= page.pages()) {
                List<Type> batch = entities.subList(page.offset(), page.limit());
                result &= insertWithoutId(state, batch);
            }
            return result;
        } finally {
            closeAll(state);
        }
    }

    private boolean insertWithoutId(PreparedStatement state, List<Type> entities) {
        return db.queryBatch(state, setter.withoutId(entities));
    }

    @Override
    public boolean update(Type entity) {
        PreparedStatement state = getStatement("update-by-id");
        try {
            return db.query(state, setter.withIdLast(entity));
        } finally {
            closeAll(state);
        }
    }

    @Override
    public boolean update(List<Type> entities) {
        PreparedStatement state = getStatement("update-by-id");
        try {
            boolean result = true;
            Page page = new Page(0, batchSize).setCount(entities.size());
            while (page.next().getPage() <= page.pages()) {
                List<Type> batch = entities.subList(page.offset(), page.limit());
                result &= updateByIds(state, batch);
            }
            return result;
        } finally {
            closeAll(state);
        }
    }

    private boolean updateByIds(PreparedStatement state, List<Type> entities) {
        return db.queryBatch(state, setter.withIdLast(entities));
    }

    @Override
    public Type selectById(Id id) {
        PreparedStatement state = getStatement("select-by-id");
        try {
            StatementInitializer init = s -> setter.setId(s, id);
            return db.selectOne(state, getter, init);
        } finally {
            closeAll(state);
        }
    }

    @Override
    public List<Type> selectById(List<Id> ids) {
        PreparedStatement state = getStatement("select-by-ids");
        try {
            StatementInitializer init = s -> setter.setId(s, ids);
            return db.selectMany(state, getter, init);
        } finally {
            closeAll(state);
        }
    }

    @Override
    public boolean deleteById(Id id) {
        PreparedStatement state = getStatement("delete-by-id");
        try {
            return db.query(state, s -> setter.setId(s, id));
        } finally {
            closeAll(state);
        }
    }

    @Override
    public boolean deleteById(List<Id> ids) {
        PreparedStatement state = getStatement("delete-by-ids");
        try {
            return db.query(state, s -> setter.setId(s, ids));
        } finally {
            closeAll(state);
        }
    }

    protected PreparedStatement getStatement(String name) {
        return nullable(name)
            .map(templates::get)
            .map(db::prepareStatement)
            .orElseThrow(() -> createIllegalState("Can't create statement for name " + name));
    }

}
