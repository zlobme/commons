
package me.zlob.jdbc.orm;

import java.util.*;
import java.util.stream.*;

import org.jooq.*;

import static org.jooq.impl.DSL.*;

import me.zlob.collections.*;

import static me.zlob.util.Conditions.*;

public class CrudSqlCreator implements SqlCreator {

    private static final List<Integer> DEFAULT_PARAMS_SIZES;

    static {
        DEFAULT_PARAMS_SIZES = Arrays.asList(10, 20, 50, 100, 500, 1000);
    }

    private DSLContext sqlContext;

    private String scheme;

    private String tableName;

    private String identityName;

    private List<String> fields;

    private List<Integer> sizes;

    public CrudSqlCreator(
        DSLContext sqlContext,
        String scheme,
        String tableName,
        String identityName,
        List<String> fields
    ) {
        this(sqlContext, scheme, tableName, identityName, fields, DEFAULT_PARAMS_SIZES);
    }

    public CrudSqlCreator(
        DSLContext sqlContext,
        String scheme,
        String tableName,
        String identityName,
        List<String> fields,
        List<Integer> sizes
    ) {
        this.sqlContext = checkNotNull(sqlContext,"sqlContext");
        this.scheme = checkArgNotEmpty(scheme, "scheme");
        this.tableName = checkArgNotEmpty(tableName, "tableName");
        this.identityName = checkArgNotEmpty(identityName, "identityName");
        this.fields = checkArgNotEmpty(fields, "fields");
        this.sizes = checkArgNotEmpty(sizes, "sizes");
    }

    @Override
    public Map<String, String> create() {
        Map<String, String> result = new HashMap<>();
        result.put("insert-with-id", getInsertWithIdSql());
        result.put("insert-without-id", getInsertWithoutIdSql());
        result.put("select-by-id", getSelectByIdSql());
        result.put("select-by-ids", getSelectByIdsSql(20));
        result.put("update-by-id", getUpdateByIdSql());
        result.put("delete-by-id", getDeleteByIdSql());
        result.put("delete-by-ids", getDeleteByIdsSql(20));
        for (Integer size : sizes) {
            result.put("select-by-ids-" + size, getSelectByIdsSql(size));
            result.put("delete-by-ids-" + size, getDeleteByIdsSql(size));
        }
        return result;
    }

    public String getInsertWithIdSql() {
        Table table = getTable();
        List<Field<?>> tableFields = getFields(Containers.concat(identityName, fields));
        List<Field<?>> params = params(fields.size() + 1);
        return getInsertSql(table, tableFields, params);
    }

    public String getInsertWithoutIdSql() {
        Table table = getTable();
        List<Field<?>> tableFields = getFields(fields);
        List<Field<?>> params = params(fields.size());
        return getInsertSql(table, tableFields, params);
    }

    public String getInsertSql(Table table, List<Field<?>> fields, List<Field<?>> params) {
        return sqlContext
            .insertInto(table, fields)
            .values(params)
            .getSQL();
    }

    public String getSelectByIdSql() {
        Table table = getTableAlias();
        Field tableId = field(alias(table, identityName));
        Condition condition = tableId.eq(param());
        return getSelectWhere(table, condition).getSQL();
    }

    public String getSelectByIdsSql(int number) {
        Table table = getTableAlias();
        Field tableId = field(alias(table, identityName));
        Condition condition = tableId.in(params(number));
        return getSelectWhere(table, condition).getSQL();
    }

    protected Query getSelectWhere(Table table, Condition condition) {
        List<Field<?>> tableFields = getFields(table, Containers.concat(identityName, fields));
        return getSelectWhere(table, tableFields, condition);
    }

    protected Query getSelectWhere(Table table, List<Field<?>> fields, Condition condition) {
        return sqlContext
            .select(fields)
            .from(table)
            .where(condition);
    }

    public String getUpdateByIdSql() {
        Table table = getTableAlias();
        Field tableId = field(alias(table, identityName));
        Condition condition = tableId.eq(param());
        return getUpdateWhere(table, condition).getSQL();
    }

    protected Query getUpdateWhere(Table table, Condition condition) {
        List<Field> tableFields = getFieldsRaw(table, fields);
        return getUpdateWhere(table, tableFields, condition);
    }

    protected Query getUpdateWhere(Table table, List<Field> fields, Condition condition) {
        UpdateSetFirstStep<Record> update = sqlContext.update(table);
        UpdateSetMoreStep updateSet = update.set(fields.get(0), "?");
        for (Field field : fields) {
            updateSet = update.set(field, "?");
        }
        return updateSet.where(condition);
    }

    public String getDeleteByIdSql() {
        Table table = getTable();
        Field tableId = field(alias(table, identityName));
        Condition condition = tableId.eq(param());
        return getDeleteWhere(table, condition).getSQL();
    }

    public String getDeleteByIdsSql(int number) {
        Table table = getTable();
        Field tableId = field(alias(table, identityName));
        Condition condition = tableId.in(params(number));
        return getDeleteWhere(table, condition).getSQL();
    }

    protected Query getDeleteWhere(Table table, Condition condition) {
        return sqlContext
            .delete(table)
            .where(condition);
    }

    protected Table getTable() {
        return table(name(scheme, tableName));
    }

    protected Table getTableAlias() {
        return table(name(scheme, tableName)).as("t");
    }

    protected List<Field<?>> getFields(List<String> fields) {
        return fields.stream()
            .map(f -> field(name(f)))
            .collect(Collectors.toList());
    }

    protected List<Field<?>> getFields(Table table, List<String> fields) {
        return fields.stream()
            .map(f -> field(alias(table, f)))
            .collect(Collectors.toList());
    }

    protected List<Field> getFieldsRaw(Table table, List<String> fields) {
        return fields.stream()
            .map(f -> field(alias(table, f)))
            .collect(Collectors.toList());
    }

    protected Name alias(Table table, String field) {
        return alias(table.getQualifiedName(), field);
    }

    protected Name alias(Name table, String field) {
        List<String> names = Arrays.asList(table.getName());
        List<String> mutableNames = new ArrayList<>(names);
        mutableNames.add(field);
        return name(mutableNames);
    }

    protected List<Field<?>> params(int number) {
        List<Field<?>> placeHolders = new ArrayList<>(number);
        for (int i = 0; i < number; i++) {
            placeHolders.add(param());
        }
        return placeHolders;
    }

}
