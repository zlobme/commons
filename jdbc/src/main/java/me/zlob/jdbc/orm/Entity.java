
package me.zlob.jdbc.orm;

public interface Entity<Type> {

    public Type getId();

    public void setId(Type id);

}
