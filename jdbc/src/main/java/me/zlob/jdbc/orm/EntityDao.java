
package me.zlob.jdbc.orm;

import java.util.*;

public interface EntityDao<Id, Type extends Entity<Id>> {

    public boolean insert(Type entity);

    public boolean insert(List<Type> entities);

    public boolean update(Type entity);

    public boolean update(List<Type> entities);

    public Type selectById(Id id);

    public List<Type> selectById(List<Id> ids);

    public boolean delete(Type entity);

    public boolean delete(List<Type> entities);

    public boolean deleteById(Id id);

    public boolean deleteById(List<Id> ids);

}
