
package me.zlob.jdbc.orm;

import java.sql.*;
import java.util.*;

import me.zlob.jdbc.*;

@FunctionalInterface
public interface EntitySetter<Id, Type extends Entity<Id>> {

    public int setObjectWithoutId(PreparedStatement statement, int pos, Type object) throws SQLException;

    default int setObjectWithoutId(PreparedStatement statement, Type object) throws SQLException {
        return setObjectWithoutId(statement, 1, object);
    }

    default int setObjectId(PreparedStatement statement, Type object) throws SQLException {
        return setObjectId(statement, 1, object);
    }

    default int setObjectId(PreparedStatement statement, int position, Type object) throws SQLException {
        return setId(statement, object.getId());
    }

    default int setId(PreparedStatement statement, Id id) throws SQLException {
        return setId(statement, 1, id);
    }

    default int setId(PreparedStatement statement, int position, Id id) throws SQLException {
        int result = position;
        if (id instanceof Integer) {
            statement.setInt(result++, (Integer) id);
        } else if (id instanceof Long) {
            statement.setLong(result++, (Long) id);
        } else if (id instanceof String) {
            statement.setString(result++, (String) id);
        } else {
            statement.setString(result++, id.toString());
        }
        return result;
    }

    default int setId(PreparedStatement statement, List<Id> ids) throws SQLException {
        return setId(statement, 1, ids);
    }

    default int setId(PreparedStatement statement, int position, List<Id> ids) throws SQLException {
        int result = position;
        for (Id id : ids) {
            setId(statement, result++, id);
        }
        return result;
    }

    default StatementInitializer withoutId(Type entity) {
        return s -> {
            setObjectWithoutId(s, entity);
        };
    }

    default StatementInitializer withoutId(List<Type> entities) {
        return s -> {
            for (Type entity : entities) {
                setObjectWithoutId(s, entity);
                s.addBatch();
            }
        };
    }

    default StatementInitializer withIdFirst(Type entity) {
        return s -> {
            int pos = setObjectId(s, entity);
            setObjectWithoutId(s, pos, entity);
        };
    }

    default StatementInitializer withIdFirst(List<Type> entities) {
        return s -> {
            for (Type entity : entities) {
                int pos = setObjectId(s, entity);
                setObjectWithoutId(s, pos, entity);
                s.addBatch();
            }
        };
    }

    default StatementInitializer withIdLast(Type entity) {
        return s -> {
            int pos = setObjectWithoutId(s, entity);
            setObjectId(s, pos, entity);
        };
    }

    default StatementInitializer withIdLast(List<Type> entities) {
        return s -> {
            for (Type entity : entities) {
                int pos = setObjectWithoutId(s, entity);
                setObjectId(s, pos, entity);
                s.addBatch();
            }
        };
    }

}
