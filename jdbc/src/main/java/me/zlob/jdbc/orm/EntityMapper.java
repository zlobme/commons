
package me.zlob.jdbc.orm;

import java.util.*;

import lombok.*;

import static me.zlob.util.Conditions.*;

public class EntityMapper {

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private List<String> fields;

    private Map<String, String> templates;

    public EntityMapper(String name, List<String> fields, Map<String, String> templates) {
        this.name = checkNotNull(name,"name");
        this.fields = checkArgNotEmpty(fields, "fields");
        this.templates = checkArgNotEmpty(templates, "templates");
    }

    public String getSql(String name) {
        return nullable(name)
            .map(templates::get)
            .orElseThrow(IllegalArgumentException::new);
    }

}
