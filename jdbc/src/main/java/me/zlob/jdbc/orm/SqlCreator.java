
package me.zlob.jdbc.orm;

import java.util.*;

@FunctionalInterface
public interface SqlCreator {

    public Map<String, String> create();

}
