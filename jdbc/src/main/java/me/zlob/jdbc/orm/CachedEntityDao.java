
package me.zlob.jdbc.orm;

import java.time.*;
import java.util.*;
import java.util.stream.*;

import me.zlob.cache.*;

import static me.zlob.util.Conditions.*;

public class CachedEntityDao<Id, Type extends Entity<Id>> implements EntityDao<Id, Type> {

    //<editor-fold defaultstate="collapsed" desc="KeyChain">
    private static class KeyChain {

        public static <Id, Type extends Entity<Id>> String key(Type entity) {
            return key(entity.getId(), "");
        }

        public static <Id, Type extends Entity<Id>> String key(Type entity, String prefix) {
            return key(entity.getId(), prefix);
        }

        public static <Id> String key(Id id) {
            return key(id, "");
        }

        public static <Id> String key(Id id, String prefix) {
            return "entity_" + (existsNotEmpty(prefix) ? prefix + "_" : "") + id;
        }

    }
    //</editor-fold>

    private EntityDao<Id, Type> entityDao;

    private Cache<String, Type> cache;

    private String prefix = "";

    public CachedEntityDao(EntityDao<Id, Type> entityDao, Cache<String, Type> cache) {
        this.entityDao = entityDao;
        this.cache = cache;
    }

    public CachedEntityDao(EntityDao<Id, Type> entityDao, Cache<String, Type> cache, String prefix) {
        this.entityDao = entityDao;
        this.cache = cache;
        this.prefix = prefix;
    }

    @Override
    public boolean insert(Type entity) {
        return entityDao.insert(entity);
    }

    @Override
    public boolean insert(List<Type> entities) {
        return entityDao.insert(entities);
    }

    @Override
    public boolean update(Type entity) {
        boolean result = entityDao.update(entity);
        if (result) {
            cache.delete(KeyChain.key(entity, prefix));
        }
        return result;
    }

    @Override
    public boolean update(List<Type> entities) {
        boolean result = entityDao.update(entities);
        if (result) {
            for (Type entity : entities) {
                cache.delete(KeyChain.key(entity, prefix));
            }
        }
        return result;
    }

    @Override
    public Type selectById(Id id) {
        String key = KeyChain.key(id, prefix);
        Type entity = (Type) cache.get(key);
        if (entity == null) {
            entity = entityDao.selectById(id);
            if (entity != null) {
                cache.put(key, entity, Duration.ofHours(1));
            }
        }
        return entity;
    }

    @Override
    public List<Type> selectById(List<Id> ids) {
        List<Type> result = new ArrayList<>();

        List<String> keys = ids.stream()
            .map(id -> KeyChain.key(id, prefix))
            .collect(Collectors.toList());

        List<Id> idsToGet = new ArrayList<>();

        List<Type> cachedEntities = cache.getMulti(keys);
        for (int i = 0; i < cachedEntities.size(); i++) {
            Type entity = cachedEntities.get(i);
            if (entity == null) {
                idsToGet.add(ids.get(i));
            } else {
                result.add(entity);
            }
        }

        if (idsToGet.size() > 0) {
            List<Type> persistedEntities = entityDao.selectById(idsToGet);
            for (Type entity : persistedEntities) {
                cache.put(KeyChain.key(entity, prefix), entity, Duration.ofHours(1));
                result.add(entity);
            }
        }
        return result;
    }

    @Override
    public boolean delete(Type entity) {
        cache.delete(KeyChain.key(entity, prefix));
        return entityDao.delete(entity);
    }

    @Override
    public boolean delete(List<Type> entities) {
        for (Type entity : entities) {
            cache.delete(KeyChain.key(entity, prefix));
        }
        return entityDao.delete(entities);
    }

    @Override
    public boolean deleteById(Id id) {
        cache.delete(KeyChain.key(id, prefix));
        return entityDao.deleteById(id);
    }

    @Override
    public boolean deleteById(List<Id> ids) {
        for (Id id : ids) {
            cache.delete(KeyChain.key(id, prefix));
        }
        return entityDao.deleteById(ids);
    }

}
