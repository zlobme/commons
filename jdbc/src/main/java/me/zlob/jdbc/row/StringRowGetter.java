
package me.zlob.jdbc.row;

import java.sql.*;

public class StringRowGetter extends AbstractRowGetter<String> {

    private int index;

    public StringRowGetter() {
        this(1);
    }

    public StringRowGetter(int index) {
        this.index = index;
    }

    @Override
    public String mapRow(ResultSet rs) throws SQLException {
        return rs.getString(index);
    }

}
