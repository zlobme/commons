
package me.zlob.jdbc.row;

import java.sql.*;

public class IntegerRowGetter extends AbstractRowGetter<Integer> {

    private int index;

    public IntegerRowGetter() {
        this(1);
    }

    public IntegerRowGetter(int index) {
        this.index = index;
    }

    @Override
    public Integer mapRow(ResultSet rs) throws SQLException {
        return rs.getInt(index);
    }

}
