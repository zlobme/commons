
package me.zlob.jdbc.row;

import java.sql.*;

public class LongRowGetter extends AbstractRowGetter<Long> {

    private int index;

    public LongRowGetter() {
        this(1);
    }

    public LongRowGetter(int index) {
        this.index = index;
    }

    @Override
    public Long mapRow(ResultSet rs) throws SQLException {
        return rs.getLong(index);
    }

}
