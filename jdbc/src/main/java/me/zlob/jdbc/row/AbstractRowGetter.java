
package me.zlob.jdbc.row;

import java.sql.*;
import java.util.*;

import org.slf4j.*;

public abstract class AbstractRowGetter<Type> implements RowGetter<Type> {

    private static final Logger log = LoggerFactory.getLogger(AbstractRowGetter.class);

    @Override
    public abstract Type mapRow(ResultSet rs) throws SQLException;

    @Override
    public Type getObject(ResultSet rs) {
        Type result = null;
        try {
            if (rs != null && rs.first()) {
                result = mapRow(rs);
            }
        } catch (SQLException ex) {
            log.error(ex.getMessage(), ex);
        }
        return result;
    }

    @Override
    public List<Type> getObjects(ResultSet rs) {
        List<Type> result = Collections.emptyList();
        if (rs != null) {
            try {
                result = new ArrayList<>();
                rs.beforeFirst();
                while (rs.next()) {
                    result.add(mapRow(rs));
                }
            } catch (SQLException ex) {
                log.error(ex.getMessage(), ex);
            }
        }
        return result;
    }

}
