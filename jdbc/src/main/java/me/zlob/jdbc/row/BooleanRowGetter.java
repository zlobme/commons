
package me.zlob.jdbc.row;

import java.sql.*;

public class BooleanRowGetter extends AbstractRowGetter<Boolean> {

    private int index;

    public BooleanRowGetter() {
        this(1);
    }

    public BooleanRowGetter(int index) {
        this.index = index;
    }

    @Override
    public Boolean mapRow(ResultSet rs) throws SQLException {
        return rs.getBoolean(index);
    }

}
