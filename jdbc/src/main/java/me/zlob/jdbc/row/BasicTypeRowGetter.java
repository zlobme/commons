
package me.zlob.jdbc.row;

import java.sql.*;

public class BasicTypeRowGetter<Type> extends AbstractRowGetter<Type> {

    private int index;

    public BasicTypeRowGetter() {
        this(1);
    }

    public BasicTypeRowGetter(int index) {
        this.index = index;
    }

    @Override
    public Type mapRow(ResultSet rs) throws SQLException {
        return (Type) rs.getObject(index);
    }

}
