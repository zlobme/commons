
package me.zlob.jdbc.row;

import java.sql.*;
import java.util.*;

public interface RowGetter<Type> {

    public Type mapRow(ResultSet rs) throws SQLException;

    public Type getObject(ResultSet rs);

    public List<Type> getObjects(ResultSet rs);

}
