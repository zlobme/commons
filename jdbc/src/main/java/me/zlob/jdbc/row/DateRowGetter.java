
package me.zlob.jdbc.row;

import java.sql.*;
import java.util.Date;

public class DateRowGetter extends AbstractRowGetter<Date> {

    private int index;

    public DateRowGetter() {
        this(1);
    }

    public DateRowGetter(int index) {
        this.index = index;
    }

    @Override
    public Date mapRow(ResultSet rs) throws SQLException {
        return new Date(rs.getTimestamp(index).getTime());
    }

}
