
package me.zlob.jdbc.row;

import java.sql.*;

public class DoubleRowGetter extends AbstractRowGetter<Double> {

    private int index;

    public DoubleRowGetter() {
        this(1);
    }

    public DoubleRowGetter(int index) {
        this.index = index;
    }

    @Override
    public Double mapRow(ResultSet rs) throws SQLException {
        return rs.getDouble(index);
    }

}
