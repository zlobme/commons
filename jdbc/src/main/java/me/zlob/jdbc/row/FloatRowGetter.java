
package me.zlob.jdbc.row;

import java.sql.*;

public class FloatRowGetter extends AbstractRowGetter<Float> {

    private int index;

    public FloatRowGetter() {
        this(1);
    }

    public FloatRowGetter(int index) {
        this.index = index;
    }

    @Override
    public Float mapRow(ResultSet rs) throws SQLException {
        return rs.getFloat(index);
    }

}
