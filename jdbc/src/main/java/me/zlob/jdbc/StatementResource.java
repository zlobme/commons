
package me.zlob.jdbc;

import java.sql.*;

import me.zlob.util.*;

import static me.zlob.jdbc.JdbcResourceHelper.*;

public class StatementResource<Type extends Statement> extends Resource<Type> {

    public static Resource wrap(Statement origin) {
        return new StatementResource<>(origin);
    }

    public static Resource wrap(PreparedStatement origin) {
        return new StatementResource<>(origin);
    }

    public StatementResource(Type origin) {
        super(origin);
    }

    @Override
    public void close() {
        closeAll(getOrigin());
    }

}
