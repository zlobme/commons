
package me.zlob.jdbc;

import java.io.*;
import java.sql.*;

import javax.sql.*;

import org.slf4j.*;

import org.jooq.*;

import static org.jooq.impl.DSL.*;

import org.junit.*;
import static org.junit.Assert.*;

import me.zlob.jdbc.pool.*;
import me.zlob.jdbc.row.*;

import static me.zlob.util.Conditions.*;

public class DataBaseIT extends AbstractJdbcTest {

    private static final Logger log = LoggerFactory.getLogger(DataBaseIT.class);

    private static DataSource dataSource;

    private static DSLContext context;

    @BeforeClass
    public static void init() {
        dataSource = new PooledDataSource(createH2DataSource());
        context = createContext(SQLDialect.H2);
    }

    @Before
    @After
    public void cleanup() {
        File tempDB = new File("./target/test.mv.db");
        if (tempDB.exists()) {
            checkState(!tempDB.delete(),
                "Temporary file doesn't deleted: %s",
                tempDB.getAbsolutePath());
        }
    }

    @Test
    public void open_connection() throws SQLException {
        try (Connection connection = dataSource.getConnection()) {
            assertFalse(connection.isClosed());
        }
    }

    @Test
    public void prepare_statement() {
        String select = context.select(val("1")).getSQL();

        DataBase db = new DataBase(dataSource);
        PreparedStatement preparedStatement = db.prepareStatement(select);

        assertNotNull(preparedStatement);
    }

    @Test
    public void run_query() throws SQLException {
        String select = context.select(field("1", Integer.class)).getSQL();

        DataBase db = new DataBase(dataSource);

        try (ResultSet rs = db.select(select)) {
            int number = new IntegerRowGetter().getObject(rs);
            assertEquals(1, number);
        }
    }

}
