
package me.zlob.jdbc.beans;

import java.io.*;
import java.util.*;

import me.zlob.jdbc.orm.*;

public class LogRecord implements Entity<Long>, Serializable {

    public enum Action {

        UNDEFINED, CREATE, GET, UPDATE, DELETE

    }

    private Long id;

    private long user;

    private Action action = Action.UNDEFINED;

    private long subject;

    private String subjectType = "";

    private String description = "";

    private String ip = "";

    private Date date = new Date();

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public long getUser() {
        return user;
    }

    public void setUser(long user) {
        this.user = user;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public long getSubject() {
        return subject;
    }

    public void setSubject(long subject) {
        this.subject = subject;
    }

    public String getSubjectType() {
        return subjectType;
    }

    public void setSubjectType(String subjectType) {
        this.subjectType = subjectType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

}
