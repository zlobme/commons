
package me.zlob.jdbc.beans;

import java.util.*;

import org.jooq.*;

import org.junit.*;

import me.zlob.jdbc.*;
import me.zlob.jdbc.orm.*;

public class LogRecordTest extends AbstractJdbcTest {

    @Test
    public void test() {
        DSLContext context = createContext(SQLDialect.MYSQL_5_7);
        String scheme = "test";
        String table = "audit_log";
        String id = "id";
        List<String> fields = Arrays.asList(
            "id", "user", "action",
            "subject", "subject_type",
            "description", "ip", "date"
        );
        CrudSqlCreator sqlCreator = new CrudSqlCreator(context, scheme, table, id, fields);

        System.out.println(sqlCreator.getUpdateByIdSql());
    }

}
