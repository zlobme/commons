
package me.zlob.jdbc.orm;

import java.util.*;

import org.slf4j.*;

import org.jooq.*;

import org.junit.*;
import static org.junit.Assert.*;

import me.zlob.jdbc.*;

public class CrudSqlCreatorTest extends AbstractJdbcTest {

    private static final Logger log = LoggerFactory.getLogger(DataBaseIT.class);

    private DSLContext context;

    private CrudSqlCreator sqlCreator;

    @Before
    public void init() {
        context = createContext(SQLDialect.MYSQL_5_7);
        String scheme = "test";
        String table = "table";
        String id = "id";
        List<String> fields = Arrays.asList("data");
        sqlCreator = new CrudSqlCreator(context, scheme, table, id, fields);
    }

    @Test
    public void create_insert_with_id_sql() {
        String expected = "insert into `test`.`table` (`id`, `data`) values (?, ?)";

        String sql = sqlCreator.getInsertWithIdSql();
        log.debug("insert with id sql: {}", sql);

        assertEquals(expected, sql);
    }

    @Test
    public void create_insert_without_id_sql() {
        String expected = "insert into `test`.`table` (`data`) values (?)";

        String sql = sqlCreator.getInsertWithoutIdSql();
        log.debug("insert without id sql: {}", sql);

        assertEquals(expected, sql);
    }

    @Test
    public void create_select_by_id_sql() {
        String expected = "select `t`.`id`, `t`.`data` from `test`.`table` as `t` where `t`.`id` = ?";

        String sql = sqlCreator.getSelectByIdSql();
        log.debug("select by id sql: {}", sql);

        assertEquals(expected, sql);
    }

    @Test
    public void create_select_by_ids_sql() {
        String expected = "select `t`.`id`, `t`.`data` from `test`.`table` as `t` where `t`.`id` in (?, ?)";

        String sql = sqlCreator.getSelectByIdsSql(2);
        log.debug("select by ids sql: {}", sql);

        assertEquals(expected, sql);
    }

    @Test
    public void create_update_by_id_sql() {
        String expected = "update `test`.`table` as `t` set `t`.`data` = ? where `t`.`id` = ?";

        String sql = sqlCreator.getUpdateByIdSql();
        log.debug("update by id sql: {}", sql);

        assertEquals(expected, sql);
    }

    @Test
    public void create_delete_by_id_sql() {
        String expected = "delete from `test`.`table` where `test`.`table`.`id` = ?";

        String sql = sqlCreator.getDeleteByIdSql();
        log.debug("delete by id sql: {}", sql);

        assertEquals(expected, sql);
    }

    @Test
    public void create_delete_by_ids_sql() {
        String expected = "delete from `test`.`table` where `test`.`table`.`id` in (?, ?)";

        String sql = sqlCreator.getDeleteByIdsSql(2);
        log.debug("delete by ids sql: {}", sql);

        assertEquals(expected, sql);
    }

}
