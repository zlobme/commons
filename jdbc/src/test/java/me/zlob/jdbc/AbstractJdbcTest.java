
package me.zlob.jdbc;

import javax.sql.*;

import org.slf4j.*;

import com.mysql.jdbc.jdbc2.optional.*;
import org.h2.jdbcx.*;
import org.jooq.*;
import org.jooq.conf.*;
import org.jooq.impl.*;

import me.zlob.util.*;

public class AbstractJdbcTest {

    private static final Logger log = LoggerFactory.getLogger(AbstractJdbcTest.class);

    protected static DataSource createH2DataSource() {
        JdbcDataSource ds = new JdbcDataSource();
        ds.setUrl("jdbc:h2:./target/test;TRACE_LEVEL_FILE=4");
        return ds;
    }

    protected static DataSource createMySQLDataSource(String database, String user, String pass) {
        Url url = Url.of("jdbc:mysql://127.0.0.1/")
            .path(database)
            .set("useUnicode", "true")
            .set("characterEncoding", "UTF-8")
            .set("characterSetResults", "UTF-8")
            .set("retainStatementAfterResultSetClose", "true")
            .set("autoReconnect", "true");

        MysqlDataSource ds = new MysqlConnectionPoolDataSource();
        ds.setUrl(url.get());
        ds.setUser(user);
        ds.setPassword(pass);
        return ds;
    }

    protected static Settings getSettings() {
        Settings settings = new Settings();
        settings.setStatementType(StatementType.PREPARED_STATEMENT);
        settings.setParamCastMode(ParamCastMode.NEVER);
        settings.setParamType(ParamType.INDEXED);
        return settings;
    }

    protected static DSLContext createContext(SQLDialect dialect) {
        Settings settings = getSettings();
        Configuration configuration = new DefaultConfiguration();
        configuration.set(dialect);
        configuration.set(settings);
        return DSL.using(configuration);
    }

}
