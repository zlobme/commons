
package me.zlob.web;

import java.time.*;

import javax.servlet.http.*;

import com.google.gson.*;

import static org.mockito.Mockito.*;

import org.junit.*;
import static org.junit.Assert.*;

import me.zlob.cache.*;
import me.zlob.util.*;

public class SessionServiceTest {

    public static final Gson gson = GsonFactory.getGson(true);

    public static final CookieBuilder BUILDER = new CookieBuilder();

    public static final Duration EXPIRE_TIME = Duration.ofMinutes(30);

    @Test
    public void get_session_with_no_cookies_no_create() {
        Cache cache = mock(Cache.class);

        SessionService service = new SessionService(cache, BUILDER);

        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getCookies()).thenReturn(new Cookie[0]);

        HttpServletResponse response = mock(HttpServletResponse.class);

        Session session = service.get(request, response);
        assertTrue(session == null);

        verify(request).getCookies();
        verify(response, never()).addCookie(any());
    }

    @Test
    public void get_session_with_wrong_cookies_no_create() {
        Cache cache = mock(Cache.class);
        when(cache.get(any())).thenReturn(null);

        SessionService service = new SessionService(cache, BUILDER);

        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getCookies()).thenReturn(new Cookie[]{new Cookie("session", "abcd")});

        HttpServletResponse response = mock(HttpServletResponse.class);

        Session session = service.get(request, response);

        assertTrue(session == null);

        verify(request).getCookies();
    }

    @Test
    public void get_session_with_good_cookies_no_create() {
        Cache cache = mock(Cache.class);
        when(cache.get(eq("abcd"))).thenReturn(new Session("abcd"));

        SessionService service = new SessionService(cache, BUILDER);

        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getCookies()).thenReturn(new Cookie[]{new Cookie("session", "abcd")});

        HttpServletResponse response = mock(HttpServletResponse.class);

        Session session = service.get(request, response);

        assertTrue(session != null);
        assertTrue(session.getId() != null);
        assertEquals("abcd", session.getId());
        assertFalse(session.isChanged());

        verify(cache).get(any());
        verify(request).getCookies();
    }

    @Test
    public void get_session_with_no_cookies_with_create() {
        Cache cache = mock(Cache.class);
        when(cache.get(any())).thenReturn(null);
        when(cache.put(any(), any(), eq(EXPIRE_TIME))).thenReturn(true);

        SessionService service = new SessionService(cache, BUILDER);

        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getCookies()).thenReturn(new Cookie[]{});

        HttpServletResponse response = mock(HttpServletResponse.class);

        Session session = service.get(request, response, true);

        assertTrue(session != null);
        assertTrue(session.getId() != null);
        assertFalse(session.isChanged());

        verify(request).getCookies();
        verify(response).addCookie(any());
        verify(cache).put(any(), any(), eq(EXPIRE_TIME));
    }

    @Test
    public void get_session_with_wrong_cookies_with_create() {
        Cache cache = mock(Cache.class);
        when(cache.get(any())).thenReturn(null);
        when(cache.put(any(), any(), eq(EXPIRE_TIME))).thenReturn(true);

        SessionService service = new SessionService(cache, BUILDER);

        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getCookies()).thenReturn(new Cookie[]{new Cookie("session", "abcd")});

        HttpServletResponse response = mock(HttpServletResponse.class);

        Session session = service.get(request, response, true);

        assertTrue(session != null);
        assertTrue(session.getId() != null);
        assertFalse(session.isChanged());

        verify(cache).put(any(), any(), eq(EXPIRE_TIME));

        verify(request).getCookies();
        verify(response).addCookie(any());
    }

    @Test
    public void get_session_with_good_cookies_with_create() {
        Session stubSession = new Session("abcd");

        Cache cache = mock(Cache.class);
        when(cache.get("abcd")).thenReturn(stubSession);
        when(cache.put(any(), any(), eq(EXPIRE_TIME))).thenReturn(true);

        SessionService service = new SessionService(cache, BUILDER);

        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getCookies()).thenReturn(new Cookie[]{new Cookie("session", "abcd")});

        HttpServletResponse response = mock(HttpServletResponse.class);

        Session session = service.get(request, response, true);

        assertTrue(session != null);
        assertTrue(session.getId() != null);
        assertFalse(session.isChanged());

        verify(cache).expire(any(), eq(EXPIRE_TIME));

        verify(request).getCookies();
    }

}
