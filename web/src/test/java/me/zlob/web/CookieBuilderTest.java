
package me.zlob.web;

import org.junit.*;
import static org.junit.Assert.*;

public class CookieBuilderTest {

    private static CookieBuilder builder = new CookieBuilder("example.com", "", true, true, -1);

    @Test
    public void initialization_test() {
        assertEquals("example.com", builder.getDomain());
        assertEquals("", builder.getPath());
        assertTrue(builder.isSecure());
        assertTrue(builder.isHttpOnly());
        assertEquals(-1, builder.getMaxAge());
    }

    @Test
    public void setters_test() {
        assertEquals(".example.com", builder.setDomain(".example.com").getDomain());
        assertEquals("/", builder.setPath("/").getPath());
        assertEquals(false, builder.setSecure(false).isSecure());
        assertEquals(true, builder.isSecure());
        assertEquals(false, builder.setHttpOnly(false).isHttpOnly());
        assertEquals(true, builder.isHttpOnly());
        assertEquals(100, builder.setMaxAge(100).getMaxAge());
        assertEquals(-1, builder.getMaxAge());
    }

}
