
package me.zlob.web;

import java.util.*;

import static org.mockito.Mockito.*;

import org.junit.*;
import static org.junit.Assert.*;

import me.zlob.nosql.redis.*;

import static me.zlob.properties.PropertiesProcessors.*;

public class SettingsTest {

    @Test
    public void test() {
        Redis redis = mock(Redis.class);
        when(redis.keys(any())).thenReturn(Collections.singleton("setting_test"));
        when(redis.get("setting_test")).thenReturn(null).thenReturn("value");

        Properties props = new Properties();
        props.setProperty("test", "value");

        Settings settings = new Settings(redis, fromPreset(props));
        assertTrue(settings.get("test").equals(""));

        settings.init();
        assertTrue(settings.get("test").equals("value"));
    }

}
