
package me.zlob.web;

import java.util.*;

import org.junit.*;
import static org.junit.Assert.*;

import static me.zlob.web.Paginator.*;

public class PaginatorTest {

    @Test
    public void short_pages_first_list() {
        assertEquals(Arrays.asList("a", "2", "3", "4", "5", ".", ">"), getShort(1, 10));
    }

    @Test
    public void short_pages_first_shift() {
        assertEquals(Arrays.asList("<", "1", ".", "4", "a", "6", ".", ">"), getShort(5, 10));
    }

    @Test
    public void short_pages_last_shift() {
        assertEquals(Arrays.asList("<", "1", ".", "5", "a", "7", ".", ">"), getShort(6, 10));
    }

    @Test
    public void short_pages_last_list() {
        assertEquals(Arrays.asList("<", "1", ".", "7", "8", "9", "a"), getShort(10, 10));
    }

    //@Test
    public void short_pages_show_case() {
        System.out.println("-----------short-----------");
        int pages = 25;
        for (int i = 1; i <= pages; i++) {
            List<String> paged = getShort(i, pages);
            System.out.println(String.join(" ", paged));
        }
        System.out.println();
    }

    //@Test
    public void long_pages_show_case() {
        System.out.println("-----------long-----------");
        int pages = 25;
        for (int i = 1; i <= pages; i++) {
            List<String> paged = getLong(i, pages);
            System.out.println(String.join(" ", paged));
        }
        System.out.println();
    }

}
