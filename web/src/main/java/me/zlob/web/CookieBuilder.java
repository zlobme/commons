
package me.zlob.web;

import javax.servlet.http.*;

import static me.zlob.util.Conditions.*;

public class CookieBuilder {

    private final String domain;

    private final String path;

    private final boolean secure;

    private final boolean httpOnly;

    private final int maxAge;

    public CookieBuilder() {
        this.domain = "localhost";
        this.path = "";
        this.secure = false;
        this.httpOnly = false;
        this.maxAge = -1;
    }

    public CookieBuilder(String domain, String path, boolean secure, boolean httpOnly, int maxAge) {
        this.domain = checkNotNull(domain, "domain");
        this.path = path;
        this.secure = secure;
        this.httpOnly = httpOnly;
        this.maxAge = maxAge;
    }

    public CookieBuilder setDomain(String domain) {
        return new CookieBuilder(domain, path, secure, httpOnly, maxAge);
    }

    public CookieBuilder setPath(String path) {
        return new CookieBuilder(domain, path, secure, httpOnly, maxAge);
    }

    public CookieBuilder setSecure(boolean secure) {
        return new CookieBuilder(domain, path, secure, httpOnly, maxAge);
    }

    public CookieBuilder setHttpOnly(boolean httpOnly) {
        return new CookieBuilder(domain, path, secure, httpOnly, maxAge);
    }

    public CookieBuilder setMaxAge(int maxAge) {
        return new CookieBuilder(domain, path, secure, httpOnly, maxAge);
    }

    public Cookie build(String name, String value) {
        Cookie cookie = new Cookie(name, value);
        cookie.setDomain(domain);
        cookie.setPath(path);
        cookie.setSecure(secure);
        cookie.setHttpOnly(httpOnly);
        cookie.setMaxAge(maxAge);
        return cookie;
    }

    public Cookie getCookie(Cookie[] cookies, String name) {
        Cookie result = null;
        if (existsNotEmpty(cookies)) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(name)) {
                    result = cookie;
                }
            }
        }
        return result;
    }

    //<editor-fold defaultstate="collapsed" desc="getters">
    public String getDomain() {
        return domain;
    }

    public String getPath() {
        return path;
    }

    public boolean isSecure() {
        return secure;
    }

    public boolean isHttpOnly() {
        return httpOnly;
    }

    public int getMaxAge() {
        return maxAge;
    }
    //</editor-fold>

}
