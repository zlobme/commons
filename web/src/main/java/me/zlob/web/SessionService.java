
package me.zlob.web;

import java.time.*;

import javax.servlet.http.*;

import org.slf4j.*;

import me.zlob.cache.*;

import static me.zlob.util.Conditions.*;
import static me.zlob.util.Util.*;

/**
 * Session manager service.
 */
public class SessionService {

    private static final Logger log = LoggerFactory.getLogger(SessionService.class);

    private static final Duration DEFAULT_TIMEOUT = Duration.ofMinutes(30);

    //<editor-fold defaultstate="collapsed" desc="KeyChain">
    private static class KeyChain {

        public static String session(Session session) {
            return session(session.getId());
        }

        public static String session(String sessionId) {
            return sessionId;
        }

    }
    //</editor-fold>

    private final Cache<String, Object> cache;

    private final CookieBuilder cookieBuilder;

    private final String cookieName;

    private final Duration timeout;

    public SessionService(Cache<String, Object> cache, CookieBuilder builder) {
        this(cache, builder, "session", DEFAULT_TIMEOUT);
    }

    public SessionService(Cache<String, Object> cache, CookieBuilder builder, String cookieName) {
        this(cache, builder, cookieName, DEFAULT_TIMEOUT);
    }

    public SessionService(Cache<String, Object> cache, CookieBuilder builder, String cookieName, long timeoutSeconds) {
        this(cache, builder, cookieName, Duration.ofSeconds(timeoutSeconds));
    }

    public SessionService(Cache<String, Object> cache, CookieBuilder builder, String cookieName, Duration timeout) {
        this.cache = checkNotNull(cache, "cache");
        this.cookieBuilder = checkNotNull(builder, "builder");
        this.cookieName = checkArgNotEmpty(cookieName, "cookieName");
        this.timeout = checkArg(timeout, timeout.isZero(), "Timeout must be greater than zero");
    }

    public Session get(HttpServletRequest request, HttpServletResponse response) {
        return get(request, response, false);
    }

    public Session get(HttpServletRequest request, HttpServletResponse response, boolean create) {
        Session session = null;
        Cookie cookie = getCookie(request);
        if (cookie != null) {
            session = getSession(cookie);
        }
        if (session == null && create) {
            session = createSession(response);
        }
        return expireSession(session);
    }

    public void update(Session session) {
        if (session != null && session.isChanged()) {
            cache.put(KeyChain.session(session), session, timeout);
            session.setChanged(false);
        }
    }

    public void delete(HttpServletRequest request, HttpServletResponse response) {
        Cookie cookie = getCookie(request);
        if (cookie != null) {
            response.addCookie(expireCookie(cookie));
            cache.delete(KeyChain.session(cookie.getValue()));
        }
    }

    private Cookie getCookie(HttpServletRequest request) {
        return cookieBuilder.getCookie(request.getCookies(), cookieName);
    }

    private Cookie expireCookie(Cookie cookie) {
        cookie.setMaxAge(0);
        return cookie;
    }

    private Session getSession(Cookie cookie) {
        return (Session) cache.get(KeyChain.session(cookie.getValue()));
    }

    private Session createSession(HttpServletResponse response) {
        return createSession(response, true);
    }

    private Session createSession(HttpServletResponse response, boolean create) {
        Session session = null;
        if (create) {
            String sessionId = randomUUID();
            session = new Session(sessionId);
            cache.put(KeyChain.session(sessionId), session, timeout);
            response.addCookie(cookieBuilder.build(cookieName, sessionId));
        }
        return session;
    }

    private Session expireSession(Session session) {
        if (session != null) {
            cache.expire(KeyChain.session(session), timeout);
        }
        return session;
    }

}
