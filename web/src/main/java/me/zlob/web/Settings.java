
package me.zlob.web;

import java.util.*;

import org.slf4j.*;

import me.zlob.nosql.redis.*;
import me.zlob.properties.*;

import static me.zlob.util.Conditions.*;

public class Settings {

    private static final Logger log = LoggerFactory.getLogger(Settings.class);

    //<editor-fold defaultstate="collapsed" desc="KeyChains">
    private static class RedisKeyChain {

        public static String prefix() {
            return "setting_";
        }

        public static String settingsMask() {
            return prefix() + "*";
        }

        public static String setting(String setting) {
            return prefix() + setting.replaceAll("\\.", "_");
        }

    }

    private static class SettingsKeyChain {

        public static String setting(String setting) {
            return setting.replaceAll("\\.", "_");
        }

    }
    //</editor-fold>

    private final Redis redis;

    private final PropertiesProcessor loader;

    private final Map<String, String> settings = new HashMap<>();

    private void save(Map<String, String> state, boolean override) {
        for (String key : state.keySet()) {
            String redisKey = RedisKeyChain.setting(key);
            if (override) {
                redis.set(redisKey, state.get(key));
            } else {
                redis.setnx(redisKey, state.get(key));
            }
        }
    }

    private Map<String, String> load(Set<String> keys) {
        Map<String, String> result = new HashMap<>();
        for (String key : keys) {
            String subKey = key.substring(RedisKeyChain.prefix().length());
            result.put(SettingsKeyChain.setting(subKey), redis.get(key));
        }
        return result;
    }

    private Set<String> getPersistedKeys() {
        return redis.keys(RedisKeyChain.settingsMask());
    }

    public Settings(Redis redis) {
        this(redis, PropertiesProcessors.fromEmpty());
    }

    public Settings(Redis redis, PropertiesProcessor loader) {
        this.redis = checkNotNull(redis, "redis");
        this.loader = checkNotNull(loader, "loader");
    }

    public void init() {
        save(loader.loadAsMap(), false);
        settings.putAll(load(getPersistedKeys()));
    }

    public void destroy() {
        save(settings, true);
    }

    public String set(String key, String value) {
        redis.set(RedisKeyChain.setting(key), value);
        return settings.put(SettingsKeyChain.setting(key), value);
    }

    public String get(String key) {
        String value = settings.get(key);
        if (notExistsOrEmpty(value)) {
            value = redis.get(RedisKeyChain.setting(key));
            if (existsNotEmpty(value)) {
                settings.put(SettingsKeyChain.setting(key), value);
            }
        }
        return nullable(value).orElse("");
    }

    public Set<String> getKeys() {
        return settings.keySet();
    }

}
