
package me.zlob.web;

import java.io.*;
import java.util.*;

public class Session implements Serializable {

    private final String id;

    private final Map<String, Object> attributes;

    private transient boolean changed = false;

    public Session(String id) {
        this(id, new HashMap<>());
    }

    public Session(String id, Map<String, Object> attributes) {
        this.id = id;
        this.attributes = attributes;
    }

    public String getId() {
        return id;
    }

    public Object getAttribute(String name) {
        return attributes.get(name);
    }

    public void setAttribute(String name, Object value) {
        changed = true;
        attributes.put(name, value);
    }

    public void removeAttribute(String name) {
        changed = true;
        attributes.remove(name);
    }

    public Set<String> getAttributeNames() {
        return attributes.keySet();
    }

    public boolean isChanged() {
        return changed;
    }

    public void setChanged(boolean changed) {
        this.changed = changed;
    }

}
