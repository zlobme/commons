
package me.zlob.web;

import java.util.*;

import org.slf4j.*;

import org.eclipse.jetty.server.*;
import org.eclipse.jetty.util.thread.*;
import org.eclipse.jetty.webapp.*;

import static me.zlob.properties.PropertiesProcessors.*;
import static me.zlob.util.Conditions.*;
import static me.zlob.util.Exceptions.*;
import static me.zlob.util.Util.*;

/**
 * Bootstrap embedded-jetty server.
 * Basic usage cause to start jetty server and stop monitor for it:
 * <br><br>
 * <code>
 * <pre>
 * public static void main(String[] args) {
 *     JettyLauncher.start(args);
 * }
 * </pre>
 * </code>
 * <br>
 * Need {@code webapp} directory in classpath. Control flow:
 * <ul>
 * <li> main class
 * <li> jetty launcher
 * <li> jetty server
 * <li> webapp/WEB-INF/web.xml
 * <li> *[spring context]
 * </ul>
 */
public class JettyLauncher {

    private static final Logger log = LoggerFactory.getLogger(JettyLauncher.class);

    private static final String PROPERTIES_DEFAULT = "me/zlob/web/jetty-default.properties";

    private static final ClassLoader CLASS_LOADER = JettyLauncher.class.getClassLoader();

    /**
     * Start jetty server.
     *
     * @param args may contain path to properties file.
     */
    public static void start(String[] args) {
        start(args != null && args.length > 0 ? args[0] : "");
    }

    /**
     * Start jetty server.
     *
     * @param propertiesFile path to properties file, nullable.
     */
    public static void start(String propertiesFile) {
        Map<String, String> properties = fromEmpty()
            .andThen(fromClasspath(PROPERTIES_DEFAULT, CLASS_LOADER))
            .andThen(fromFile(propertiesFile, false))
            .loadAsMap();
        start(properties);
    }

    /**
     * Start jetty server.
     */
    public static void start(Map<String, String> properties) {
        Server server = getServer(properties);
        rethrow(() -> server.start());
        getMonitor(server, properties).start();
        rethrow(() -> server.join());
    }

    protected static Server getServer(Map<String, String> props) {
        Server server = new Server(getThreadPool(props));
        server.setDumpAfterStart(false);
        server.setDumpBeforeStop(false);
        server.setStopAtShutdown(true);
        server.setHandler(getWebAppContext(props));
        server.addConnector(getServerConnector(server, props));
        return server;
    }

    protected static JettyStopMonitor getMonitor(Server server, Map<String, String> props) {
        int stopPort = parseInt(props.get("http.stop.port"), 8001);
        String command = props.getOrDefault("http.stop.command", "stop");
        return new JettyStopMonitor(server, stopPort, command);
    }

    protected static QueuedThreadPool getThreadPool(Map<String, String> props) {
        QueuedThreadPool threadPool = new QueuedThreadPool();
        threadPool.setMinThreads(parseInt(props.get("thread.pool.min"), 8));
        threadPool.setMaxThreads(parseInt(props.get("thread.pool.max"), 1024));
        threadPool.setIdleTimeout(parseInt(props.get("thread.idle.timeout"), 30000));
        return threadPool;
    }

    protected static HttpConfiguration getHttpConfiguration(Map<String, String> props) {
        HttpConfiguration config = new HttpConfiguration();
        config.setOutputBufferSize(parseInt(props.get("buffer.out.size"), 32768));
        config.setRequestHeaderSize(parseInt(props.get("request.header.size"), 8192));
        config.setResponseHeaderSize(parseInt(props.get("response.header.size"), 8192));
        config.setSendServerVersion(false);
        config.setSendDateHeader(false);
        return config;
    }

    protected static HttpConnectionFactory getHttpConnectionFactory(Map<String, String> props) {
        return new HttpConnectionFactory(getHttpConfiguration(props));
    }

    protected static WebAppContext getWebAppContext(Map<String, String> props) {
        WebAppContext context = new WebAppContext();
        context.setContextPath(props.getOrDefault("http.context", "/"));
        context.setDescriptor("webapp/WEB-INF/web.xml");
        context.setParentLoaderPriority(true);

        String resourceUrl = nullable(ClassLoader.getSystemClassLoader())
            .map(loader -> loader.getResource("webapp"))
            .map(address -> rethrow(address::toURI))
            .map(url -> url.toString())
            .orElseThrow(() -> createIllegalState("Can't get web app root from classpath."));

        context.setResourceBase(resourceUrl);
        return context;
    }

    protected static ServerConnector getServerConnector(Server server, Map<String, String> props) {
        ServerConnector http = new ServerConnector(server, 4, 8, getHttpConnectionFactory(props));
        http.setPort(parseInt(props.get("http.port"), 8000));
        http.setIdleTimeout(parseLong(props.get("http.idle.timeout"), 30000L));
        http.setAcceptQueueSize(parseInt(props.get("http.queue.size"), 8192));
        return http;
    }

}
