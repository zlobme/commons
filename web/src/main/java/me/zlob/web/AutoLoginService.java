
package me.zlob.web;

import javax.servlet.http.*;

import me.zlob.nosql.redis.*;
import me.zlob.util.*;

import static me.zlob.util.Conditions.*;
import static me.zlob.util.Util.*;

public class AutoLoginService {

    private static final int DEFAULT_TOKEN_TIME = (int) Time.YEAR;

    private static final int DEFAULT_SERIAL_TIME = (int) Time.YEAR;

    private final Redis redis;

    private final CookieBuilder cookieBuilder;

    private String tokenName;

    private String serialName;

    private int tokenTime;

    private int serialTime;

    public AutoLoginService(Redis redis, CookieBuilder cookieBuilder) {
        this(redis, cookieBuilder, "sessiont", "sessions");
    }

    public AutoLoginService(Redis redis, CookieBuilder cookieBuilder, String tokenName, String serialName) {
        this(redis, cookieBuilder, tokenName, serialName, DEFAULT_TOKEN_TIME, DEFAULT_SERIAL_TIME);
    }

    public AutoLoginService(
        Redis redis,
        CookieBuilder cookieBuilder,
        String tokenName,
        String serialName,
        int tokenTime,
        int serialTime
    ) {
        this.redis = checkNotNull(redis, "redis");
        this.cookieBuilder = checkNotNull(cookieBuilder, "cookieBuilder");
        this.tokenName = checkNotNull(tokenName, "tokenName");
        this.serialName = checkNotNull(serialName, "serialName");
        this.tokenTime = checkArg(tokenTime, tokenTime < 1, "Token TTL must be > 0");
        this.serialTime = checkArg(serialTime, serialTime < 1, "Serial TTL must be > 0");
    }

    public boolean startSerial(String userId, HttpServletResponse response) {
        boolean result = false;
        if (existsNotEmpty(userId)) {
            String serialId = randomUUID();
            redis.set(KeyChain.serial(serialId), userId);
            redis.expire(KeyChain.serial(serialId), serialTime);
            redis.sadd(KeyChain.serials(userId), serialId);
            response.addCookie(buildSerialCookie(serialId));

            String tokenId = randomUUID();
            redis.set(KeyChain.token(serialId, tokenId), userId);
            redis.expire(KeyChain.token(serialId, tokenId), tokenTime);
            response.addCookie(buildTokenCookie(tokenId));

            result = true;
        }
        return result;
    }

    public boolean stopSerial(String userId, HttpServletRequest request) {
        return stopSerial(userId, request.getCookies());
    }

    public boolean stopSerial(String userId, Cookie[] cookies) {
        return nullable(cookieBuilder.getCookie(cookies, serialName))
            .map(c -> stopSerial(userId, c.getValue()))
            .orElse(false);
    }

    public boolean stopSerial(String userId, String serial) {
        boolean result = false;
        String existedUserId = null;
        if (notExistsOrEmpty(userId)) {
            existedUserId = redis.get(KeyChain.serial(serial));
        }
        if (existsNotEmpty(existedUserId)
            && existedUserId.equals(userId)
            && existsNotEmpty(serial)) {
            result = redis.srem(KeyChain.serials(userId), serial) == 1;
        }
        return result;
    }

    public void refreshToken(String userId, HttpServletRequest request, HttpServletResponse response) {
        Cookie tokenCookie = refreshToken(userId, request);
        if (tokenCookie != null) {
            response.addCookie(tokenCookie);
        }
    }

    public Cookie refreshToken(String userId, HttpServletRequest request) {
        return refreshToken(userId, request.getCookies());
    }

    public Cookie refreshToken(String userId, Cookie[] cookies) {
        Cookie result = null;
        Cookie serialCookie = cookieBuilder.getCookie(cookies, serialName);
        Cookie tokenCookie = cookieBuilder.getCookie(cookies, tokenName);
        if (serialCookie != null && tokenCookie != null) {
            result = refreshToken(userId, serialCookie.getValue(), tokenCookie.getValue());
        }
        return result;
    }

    public Cookie refreshToken(String userId, String serial, String token) {
        Cookie result = null;
        if (existsNotEmpty(userId)) {
            redis.expire(KeyChain.token(serial, token), 60);

            String newToken = randomUUID();
            redis.set(KeyChain.token(serial, newToken), userId);
            redis.expire(KeyChain.token(serial, newToken), tokenTime);

            result = buildTokenCookie(newToken);
        }
        return result;
    }

    public String getUser(HttpServletRequest request) {
        return getUser(request.getCookies());
    }

    public String getUser(Cookie[] cookies) {
        String user = null;
        Cookie serialCookie = cookieBuilder.getCookie(cookies, serialName);
        Cookie tokenCookie = cookieBuilder.getCookie(cookies, tokenName);
        if (serialCookie != null && tokenCookie != null) {
            user = getUser(serialCookie.getValue(), tokenCookie.getValue());
        }
        return user;
    }

    public String getUser(String serial, String token) {
        String user = null;
        if (existsNotEmpty(serial) && existsNotEmpty(token)) {
            user = redis.get(KeyChain.token(serial, token));
        }
        return user;
    }

    public boolean isLegalUser(String userId, HttpServletRequest request) {
        return isLegalUser(userId, request.getCookies());
    }

    public boolean isLegalUser(String userId, Cookie[] cookies) {
        boolean result = false;
        Cookie serialCookie = cookieBuilder.getCookie(cookies, serialName);
        if (serialCookie != null) {
            result = isLegalUser(userId, serialCookie.getValue());
        }
        return result;
    }

    public boolean isLegalUser(String userId, String serial) {
        boolean result = false;
        if (existsNotEmpty(userId) && existsNotEmpty(serial)) {
            result = redis.sismember(KeyChain.serials(userId), serial);
        }
        return result;
    }

    public Cookie buildSerialCookie(String value) {
        return cookieBuilder.setMaxAge(serialTime).build(serialName, value);
    }

    public Cookie buildTokenCookie(String value) {
        return cookieBuilder.setMaxAge(tokenTime).build(tokenName, value);
    }

    public Cookie expireSerialCookie(String value) {
        return cookieBuilder.setMaxAge(0).build(serialName, value);
    }

    public Cookie expireTokenCookie(String value) {
        return cookieBuilder.setMaxAge(0).build(tokenName, value);
    }

    //<editor-fold defaultstate="collapsed" desc="getters/setters">
    public String getTokenName() {
        return tokenName;
    }

    public String getSerialName() {
        return serialName;
    }

    public static class KeyChain {

        public static String token(String serial, String token) {
            return "token_" + serial + "_" + sha256(token);
        }

        public static String serial(String serial) {
            return "serial_" + sha256(serial);
        }

        public static String serials(String userId) {
            return "serials_" + sha256(userId);
        }

    }
    //</editor-fold>

}
