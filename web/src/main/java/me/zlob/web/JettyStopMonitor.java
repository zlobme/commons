
package me.zlob.web;

import java.io.*;
import java.net.*;

import static java.net.InetAddress.*;

import org.slf4j.*;

import org.eclipse.jetty.server.*;

import me.zlob.util.*;

import static me.zlob.util.Conditions.*;

/**
 * Http monitor for listening command to stop jetty server.
 */
public class JettyStopMonitor extends Thread {

    private static final Logger log = LoggerFactory.getLogger(JettyStopMonitor.class);

    private static final String BAD_PORT = "Stop monitor port must be > 1000";

    private Server server;

    private ServerSocket serverSocket;

    private String stopCommand;

    public JettyStopMonitor(Server server, int port, String stopCommand) {
        super("jetty-stop-monitor");
        this.server = checkNotNull(server,"server");
        this.stopCommand = checkArgNotEmpty(stopCommand, "stopCommand");
        this.serverSocket = createServerSocket(checkArg(port, port <= 1000, BAD_PORT));
    }

    @Override
    public void run() {
        while (serverSocket != null) {
            try (Socket socket = serverSocket.accept()) {
                socket.setSoLinger(false, 0);
                try (InputStream is = socket.getInputStream()) {
                    String cmd = IO.read(is);
                    if (existsAndEqual(cmd, stopCommand)) {
                        Exceptions.rethrow(() -> {
                            log.info("Stopping server");
                            serverSocket.close();
                            serverSocket = null;
                            server.stop();
                        });
                    }
                }
            } catch (IOException ex) {
                log.error(ex.getMessage(), ex);
            }
        }
    }

    private ServerSocket createServerSocket(int port) {
        return Exceptions.rethrow(() -> {
            ServerSocket serverSocket;
            InetAddress address = getByName("127.0.0.1");
            serverSocket = new ServerSocket(port, 1, address);
            serverSocket.setReuseAddress(true);
            return serverSocket;
        });
    }

}
