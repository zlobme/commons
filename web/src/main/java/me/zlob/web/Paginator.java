
package me.zlob.web;

import java.util.*;

import me.zlob.util.*;

public class Paginator {

    public static List<String> getShort(Page page) {
        return getShort(page.getPage(), page.pages());
    }

    public static List<String> getShort(Page page, int max) {
        return getShort(page.getPage(), Math.min(page.pages(), max));
    }

    public static List<String> getShort(int page, int pages) {
        List<String> result = new ArrayList<>();
        if (pages > 1) {
            if (page > 1) {
                result.add("<");
            }
            if ((page <= 5 && pages <= 5) || (page < 5 && pages > 5)) {
                for (int i = 1; i <= Math.min(5, pages); i++) {
                    if (page == i) {
                        result.add("a");
                    } else {
                        result.add(i + "");
                    }
                }
            } else if (page == 5 && pages > 5) {
                result.add("1");
                result.add(".");
                result.add("4");
                result.add("a");
                result.add("6");
            } else if (page > 5 && pages > 5 && page < pages - 3) {
                result.add("1");
                result.add(".");
                if (page == pages) {
                    result.add((page - 2) + "");
                    result.add((page - 1) + "");
                    result.add("a");
                } else if (page == pages - 1) {
                    result.add((page - 1) + "");
                    result.add("a");
                    result.add((page + 1) + "");
                } else {
                    result.add((page - 1) + "");
                    result.add("a");
                    result.add((page + 1) + "");
                }
            } else if (page > 5 && pages > 5 && page >= pages - 3) {
                result.add("1");
                result.add(".");
                for (int i = pages - 3; i <= pages; i++) {
                    if (page == i) {
                        result.add("a");
                    } else {
                        result.add(i + "");
                    }
                }
            }
            if (pages > 5 && page < pages - 3) {
                result.add(".");
            }
            if (page < pages) {
                result.add(">");
            }
        }
        return result;
    }

    public static List<String> getLong(Page page) {
        return getLong(page.getPage(), page.pages());
    }

    public static List<String> getLong(Page page, int max) {
        return getLong(page.getPage(), Math.min(page.pages(), max));
    }

    public static List<String> getLong(int page, int pages) {
        List<String> result = new ArrayList<>();
        if (pages > 1) {
            if (page > 1) {
                result.add("<");
            }
            if (page <= 9 && pages <= 9) {
                for (int i = 1; i <= Math.min(9, pages); i++) {
                    if (page == i) {
                        result.add("a");
                    } else {
                        result.add(i + "");
                    }
                }
            } else if (page < 9 && pages > 9) {
                for (int i = 1; i <= Math.min(9, pages); i++) {
                    if (page == i) {
                        result.add("a");
                    } else {
                        result.add(i + "");
                    }
                }
            } else if (pages > 9 && (pages - page) < 6) {
                result.add("1");
                result.add(".");
                for (int i = Math.min(page, pages - 6); i <= pages; i++) {
                    if (page == i) {
                        result.add("a");
                    } else {
                        result.add(i + "");
                    }
                }
            } else if (page >= 9 && pages > 9) {
                result.add("1");
                result.add(".");
                if (page == pages) {
                    result.add((page - 3) + "");
                    result.add((page - 2) + "");
                    result.add((page - 1) + "");
                    result.add("a");
                } else if (page == pages - 1) {
                    result.add((page - 1) + "");
                    result.add("a");
                    result.add((page + 1) + "");
                } else {
                    result.add((page - 3) + "");
                    result.add((page - 2) + "");
                    result.add((page - 1) + "");
                    result.add("a");
                    result.add((page + 1) + "");
                    result.add((page + 2) + "");
                    result.add((page + 3) + "");
                }
            }
            if (pages > 9 && page <= (pages - 6)) {
                result.add(".");
            }
            if (page < pages) {
                result.add(">");
            }
        }
        return result;
    }

}
