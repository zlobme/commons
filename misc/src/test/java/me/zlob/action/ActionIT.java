
package me.zlob.action;

import java.io.*;

import com.google.gson.*;

import org.junit.*;
import static org.junit.Assert.*;

import static me.zlob.util.Conditions.*;

public class ActionIT {

    private String resource = "me/zlob/action/action-params.json";

    private static class HelloAction extends JsonInitializedAction<String> {

        private String name;

        public HelloAction(Gson gson) {
            super(gson);
        }

        @Override
        public void preAction() {
            name = (String) getParams().get("name");
            setInitialized(existsNotEmpty(name));
        }

        @Override
        public void doAction() {
            setResult("Hello, " + name + "!");
        }

    }

    @Test
    public void act_test() {
        InputStream is = ClassLoader.getSystemClassLoader().getResourceAsStream(resource);
        String message = new HelloAction(new Gson()).init(is).act().getResult();
        assertEquals("Hello, Vasia!", message);
    }

}
