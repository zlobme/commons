
package me.zlob.util;

import java.util.*;

import freemarker.template.*;

import org.junit.*;
import static org.junit.Assert.*;

public class TemplateProcessorIT {

    private Configuration cfg;

    @Before
    public void init() {
        cfg = new Configuration(new Version("2.3.23"));
    }

    @Test
    public void create_simple_template() {
        TemplateProcessor templates = new TemplateProcessor(cfg);
        assertNotNull(templates.create("template", "[#ftl]${name}"));
    }

    @Test
    public void process_simple_template() {
        TemplateProcessor templates = new TemplateProcessor(cfg);
        Template template = templates.create("template", "[#ftl]${name}");
        assertEquals("bob", templates.process(template, Collections.singletonMap("name", "bob")));
    }

    @Test
    public void create_missing_template() {
        TemplateProcessor templates = new TemplateProcessor(cfg);
        assertNull(templates.get("template"));
    }

}
