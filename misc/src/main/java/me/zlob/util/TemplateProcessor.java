
package me.zlob.util;

import java.io.*;
import java.util.*;

import org.slf4j.*;

import freemarker.template.*;
import lombok.*;

import static me.zlob.util.Conditions.*;

public class TemplateProcessor {

    private static final Logger log = LoggerFactory.getLogger(TemplateProcessor.class);

    private final String encoding;

    @Getter
    private final Configuration configuration;

    public TemplateProcessor(Configuration configuration) {
        this.configuration = checkNotNull(configuration, "configuration");
        this.encoding = "utf8";
    }

    public TemplateProcessor(Configuration configuration, String encoding) {
        this.configuration = checkNotNull(configuration, "configuration");
        this.encoding = checkNotNull(encoding, "encoding");
    }

    public Template get(String name) {
        Template result = null;
        try {
            result = configuration.getTemplate(name);
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        return result;
    }

    public Template create(String name, String content) {
        Template result = null;
        try {
            result = new Template(name, content, configuration);
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        return result;
    }

    public String process(String name, Map<String, Object> params) {
        String result = null;
        try {
            Template template = configuration.getTemplate(name, encoding);
            result = process(template, params);
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        return result;
    }

    public String process(Template template, Map<String, Object> params) {
        String result = null;
        try (StringWriter writer = new StringWriter()) {
            template.process(params, writer);
            result = writer.toString();
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        return result;
    }

}
