
package me.zlob.util;

import java.util.*;

import javax.mail.*;
import javax.mail.internet.*;

import org.slf4j.*;

import static me.zlob.util.Conditions.*;

public class MailSender {

    private static final Logger log = LoggerFactory.getLogger(MailSender.class);

    private final Properties properties;

    public MailSender(Properties properties) {
        this.properties = properties;
    }

    public boolean sendEmail(String to, String from, String subject, String text) {
        boolean result = false;
        if (existsNotEmpty(to) && subject != null && text != null) {
            Session session = Session.getDefaultInstance(properties);
            try {
                Transport transport = session.getTransport();
                transport.connect(properties.getProperty("mail.smtp.host"),
                                  Integer.parseInt(properties.getProperty("mail.smtp.port")),
                                  properties.getProperty("mail.smtp.user"),
                                  properties.getProperty("mail.smtp.pass"));

                MimeMessage message = new MimeMessage(session);
                message.setFrom(new InternetAddress(from));
                message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
                message.setSubject(subject, "utf8");
                message.setContent(text, "text/html; charset=utf-8");

                transport.sendMessage(message, message.getRecipients(Message.RecipientType.TO));
                result = true;
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
            }
        }
        return result;
    }

}
