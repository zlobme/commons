
package me.zlob.annotations;

public interface Indexer<Source, Result> {

    public Result index(Source bean);

}
