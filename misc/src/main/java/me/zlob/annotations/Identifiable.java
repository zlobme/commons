
package me.zlob.annotations;

public interface Identifiable {

    public long getUid();

    public void setUid(long uid);

}
