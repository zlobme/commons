
package me.zlob.annotations;

public interface Nameable {

    public String getName();

    public void setName(String name);

}
