
package me.zlob.annotations;

public interface Extractor<Type> {

    public Extractor init();

    public boolean hasNext();

    public Type extract();

}
