
package me.zlob.action;

import java.io.*;
import java.util.*;

public interface Action<Type> {

    public Action<Type> init(InputStream is);

    public Action<Type> init(Map<String, Object> params);

    public Action<Type> act();

    public Type getResult();

    public void close();

}
