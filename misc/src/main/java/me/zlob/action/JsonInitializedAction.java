
package me.zlob.action;

import java.io.*;
import java.util.*;

import com.google.gson.*;

import me.zlob.util.*;

import static me.zlob.util.Conditions.*;

public abstract class JsonInitializedAction<Type> extends AbstractAction<Type> {

    private final Gson gson;

    public JsonInitializedAction(Gson gson) {
        this.gson = checkNotNull(gson, "gson");
    }

    protected Map<String, Object> fromJson(String json) {
        //noinspection unchecked
        return gson.fromJson(json, Map.class);
    }

    @Override
    public Action<Type> init(InputStream is) {
        String json = IO.read(is);
        return init(fromJson(json));
    }

}
