
package me.zlob.action;

import java.util.*;

import lombok.*;

import static lombok.AccessLevel.*;

import static me.zlob.util.Conditions.*;

public abstract class AbstractAction<Type> implements Action<Type> {

    @Getter(value = PROTECTED)
    private Map<String, Object> params = Collections.emptyMap();

    @Getter(value = PROTECTED)
    @Setter(value = PROTECTED)
    private boolean initialized = false;

    @Getter
    @Setter(value = PROTECTED)
    private Type result = null;

    protected void preAction() {
    }

    protected abstract void doAction();

    protected void postAction() {
    }

    @Override
    public Action<Type> init(Map<String, Object> params) {
        this.params = checkNotNull(params, "params");
        this.initialized = true;
        return this;
    }

    @Override
    public Action<Type> act() {
        preAction();
        if (isInitialized()) {
            doAction();
        }
        postAction();
        return this;
    }

    @Override
    public void close() {
    }

}
