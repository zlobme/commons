
package me.zlob.functional;

import java.math.*;
import java.time.*;
import java.util.*;
import java.util.function.*;

import lombok.experimental.*;

/**
 * Factory of some useful accumulators like: min, max, sum.
 */
@UtilityClass
public class Accumulators {

    public static <Type extends Comparable<Type>> Accumulator<Type> min(Type init) {
        return new Accumulator<>(init, BinaryOperator.minBy(Comparator.naturalOrder()));
    }

    public static <Type extends Comparable<Type>> Accumulator<Type> max(Type init) {
        return new Accumulator<>(init, BinaryOperator.maxBy(Comparator.naturalOrder()));
    }

    public static Accumulator<Integer> sum(Integer init) {
        return new Accumulator<>(init, Integer::sum);
    }

    public static Accumulator<Long> sum(Long init) {
        return new Accumulator<>(init, Long::sum);
    }

    public static Accumulator<Float> sum(Float init) {
        return new Accumulator<>(init, Float::sum);
    }

    public static Accumulator<Double> sum(Double init) {
        return new Accumulator<>(init, Double::sum);
    }

    public static Accumulator<BigDecimal> sum(BigDecimal init) {
        return new Accumulator<>(init, BigDecimal::add);
    }

    public static Accumulator<Duration> sum(Duration init) {
        return new Accumulator<>(init, Duration::plus);
    }

}
