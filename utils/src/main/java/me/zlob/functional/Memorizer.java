package me.zlob.functional;

import java.util.function.*;

public class Memorizer<Type> implements Supplier<Type> {

    private boolean cached = false;

    private Type cachedValue;

    private Supplier<Type> supplier;

    public static <T> Memorizer<T> of(Supplier<T> supplier) {
        return new Memorizer<>(supplier);
    }

    protected Memorizer(Supplier<Type> supplier) {
        this.supplier = supplier;
    }

    public Type get() {
        if (!cached) {
            cachedValue = supplier.get();
            cached = true;
        }
        return cachedValue;
    }

}
