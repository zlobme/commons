
package me.zlob.functional;

import java.util.Map.*;

import lombok.*;

import static me.zlob.util.Conditions.*;

/**
 * Tuple for handle {@link Entry} data
 */
@ToString
@EqualsAndHashCode
public class Tuple<K, V> implements Entry<K, V> {

    @Getter
    private final K key;

    @Getter
    private V value;

    public static <K, V> Tuple<K, V> of(K key, V value) {
        return new Tuple<>(key, value);
    }

    /**
     * Create Tuple from {@link Entry} data
     */
    public Tuple(Entry<K, V> entry) {
        this(entry.getKey(), entry.getValue());
    }

    /**
     * Create Tuple from key and value
     */
    protected Tuple(K key, V value) {
        this.key = checkNotNull(key, "key");
        this.value = value;
    }

    @Override
    public V setValue(V value) {
        V oldValue = this.value;
        this.value = value;
        return oldValue;
    }

    public K first() {
        return key;
    }

    public V second() {
        return value;
    }

}
