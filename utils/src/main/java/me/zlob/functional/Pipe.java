
package me.zlob.functional;

import java.util.function.*;

/**
 * Function with same input and output type.
 */
@FunctionalInterface
public interface Pipe<Type> extends Function<Type, Type> {

}
