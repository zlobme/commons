
package me.zlob.functional;

import java.util.function.*;

import static me.zlob.util.Conditions.*;

/**
 * Consumer which accumulate result of binary operator.
 * <p><p>
 * Useful in streams API like:
 * <code><pre>
 * Stream.of(...)
 *     .peek(accumulator)
 *     .&lt;terminalOp&gt;();
 *
 * var value = accumulator.get();
 * </pre></code>
 */
public class Accumulator<Type> implements Consumer<Type>, Supplier<Type> {

    private Type accumulator;

    private BinaryOperator<Type> reduce;

    public Accumulator(Type accumulator, BinaryOperator<Type> reduce) {
        this.accumulator = checkNotNull(accumulator, "accumulator");
        this.reduce = checkNotNull(reduce, "reduce");
    }

    @Override
    public void accept(Type value) {
        accumulator = reduce.apply(accumulator, value);
    }

    @Override
    public Type get() {
        return accumulator;
    }

}
