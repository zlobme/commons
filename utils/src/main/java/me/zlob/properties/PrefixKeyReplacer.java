
package me.zlob.properties;

import java.util.*;

import static me.zlob.util.Conditions.*;

public class PrefixKeyReplacer implements PropertiesProcessor {

    private final String prefix;

    private final String replacement;

    public PrefixKeyReplacer(String prefix, String replacement) {
        this.prefix = checkNotNull(prefix, "prefix");
        this.replacement = checkNotNull(replacement, "replacement");
    }

    @Override
    public Properties process(Properties properties) {
        Properties result = new Properties();
        for (String key : properties.stringPropertyNames()) {
            String value = properties.getProperty(key);
            String newKey = key;
            if (key.startsWith(prefix)) {
                newKey = key.replaceFirst(prefix, replacement);
            }
            result.put(newKey, value);
        }
        return result;
    }

}
