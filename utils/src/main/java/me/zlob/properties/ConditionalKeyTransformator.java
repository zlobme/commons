
package me.zlob.properties;

import java.util.*;
import java.util.function.*;

import me.zlob.functional.*;

import static me.zlob.util.Conditions.*;

public class ConditionalKeyTransformator implements PropertiesProcessor {

    private final Predicate<String> condition;

    private final Pipe<String> transformation;

    public ConditionalKeyTransformator(Predicate<String> condition, Pipe<String> transformation) {
        this.condition = checkNotNull(condition, "condition");
        this.transformation = checkNotNull(transformation, "transformation");
    }

    @Override
    public Properties process(Properties properties) {
        Properties result = new Properties();
        for (String key : properties.stringPropertyNames()) {
            String value = properties.getProperty(key);
            String newKey = key;
            if (condition.test(key)) {
                newKey = transformation.apply(key);
            }
            result.put(newKey, value);
        }
        return result;
    }

}
