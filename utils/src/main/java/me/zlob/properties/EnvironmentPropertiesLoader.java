
package me.zlob.properties;

import java.util.*;

public class EnvironmentPropertiesLoader implements PropertiesProcessor {

    @Override
    public Properties process(Properties properties) {
        Properties result = new Properties();
        result.putAll(properties);
        result.putAll(System.getProperties());
        return result;
    }

}
