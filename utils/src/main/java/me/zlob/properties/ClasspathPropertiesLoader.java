
package me.zlob.properties;

import java.io.*;
import java.util.*;

import me.zlob.util.*;

import static me.zlob.util.Conditions.*;

public class ClasspathPropertiesLoader implements PropertiesProcessor {

    private final String resource;

    private final ClassLoader classLoader;

    public ClasspathPropertiesLoader(String resource) {
        this(resource, ClassLoader.getSystemClassLoader());
    }

    public ClasspathPropertiesLoader(String resource, ClassLoader classLoader) {
        this.resource = checkNotNull(resource, "resource");
        this.classLoader = checkNotNull(classLoader, "classLoader");
    }

    @Override
    public Properties process(Properties properties) {
        Properties result = new Properties();
        result.putAll(properties);
        InputStream is = classLoader.getResourceAsStream(resource);
        if (is != null) {
            result.putAll(IO.load(is));
            IO.close(is);
        }
        return result;
    }

}
