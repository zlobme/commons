
package me.zlob.properties;

import java.io.*;
import java.util.*;

import static me.zlob.util.Conditions.*;

public class PropertiesPrinter implements PropertiesProcessor {

    private final PrintStream printer;

    public PropertiesPrinter(PrintStream printer) {
        this.printer = checkNotNull(printer, "printer");
    }

    @Override
    public Properties process(Properties properties) {
        printer.println(properties);
        return properties;
    }

}
