
package me.zlob.properties;

import java.util.*;

import static me.zlob.util.Conditions.*;

public class PresetPropertiesLoader implements PropertiesProcessor {

    private final Properties preset;

    public PresetPropertiesLoader(Properties preset) {
        this.preset = checkNotNull(preset, "preset");
    }

    @Override
    public Properties process(Properties properties) {
        Properties result = new Properties();
        result.putAll(properties);
        result.putAll(preset);
        return result;
    }

}
