
package me.zlob.properties;

import java.util.*;

import static me.zlob.util.Conditions.*;

/**
 * Interface for classes which load, transform and extract {@link Properties} from different sources
 */
@FunctionalInterface
public interface PropertiesProcessor {

    /**
     * Method which make something with original properties,
     * must return new properties object.
     *
     * @param properties original properties, not null
     *
     * @return result properties, not null
     */
    public Properties process(Properties properties);

    /**
     * Produce processor witch combine processors in chain where processors apply {@link #process} method
     * in order: original first, another second.
     *
     * @param another not null
     */
    default PropertiesProcessor andThen(PropertiesProcessor another) {
        checkNotNull(another, "another");
        return (Properties properties) -> another.process(process(properties));
    }

    /**
     * Produce processor witch combine results of processors in order: original first, another second.
     *
     * @param another not null
     */
    default PropertiesProcessor joinWith(PropertiesProcessor another) {
        checkNotNull(another, "another");
        return (Properties properties) -> {
            Properties result = process(properties);
            result.putAll(another.process(new Properties()));
            return result;
        };
    }

    /**
     * Invoke {@link #process} and convert result to {@link Map}.
     */
    default Map<String, String> loadAsMap() {
        return convert(process(new Properties()));
    }

    /**
     * Convert {@link Properties} to {@link Map}.
     *
     * @param properties not null
     */
    default Map<String, String> convert(Properties properties) {
        Map<String, String> result = Collections.emptyMap();
        if (properties != null) {
            result = new HashMap<>();
            for (String key : properties.stringPropertyNames()) {
                result.put(key, properties.getProperty(key));
            }
        }
        return result;
    }

    /**
     * Convert {@link Map} to {@link Properties}.
     *
     * @param properties not null
     */
    default Properties convert(Map<String, String> properties) {
        Properties result = new Properties();
        if (properties != null) {
            result.putAll(properties);
        }
        return result;
    }

}
