
package me.zlob.properties;

import java.util.*;
import java.util.function.*;

import static me.zlob.util.Conditions.*;

public class ConditionalKeyFilter implements PropertiesProcessor {

    private final Predicate<String> filter;

    public ConditionalKeyFilter(Predicate<String> filter) {
        this.filter = checkNotNull(filter, "filter");
    }

    @Override
    public Properties process(Properties properties) {
        Properties result = new Properties();
        for (String key : properties.stringPropertyNames()) {
            if (filter.test(key)) {
                result.put(key, properties.getProperty(key));
            }
        }
        return result;
    }

}
