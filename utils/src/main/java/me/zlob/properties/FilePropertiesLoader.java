
package me.zlob.properties;

import java.io.*;
import java.util.*;

import me.zlob.util.*;

import static me.zlob.util.Conditions.*;

public class FilePropertiesLoader implements PropertiesProcessor {

    private final String file;

    public FilePropertiesLoader(String file) {
        this.file = checkNotNull(file, "file");
    }

    @Override
    public Properties process(Properties properties) {
        Properties result = new Properties();
        result.putAll(properties);
        if (IO.exists(new File(file))) {
            IO.load(result, file);
        }
        return result;
    }

}
