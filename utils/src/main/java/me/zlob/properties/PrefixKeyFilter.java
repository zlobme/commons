
package me.zlob.properties;

import java.util.*;

import static me.zlob.util.Conditions.*;

public class PrefixKeyFilter implements PropertiesProcessor {

    private final String prefix;

    public PrefixKeyFilter(String prefix) {
        this.prefix = checkNotNull(prefix, "prefix");
    }

    @Override
    public Properties process(Properties properties) {
        Properties result = new Properties();
        for (String key : properties.stringPropertyNames()) {
            if (key.startsWith(prefix)) {
                result.put(key, properties.getProperty(key));
            }
        }
        return result;
    }

}
