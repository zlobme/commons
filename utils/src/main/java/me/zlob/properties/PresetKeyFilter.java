
package me.zlob.properties;

import java.util.*;

import static me.zlob.util.Conditions.*;

public class PresetKeyFilter implements PropertiesProcessor {

    private final Set<String> keys;

    public PresetKeyFilter(Set<String> keys) {
        this.keys = checkNotNull(keys, "keys");
    }

    @Override
    public Properties process(Properties properties) {
        Properties result = new Properties();
        for (String key : properties.stringPropertyNames()) {
            if (keys.contains(key)) {
                result.put(key, properties.getProperty(key));
            }
        }
        return result;
    }

}
