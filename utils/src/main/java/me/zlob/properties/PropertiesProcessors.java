
package me.zlob.properties;

import java.io.*;
import java.util.*;
import java.util.function.*;

import me.zlob.functional.*;

public class PropertiesProcessors {

    public static PropertiesProcessor fromEmpty() {
        return new PresetPropertiesLoader(new Properties());
    }

    public static PropertiesProcessor fromPreset(Properties properties) {
        return new PresetPropertiesLoader(properties);
    }

    public static PropertiesProcessor fromClasspath(String resource) {
        return new ClasspathPropertiesLoader(resource);
    }

    public static PropertiesProcessor fromClasspath(String resource, ClassLoader classLoader) {
        return new ClasspathPropertiesLoader(resource, classLoader);
    }

    public static PropertiesProcessor fromFile(String file) {
        return new FilePropertiesLoader(file);
    }

    public static PropertiesProcessor fromFile(String file, boolean strict) {
        return file != null || strict ? fromFile(file) : fromEmpty();
    }

    public static PropertiesProcessor fromEnvironment() {
        return new EnvironmentPropertiesLoader();
    }

    public static PropertiesProcessor filterByKey(String prefix) {
        return new PrefixKeyFilter(prefix);
    }

    public static PropertiesProcessor filterByKey(Predicate<String> filter) {
        return new ConditionalKeyFilter(filter);
    }

    public static PropertiesProcessor filterByKeys(Set<String> filter) {
        return new PresetKeyFilter(filter);
    }

    public static PropertiesProcessor transformKeys(Predicate<String> condition, Pipe<String> transformation) {
        return new ConditionalKeyTransformator(condition, transformation);
    }

    public static PropertiesProcessor replaceKeyPrefix(String prefix, String replacement) {
        return new PrefixKeyReplacer(prefix, replacement);
    }

    public static PropertiesProcessor printTo(PrintStream printer) {
        return new PropertiesPrinter(printer);
    }

}
