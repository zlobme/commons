
package me.zlob.properties;

import java.util.*;
import java.util.function.*;

import static me.zlob.util.Conditions.*;

public class ConditionalValueFilter implements PropertiesProcessor {

    private final Predicate<String> filter;

    public ConditionalValueFilter(Predicate<String> filter) {
        this.filter = checkNotNull(filter, "filter");
    }

    @Override
    public Properties process(Properties properties) {
        Properties result = new Properties();
        for (String key : properties.stringPropertyNames()) {
            String value = properties.getProperty(key);
            if (filter.test(value)) {
                result.put(key, value);
            }
        }
        return result;
    }

}
