
package me.zlob.messages;

import java.util.*;
import java.util.function.*;

import static me.zlob.util.Conditions.*;

/**
 * Mixin to render localized message templates with args assertion
 */
public interface LocalizedMessageProvider extends MessageProvider {

    /**
     * @return bundle message key
     */
    public String name();

    /**
     * @return bundle name
     */
    public String getBundleName();

    /**
     * @return bundle for particular locale or fallback bundle
     */
    default ResourceBundle getBundle(Locale locale) {
        return ResourceBundle.getBundle(
            checkNotNull(getBundleName(), "bundleName"),
            checkNotNull(locale, "locale"));
    }

    /**
     * @return default locale
     */
    default Locale getLocale() {
        return Locale.getDefault();
    }

    /**
     * @return raw template string for default locale
     */
    default String getTemplate() {
        return getTemplate(getLocale());
    }

    /**
     * @return raw template string for locale
     */
    default String getTemplate(Locale locale) {
        checkNotNull(locale, "locale");
        return getBundle(locale).getString(name());
    }

    /**
     * @return rendered message for default locale with args count assertion
     * @throws IllegalStateException when args count miss match with expected
     */
    default String getMessage() {
        return getMessage(getLocale());
    }

    /**
     * @return rendered message for default locale with args count assertion
     * @throws IllegalStateException when args count miss match with expected
     */
    default String getMessage(Object... args) {
        int argsCount = args == null ? 0 : args.length;
        checkState(getArgsCount() != argsCount,
            "Arguments count doesn't match: expected %s, found %s",
            getArgsCount(), argsCount);

        return getMessage(getLocale(), args);
    }

    /**
     * @return rendered message for locale with args count assertion
     * @throws IllegalStateException when args count miss match with expected
     */
    default String getMessage(Locale locale, Object... args) {
        checkNotNull(locale, "locale");
        int argsCount = args == null ? 0 : args.length;
        checkState(getArgsCount() != argsCount,
            "Arguments count doesn't match: expected %s, found %s",
            getArgsCount(), argsCount);

        return String.format(getTemplate(locale), args);
    }

    /**
     * @param args for message rendering
     *
     * @return supplier what will render message with default locale as needed
     */
    default Supplier<String> messageOf(Object... args) {
        return messageOf(getLocale(), args);
    }

    /**
     * @param locale for message rendering
     * @param args for message rendering
     *
     * @return supplier what will render message as needed
     */
    default Supplier<String> messageOf(Locale locale, Object... args) {
        checkNotNull(locale, "locale");
        int argsCount = args == null ? 0 : args.length;
        checkState(getArgsCount() != argsCount,
            "Arguments count doesn't match: expected %s, found %s",
            getArgsCount(), argsCount);

        return () -> this.getMessage(locale, args);
    }

}
