
package me.zlob.messages;

import java.util.function.*;

import static me.zlob.util.Conditions.*;

/**
 * Mixin to render message templates with args assertion
 */
public interface MessageProvider {

    /**
     * @return Template name
     */
    public String name();

    /**
     * @return number of expected arguments
     */
    public int getArgsCount();

    /**
     * @return raw message template string
     */
    public String getTemplate();

    /**
     * @return rendered message with args count assertion
     * @throws IllegalStateException when args count miss match with expected
     */
    default String getMessage() {
        checkState(getArgsCount() != 0,
            "Arguments count doesn't match: expected %s, found %s",
            getArgsCount(), 0);

        return getTemplate();
    }

    /**
     * @return rendered message with args count assertion
     * @throws IllegalStateException when args count miss match with expected
     */
    default String getMessage(Object... args) {
        int argsCount = args == null ? 0 : args.length;
        checkState(getArgsCount() != argsCount,
            "Arguments count doesn't match: expected %s, found %s",
            getArgsCount(), argsCount);

        return String.format(getTemplate(), args);
    }

    /**
     * @param args for message rendering
     *
     * @return supplier what will render message as needed
     */
    default Supplier<String> messageOf(Object... args) {
        return () -> this.getMessage(args);
    }

}
