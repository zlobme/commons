
package me.zlob.util;

import java.util.function.*;

import org.slf4j.*;

import lombok.experimental.*;

/**
 * Utility helper for exceptions handling.
 */
@UtilityClass
public class Exceptions {

    public interface VoidCode {

        public void run() throws Exception;

    }

    public interface ResultCode<Result> {

        public Result call() throws Exception;

    }

    public static void ignore(VoidCode code) {
        execute(code, Exceptions::ignore);
    }

    public static void rethrow(VoidCode code) {
        execute(code, Exceptions::rethrow);
    }

    public static void rethrowIllegalState(VoidCode code) {
        execute(code, createThrower(Exceptions::createIllegalState));
    }

    public static void rethrow(VoidCode code, Function<Exception, RuntimeException> creator) {
        execute(code, createThrower(creator));
    }

    public static void log(VoidCode code, Logger logger) {
        execute(code, log(logger));
    }

    public static void execute(VoidCode code, Consumer<Exception> handler) {
        try {
            code.run();
        } catch (Exception ex) {
            handler.accept(ex);
        }
    }

    public static <Result> Result ignore(ResultCode<Result> code) {
        return execute(code, Exceptions::ignore);
    }

    public static <Result> Result rethrow(ResultCode<Result> code) {
        return execute(code, createThrower(Exceptions::createRuntime));
    }

    public static <Result> Result rethrowIllegalState(ResultCode<Result> code) {
        return execute(code, createThrower(Exceptions::createIllegalState));
    }

    public static <Result> Result rethrow(ResultCode<Result> code, Function<Exception, RuntimeException> creator) {
        return execute(code, createThrower(creator));
    }

    public static <Result> Result log(ResultCode<Result> code, Logger logger) {
        return execute(code, log(logger));
    }

    public static <Result> Result execute(ResultCode<Result> code, Consumer<Exception> handler) {
        Result result = null;
        try {
            result = code.call();
        } catch (Exception ex) {
            handler.accept(ex);
        }
        return result;
    }

    public static void ignore(Exception ex) {
    }

    public static void rethrow(Exception ex) {
        throw new RuntimeException(ex);
    }

    public static Consumer<Exception> rethrowIf(boolean conditional) {
        return ex -> {
            if (conditional) {
                throw new RuntimeException(ex);
            }
        };
    }

    public static Consumer<Exception> rethrowOrLog(Logger log, boolean conditional) {
        return ex -> {
            if (conditional) {
                throw new RuntimeException(ex);
            } else {
                log.error(ex.getMessage(), ex);
            }
        };
    }

    public static Consumer<Exception> log(Logger log) {
        return ex -> log.error(ex.getMessage(), ex);
    }

    public static Consumer<Exception> createThrower(Function<Exception, ? extends RuntimeException> creator) {
        return ex -> {
            throw creator.apply(ex);
        };
    }

    public static RuntimeException createRuntime(String message) {
        return new RuntimeException(message);
    }

    public static RuntimeException createRuntime(Exception ex) {
        return new RuntimeException(ex);
    }

    public static RuntimeException createRuntime(String message, Exception ex) {
        return new RuntimeException(message, ex);
    }

    public static IllegalStateException createIllegalState(String message) {
        return new IllegalStateException(message);
    }

    public static IllegalStateException createIllegalState(Exception ex) {
        return new IllegalStateException(ex);
    }

    public static IllegalStateException createIllegalState(String message, Exception ex) {
        return new IllegalStateException(message, ex);
    }

}
