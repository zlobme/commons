
package me.zlob.util;

import java.nio.charset.*;
import java.util.*;
import java.util.stream.*;

import static java.net.URLEncoder.*;
import static java.nio.charset.StandardCharsets.*;

import lombok.*;
import lombok.experimental.*;

import static me.zlob.util.Conditions.*;
import static me.zlob.util.Exceptions.*;

/**
 * Represent url build from different parts (protocol, domain, port, etc)
 */
@Accessors(fluent = true)
public class Url {

    private static final Charset DEFAULT_ENCODING = UTF_8;

    private static final Map<String, String> KNOWN_PORTS;

    static {
        KNOWN_PORTS = new HashMap<>();
        KNOWN_PORTS.put("80", "http");
        KNOWN_PORTS.put("443", "https");
    }

    @Setter
    private Charset encoding;

    @Setter
    private String protocol;

    @Setter
    private String domain;

    @Setter
    private String port;

    @Setter
    private String context;

    @Setter
    private String path;

    @Getter
    @Setter
    private Map<String, String> params;

    public static Url empty() {
        return new Url();
    }

    public static Url of(String url) {
        return of(url, DEFAULT_ENCODING);
    }

    public static Url of(String url, Charset encoding) {
        UrlParser urlParser = new UrlParser(url, encoding);
        return new Url(urlParser, encoding);
    }

    public Url() {
        this(DEFAULT_ENCODING);
    }

    public Url(Charset encoding) {
        this(new UrlParser(""), encoding);
    }

    public Url(UrlParser urlParser) {
        this(urlParser, DEFAULT_ENCODING);
    }

    public Url(UrlParser urlParser, Charset encoding) {
        this(
            urlParser.getProtocol(),
            urlParser.getDomain(),
            urlParser.getPort(),
            urlParser.getPath(),
            urlParser.getParams(),
            encoding
        );
    }

    public Url(
        String protocol,
        String domain,
        String port,
        String path,
        Map<String, String> params,
        Charset encoding
    ) {
        this(protocol, domain, port, "", path, params, encoding);
    }

    public Url(
        String protocol,
        String domain,
        String port,
        String context,
        String path,
        Map<String, String> params,
        Charset encoding
    ) {
        this.protocol = checkNotNull(protocol, "protocol");
        this.domain = checkNotNull(domain, "domain");
        this.port = checkNotNull(port, "port");
        this.context = checkNotNull(context, "context");
        this.path = checkNotNull(path, "path");
        this.params = checkNotNull(params, "params");
        this.encoding = checkNotNull(encoding, "encoding");
    }

    public Url set(String name) {
        params.put(name, "");
        return this;
    }

    public Url set(String name, String value) {
        params.put(name, value);
        return this;
    }

    public Url clear(String name) {
        params.remove(name);
        return this;
    }

    public String get() {
        return getDomainPart() + getPathPart() + getQueryPart();
    }

    @Override
    public String toString() {
        return get();
    }

    private String getDomainPart() {
        String result = "";
        if (existsNotEmpty(protocol)
            && existsNotEmpty(domain)) {
            result = protocol + ":";
        }
        if (existsNotEmpty(domain)) {
            result += "//" + domain;
            if (!KNOWN_PORTS.containsKey(port)) {
                result += ":" + port;
            }
        }
        return result;
    }

    private String getPathPart() {
        String normalContext = normalizeContext(context);
        String normalPath = normalizePath(path);

        String result;
        if ((normalContext.equals("") || normalContext.equals("/"))
            && (normalPath.equals("") || normalPath.equals("/"))) {
            result = domain.isEmpty() ? "/" : "";
        } else if (normalPath.equals("/")) {
            result = normalContext;
        } else {
            result = normalContext + normalPath;
        }

        return result;
    }

    private String normalizeContext(String context) {
        String prefix = context.startsWith("/") ? "" : "/";
        return prefix + IO.unslash(context, "/");
    }

    private String normalizePath(String path) {
        String prefix = path.startsWith("/") ? "" : "/";
        return prefix + path;
    }

    private String getQueryPart() {
        return !params.isEmpty() ? getQueryParameters(params) : "";
    }

    private String getQueryParameters(Map<String, String> params) {
        return params.entrySet().stream()
            .map(this::mapToKeyValue)
            .collect(Collectors.joining("&", "?", ""));
    }

    private String mapToKeyValue(Map.Entry<String, String> param) {
        String value = param.getValue();
        if (existsNotEmpty(value)) {
            value = encodeValue(value);
        }
        return param.getKey() + "=" + value;
    }

    private String encodeValue(String value) {
        return rethrow(() -> encode(value, encoding.name()));
    }

}
