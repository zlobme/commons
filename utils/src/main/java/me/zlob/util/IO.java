
package me.zlob.util;

import java.io.*;
import java.net.*;
import java.nio.charset.*;
import java.util.*;

import org.slf4j.*;

import lombok.experimental.*;

import static me.zlob.util.Conditions.*;

/**
 * Utility helper for read/write operations.
 */
@UtilityClass
public class IO {

    private static final Logger log = LoggerFactory.getLogger(IO.class);

    private static final Charset DEFAULT_CHARSET = StandardCharsets.UTF_8;

    //<editor-fold defaultstate="collapsed" desc="constants">
    public static final int KiB = 1024;

    public static final int MiB = 1024 * KiB;

    public static final int GiB = 1024 * MiB;

    public static final long TiB = 1024L * GiB;
    //</editor-fold>

    private static final int MAX_LENGTH = 10 * MiB;

    private static final int BUFFER_SIZE = 10 * KiB;

    public static byte[] charsToBytes(char[] chars) {
        byte[] bytes = new byte[chars.length];
        for (int i = 0; i < chars.length; i++) {
            bytes[i] = (byte) chars[i];
        }
        return bytes;
    }

    public static char[] bytesToChars(byte[] bytes) {
        char[] chars = new char[bytes.length];
        for (int i = 0; i < bytes.length; i++) {
            chars[i] = (char) bytes[i];
        }
        return chars;
    }

    public static String read(InputStream is) {
        return read(is, DEFAULT_CHARSET);
    }

    public static String read(InputStream is, Charset charset) {
        StringBuilder sb = new StringBuilder();
        InputStreamReader streamReader = new InputStreamReader(is, charset);
        BufferedReader reader = new BufferedReader(streamReader);
        try {
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line).append("\n");
            }
        } catch (IOException ex) {
            throw new UncheckedIOException(ex);
        }
        return sb.toString().trim();
    }

    public static byte[] readData(InputStream is, int length) {
        return readData(is, length, MAX_LENGTH, BUFFER_SIZE);
    }

    public static byte[] readData(InputStream is, int length, int maxLength, int bufferSize) {
        byte[] result = null;
        if (length > 0 && length <= maxLength) {
            int n;
            int currentLength = 0;
            byte[] buffer = new byte[bufferSize];
            ByteArrayOutputStream bais = new ByteArrayOutputStream();
            while (currentLength <= length && (n = readData(is, buffer)) > 0) {
                currentLength += n;
                bais.write(buffer, 0, n);
            }
            result = bais.toByteArray();
        }
        return result;
    }

    public static int readData(InputStream is, byte[] buffer) {
        int result;
        try {
            result = is.read(buffer);
        } catch (IOException ex) {
            throw new UncheckedIOException(ex);
        }
        return result;
    }

    public static byte[] readData(File file) {
        return readData(file, MAX_LENGTH);
    }

    public static byte[] readData(File file, int maxLength) {
        byte[] result = null;
        try {
            long length = file.length();
            if (length > 0 && length <= maxLength) {
                try (InputStream is = new FileInputStream(file)) {
                    result = readData(is, (int) length, maxLength, BUFFER_SIZE);
                }
            } else {
                log.debug("Length of file is exceed limit: {} > {}", length, maxLength);
            }
        } catch (IOException ex) {
            throw new UncheckedIOException(ex);
        }
        return result;
    }

    public static byte[] readData(URL url) {
        byte[] result = null;
        try {
            URLConnection connection = url.openConnection();
            long length = connection.getContentLengthLong();
            if (length > 0 && length <= MAX_LENGTH) {
                try (InputStream is = connection.getInputStream()) {
                    result = readData(is, (int) length);
                }
            }
        } catch (IOException ex) {
            throw new UncheckedIOException(ex);
        }
        return result;
    }

    public static byte[] readDataUrl(String url) {
        byte[] result;
        try {
            result = readData(new URL(url));
        } catch (IOException ex) {
            throw new UncheckedIOException(ex);
        }
        return result;
    }

    public static byte[] readDataFile(String file) {
        return readData(new File(file));
    }

    public static String read(URL url) {
        return read(url, DEFAULT_CHARSET);
    }

    public static String read(URL url, Charset charset) {
        String result;
        try (InputStream is = url.openStream()) {
            result = read(is, charset);
        } catch (IOException ex) {
            throw new UncheckedIOException(ex);
        }
        return result;
    }

    public static String read(URL url, String proxy, int port) {
        return read(url, DEFAULT_CHARSET, proxy, port);
    }

    public static String read(URL url, Charset charset, String proxy, int port) {
        String result;
        try {
            InetSocketAddress sa = new InetSocketAddress(proxy, port);
            Proxy p = new Proxy(Proxy.Type.HTTP, sa);
            try (InputStream is = url.openConnection(p).getInputStream()) {
                result = read(is, charset);
            }
        } catch (IOException ex) {
            throw new UncheckedIOException(ex);
        }
        return result;
    }

    public static String read(File file) {
        return read(file, DEFAULT_CHARSET);
    }

    public static String read(File file, Charset charset) {
        String result;
        try (InputStream is = new FileInputStream(file)) {
            result = read(is, charset);
        } catch (IOException ex) {
            throw new UncheckedIOException(ex);
        }
        return result;
    }

    public static String read(URLConnection connection) {
        return read(connection, DEFAULT_CHARSET);
    }

    public static String read(URLConnection connection, Charset charset) {
        String result;
        try (InputStream is = connection.getInputStream()) {
            result = read(is, charset);
        } catch (IOException ex) {
            throw new UncheckedIOException(ex);
        }
        return result;
    }

    public static String readURL(String url) {
        return readURL(url, DEFAULT_CHARSET);
    }

    public static String readURL(String url, Charset charset) {
        String result;
        try {
            result = read(new URL(url), charset);
        } catch (IOException ex) {
            throw new UncheckedIOException(ex);
        }
        return result;
    }

    public static String readURL(String url, String proxy, int port) {
        return readURL(url, DEFAULT_CHARSET, proxy, port);
    }

    public static String readURL(String url, Charset charset, String proxy, int port) {
        String result;
        try {
            result = read(new URL(url), charset, proxy, port);
        } catch (IOException ex) {
            throw new UncheckedIOException(ex);
        }
        return result;
    }

    public static String readFile(String file) {
        return readFile(file, DEFAULT_CHARSET);
    }

    public static String readFile(String file, Charset charset) {
        return read(new File(file), charset);
    }

    public static void writeData(OutputStream os, byte[] data) {
        writeData(os, data, data.length);
    }

    public static void writeData(OutputStream os, byte[] buffer, int length) {
        try {
            ByteArrayOutputStream writer = new ByteArrayOutputStream();
            writer.write(buffer, 0, length);
            writer.writeTo(os);
            writer.flush();
        } catch (IOException ex) {
            throw new UncheckedIOException(ex);
        }
    }

    public static void write(OutputStream os, String data) {
        write(os, data, DEFAULT_CHARSET);
    }

    public static void write(OutputStream os, String data, Charset charset) {
        try {
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, charset));
            writer.write(data);
            writer.flush();
        } catch (IOException ex) {
            throw new UncheckedIOException(ex);
        }
    }

    public static void writeDataFile(String file, byte[] data) {
        writeData(new File(file), data);
    }

    public static void writeData(File file, byte[] data) {
        try (OutputStream os = new FileOutputStream(file)) {
            writeData(os, data);
        } catch (IOException ex) {
            throw new UncheckedIOException(ex);
        }
    }

    public static void writeFile(String file, String data) {
        writeFile(file, data, DEFAULT_CHARSET);
    }

    public static void writeFile(String file, String data, Charset charset) {
        write(new File(file), data, charset);
    }

    public static void write(File file, String data) {
        write(file, data, DEFAULT_CHARSET);
    }

    public static void write(File file, String data, Charset charset) {
        try (OutputStream os = new FileOutputStream(file)) {
            write(os, data, charset);
        } catch (IOException ex) {
            throw new UncheckedIOException(ex);
        }
    }

    public static void write(URLConnection connection, String data) {
        try (OutputStream os = connection.getOutputStream()) {
            write(os, data);
        } catch (IOException ex) {
            throw new UncheckedIOException(ex);
        }
    }

    public static String fetch(String url) {
        return fetch(url, null, "GET");
    }

    public static String fetch(String url, String data) {
        return fetch(url, data, "POST");
    }

    public static String fetch(String url, String data, String method) {
        String result;
        HttpURLConnection connection = null;
        try {
            connection = (HttpURLConnection) new URL(url).openConnection();
            connection.setDoInput(true);
            if (data != null) {
                connection.setDoOutput(true);
                connection.setRequestMethod(method);
                write(connection, data);
            }
            result = read(connection);
        } catch (IOException ex) {
            throw new UncheckedIOException(ex);
        } finally {
            close(connection);
        }
        return result;
    }

    public static void close(InputStream is) {
        close(is, false);
    }

    public static void close(InputStream is, boolean throwException) {
        try {
            if (is != null) {
                is.close();
            }
        } catch (IOException ex) {
            if (throwException) {
                throw new UncheckedIOException(ex);
            } else {
                log.error(ex.getMessage(), ex);
            }
        }
    }

    public static void close(OutputStream os) {
        close(os, false);
    }

    public static void close(OutputStream os, boolean throwException) {
        try {
            if (os != null) {
                os.close();
            }
        } catch (IOException ex) {
            if (throwException) {
                throw new UncheckedIOException(ex);
            } else {
                log.error(ex.getMessage(), ex);
            }
        }
    }

    public static void close(HttpURLConnection connection) {
        if (connection != null) {
            connection.disconnect();
        }
    }

    public static void close(Socket socket) {
        close(socket, false);
    }

    public static void close(Socket socket, boolean throwException) {
        if (socket != null) {
            try {
                socket.close();
            } catch (IOException ex) {
                if (throwException) {
                    throw new UncheckedIOException(ex);
                } else {
                    log.error(ex.getMessage(), ex);
                }
            }
        }
    }

    public static Properties load(String file) {
        return load(new Properties(), file);
    }

    public static Properties load(Properties props, String file) {
        return load(props, new File(file));
    }

    public static Properties load(Properties props, File file) {
        if (exists(file)) {
            try (InputStream is = new FileInputStream(file)) {
                return load(props, is);
            } catch (IOException ex) {
                throw new UncheckedIOException(ex);
            }
        } else {
            log.info("can't load properties: " + file.getAbsolutePath());
        }
        return props;
    }

    public static Properties load(InputStream is) {
        return load(new Properties(), is);
    }

    public static Properties load(Properties props, InputStream is) {
        try {
            props.load(is);
        } catch (IOException ex) {
            throw new UncheckedIOException(ex);
        }
        return props;
    }

    public static boolean exists(String file) {
        return exists(new File(file));
    }

    public static boolean exists(File file) {
        boolean result = false;
        try {
            result = file.exists();
        } catch (SecurityException ex) {
            log.error("can't access file: " + file.getAbsolutePath(), ex);
        }
        return result;
    }

    public static boolean canRead(String file) {
        return canRead(new File(file));
    }

    public static boolean canRead(File file) {
        boolean result = false;
        try {
            result = file.canRead();
        } catch (SecurityException ex) {
            log.error("can't access file: " + file.getAbsolutePath(), ex);
        }
        return result;
    }

    public static boolean canWrite(String file) {
        return canWrite(new File(file));
    }

    public static boolean canWrite(File file) {
        boolean result = false;
        try {
            result = file.canWrite();
        } catch (SecurityException ex) {
            log.error("can't access file: " + file.getAbsolutePath(), ex);
        }
        return result;
    }

    public static String slash(String path) {
        return slash(path, File.separator);
    }

    public static String slash(String path, String delimiter) {
        return existsNotEmpty(path) && !path.endsWith(delimiter)
            ? path + delimiter
            : path;
    }

    public static String unslash(String path) {
        return unslash(path, File.separator);
    }

    public static String unslash(String path, String delimiter) {
        return existsNotEmpty(path) && path.endsWith(delimiter)
            ? path.substring(0, path.length() - 1)
            : path;
    }

    public static String folder(String path) {
        return folder(path, File.separator);
    }

    public static String folder(String path, String delimiter) {
        return existsNotEmpty(path) && !path.endsWith(delimiter)
            ? path.substring(0, path.lastIndexOf(delimiter) + 1)
            : path;
    }

    public static boolean delete(File file) {
        boolean result = false;
        if (file.exists()) {
            if (file.isFile()) {
                result = file.delete();
            } else if (file.isDirectory()) {
                result = deleteRecursive(file);
            }
        }
        return result;
    }

    private static boolean deleteRecursive(File directory) {
        boolean result = false;
        if (directory.exists()) {
            File[] files = directory.listFiles();
            if (files != null) {
                for (File file : files) {
                    if (file.isFile()) {
                        result |= file.delete();
                    } else if (file.isDirectory()) {
                        deleteRecursive(file);
                    }
                }
            }
            result = directory.delete();
        }
        return result;
    }

}
