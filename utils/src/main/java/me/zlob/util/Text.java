
package me.zlob.util;

import java.util.*;
import java.util.function.*;
import java.util.regex.*;

import lombok.experimental.*;

import static me.zlob.util.Conditions.*;

@UtilityClass
public class Text {

    private static final Pattern CYRILLIC_ALPHABETIC_PATTERN = Pattern.compile("[а-яА-Я]");

    private static final Map<Character, String> TRANSLITERATION_CHARS;

    //<editor-fold defaultstate="collapsed" desc="TRANSLITERATION_CHARS initialization">
    static {
        TRANSLITERATION_CHARS = new HashMap<>();
        TRANSLITERATION_CHARS.put('А', "A");
        TRANSLITERATION_CHARS.put('Б', "B");
        TRANSLITERATION_CHARS.put('В', "V");
        TRANSLITERATION_CHARS.put('Г', "G");
        TRANSLITERATION_CHARS.put('Д', "D");
        TRANSLITERATION_CHARS.put('Е', "E");
        TRANSLITERATION_CHARS.put('Ё', "E");
        TRANSLITERATION_CHARS.put('Ж', "Zh");
        TRANSLITERATION_CHARS.put('З', "Z");
        TRANSLITERATION_CHARS.put('И', "I");
        TRANSLITERATION_CHARS.put('Й', "I");
        TRANSLITERATION_CHARS.put('К', "K");
        TRANSLITERATION_CHARS.put('Л', "L");
        TRANSLITERATION_CHARS.put('М', "M");
        TRANSLITERATION_CHARS.put('Н', "N");
        TRANSLITERATION_CHARS.put('О', "O");
        TRANSLITERATION_CHARS.put('П', "P");
        TRANSLITERATION_CHARS.put('Р', "R");
        TRANSLITERATION_CHARS.put('С', "S");
        TRANSLITERATION_CHARS.put('Т', "T");
        TRANSLITERATION_CHARS.put('У', "U");
        TRANSLITERATION_CHARS.put('Ф', "F");
        TRANSLITERATION_CHARS.put('Х', "H");
        TRANSLITERATION_CHARS.put('Ц', "C");
        TRANSLITERATION_CHARS.put('Ч', "Ch");
        TRANSLITERATION_CHARS.put('Ш', "Sh");
        TRANSLITERATION_CHARS.put('Щ', "Sh");
        TRANSLITERATION_CHARS.put('Ъ', "'");
        TRANSLITERATION_CHARS.put('Ы', "Y");
        TRANSLITERATION_CHARS.put('Ь', "'");
        TRANSLITERATION_CHARS.put('Э', "E");
        TRANSLITERATION_CHARS.put('Ю', "U");
        TRANSLITERATION_CHARS.put('Я', "Ya");
        TRANSLITERATION_CHARS.put('а', "a");
        TRANSLITERATION_CHARS.put('б', "b");
        TRANSLITERATION_CHARS.put('в', "v");
        TRANSLITERATION_CHARS.put('г', "g");
        TRANSLITERATION_CHARS.put('д', "d");
        TRANSLITERATION_CHARS.put('е', "e");
        TRANSLITERATION_CHARS.put('ё', "e");
        TRANSLITERATION_CHARS.put('ж', "zh");
        TRANSLITERATION_CHARS.put('з', "z");
        TRANSLITERATION_CHARS.put('и', "i");
        TRANSLITERATION_CHARS.put('й', "i");
        TRANSLITERATION_CHARS.put('к', "k");
        TRANSLITERATION_CHARS.put('л', "l");
        TRANSLITERATION_CHARS.put('м', "m");
        TRANSLITERATION_CHARS.put('н', "n");
        TRANSLITERATION_CHARS.put('о', "o");
        TRANSLITERATION_CHARS.put('п', "p");
        TRANSLITERATION_CHARS.put('р', "r");
        TRANSLITERATION_CHARS.put('с', "s");
        TRANSLITERATION_CHARS.put('т', "t");
        TRANSLITERATION_CHARS.put('у', "u");
        TRANSLITERATION_CHARS.put('ф', "f");
        TRANSLITERATION_CHARS.put('х', "h");
        TRANSLITERATION_CHARS.put('ц', "c");
        TRANSLITERATION_CHARS.put('ч', "ch");
        TRANSLITERATION_CHARS.put('ш', "sh");
        TRANSLITERATION_CHARS.put('щ', "sh");
        TRANSLITERATION_CHARS.put('ъ', "");
        TRANSLITERATION_CHARS.put('ы', "y");
        TRANSLITERATION_CHARS.put('ь', "");
        TRANSLITERATION_CHARS.put('э', "e");
        TRANSLITERATION_CHARS.put('ю', "u");
        TRANSLITERATION_CHARS.put('я', "ya");
    }
    //</editor-fold>

    private static final Map<Character, String> KEYBOARD_CHARS;

    //<editor-fold defaultstate="collapsed" desc="KEYBOARD_CHARS initialization">
    static {
        KEYBOARD_CHARS = new HashMap<>();
        KEYBOARD_CHARS.put('й', "q");
        KEYBOARD_CHARS.put('ц', "w");
        KEYBOARD_CHARS.put('у', "e");
        KEYBOARD_CHARS.put('к', "r");
        KEYBOARD_CHARS.put('е', "t");
        KEYBOARD_CHARS.put('н', "y");
        KEYBOARD_CHARS.put('г', "u");
        KEYBOARD_CHARS.put('ш', "i");
        KEYBOARD_CHARS.put('щ', "o");
        KEYBOARD_CHARS.put('з', "p");
        KEYBOARD_CHARS.put('х', "[");
        KEYBOARD_CHARS.put('ъ', "]");
        KEYBOARD_CHARS.put('ф', "a");
        KEYBOARD_CHARS.put('ы', "s");
        KEYBOARD_CHARS.put('в', "d");
        KEYBOARD_CHARS.put('а', "f");
        KEYBOARD_CHARS.put('п', "g");
        KEYBOARD_CHARS.put('р', "h");
        KEYBOARD_CHARS.put('о', "j");
        KEYBOARD_CHARS.put('л', "k");
        KEYBOARD_CHARS.put('д', "l");
        KEYBOARD_CHARS.put('ж', ";");
        KEYBOARD_CHARS.put('э', "'");
        KEYBOARD_CHARS.put('я', "z");
        KEYBOARD_CHARS.put('ч', "x");
        KEYBOARD_CHARS.put('с', "c");
        KEYBOARD_CHARS.put('м', "v");
        KEYBOARD_CHARS.put('и', "b");
        KEYBOARD_CHARS.put('т', "n");
        KEYBOARD_CHARS.put('ь', "m");
        KEYBOARD_CHARS.put('б', ",");
        KEYBOARD_CHARS.put('ю', ".");
        KEYBOARD_CHARS.put('ё', "~");
    }
    //</editor-fold>

    private static final List<Character> PUNCTUATION_CHARS;

    //<editor-fold defaultstate="collapsed" desc="PUNCTUATION_CHARS initialization">
    static {
        PUNCTUATION_CHARS = new ArrayList<>();
        PUNCTUATION_CHARS.add('!');
        PUNCTUATION_CHARS.add('@');
        PUNCTUATION_CHARS.add('#');
        PUNCTUATION_CHARS.add('$');
        PUNCTUATION_CHARS.add('%');
        PUNCTUATION_CHARS.add('^');
        PUNCTUATION_CHARS.add('&');
        PUNCTUATION_CHARS.add('*');
        PUNCTUATION_CHARS.add('(');
        PUNCTUATION_CHARS.add(')');
        PUNCTUATION_CHARS.add('-');
        PUNCTUATION_CHARS.add('_');
        PUNCTUATION_CHARS.add('=');
        PUNCTUATION_CHARS.add('+');
        PUNCTUATION_CHARS.add('`');
        PUNCTUATION_CHARS.add('~');
        PUNCTUATION_CHARS.add('{');
        PUNCTUATION_CHARS.add('}');
        PUNCTUATION_CHARS.add('[');
        PUNCTUATION_CHARS.add(']');
        PUNCTUATION_CHARS.add('"');
        PUNCTUATION_CHARS.add('\'');
        PUNCTUATION_CHARS.add('\\');
        PUNCTUATION_CHARS.add('|');
        PUNCTUATION_CHARS.add('/');
        PUNCTUATION_CHARS.add('?');
        PUNCTUATION_CHARS.add(',');
        PUNCTUATION_CHARS.add('<');
        PUNCTUATION_CHARS.add('.');
        PUNCTUATION_CHARS.add('>');
        PUNCTUATION_CHARS.add(';');
        PUNCTUATION_CHARS.add(':');
    }
    //</editor-fold>

    public static String truncate(String string, int length) {
        String result = "";
        if (existsNotEmpty(string)) {
            if (string.length() >= length) {
                result = string.substring(0, length);
            } else {
                result = string;
            }
        }
        return result;
    }

    public static String filterChars(String string, Predicate<Character> predicate) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < string.length(); i++) {
            Character letter = string.charAt(i);
            boolean isAcceptable = predicate.test(letter);
            result.append(isAcceptable ? letter : ' ');
        }
        return result.toString().replaceAll("\\s+", " ").trim();
    }

    public static String clearChars(String string, Predicate<Character> predicate) {
        return filterChars(string, predicate.negate());
    }

    public static String extractLetters(String string) {
        return filterChars(string, Character::isLetter);
    }

    public static String extractDigits(String string) {
        return filterChars(string, Character::isDigit);
    }

    public static String clearSpecial(String string) {
        return filterChars(string, Character::isLetterOrDigit);
    }

    public static String clearNonPrint(String string) {
        return string.replaceAll("[\n\t\r\f\u000B\u00A0]", " ").replaceAll("\\s+", " ").trim();
    }

    public static String clearSpaces(String string) {
        return string.replaceAll("\\s+", "");
    }

    public static String clearLetters(String string) {
        return clearChars(string, Character::isLetter);
    }

    public static String clearDigits(String string) {
        return clearChars(string, Character::isDigit);
    }

    public static String clearChars(String string, List<Character> chars) {
        return clearChars(string, chars::contains);
    }

    public static String clearPunctuation(String string) {
        return clearChars(string, PUNCTUATION_CHARS);
    }

    public static String splitLettersAndDigits(String string) {
        StringBuilder result = new StringBuilder();
        int prev = 0;
        for (int i = 0; i < string.length(); i++) {
            char letter = string.charAt(i);
            if (Character.isLetter(letter)) {
                if (prev == 2) {
                    result.append(" ");
                }
                result.append(letter);
                prev = 1;
            } else if (Character.isDigit(letter)) {
                if (prev == 1) {
                    result.append(" ");
                }
                result.append(letter);
                prev = 2;
            } else {
                result.append(letter);
                prev = 3;
            }
        }
        return result.toString().replaceAll("\\s+", " ").trim();
    }

    public static String splitUpperCase(String string) {
        StringBuilder result = new StringBuilder();
        boolean isPreviousLower = false;
        for (int i = 0; i < string.length(); i++) {
            char letter = string.charAt(i);
            if (isPreviousLower && Character.isUpperCase(letter)) {
                result.append(" ");
            }
            result.append(letter);
            isPreviousLower = Character.isLowerCase(letter);
        }
        return result.toString();
    }

    public static String capitalize(String string) {
        String result = "";
        if (string != null && string.length() > 1) {
            result = string.substring(0, 1).toUpperCase() + string.substring(1);
        } else if (string != null && string.length() == 1) {
            result = string.toUpperCase();
        }
        return result;
    }

    public static String fixPunctuation(String string) {
        String result = "";
        if (string.length() > 0) {
            StringBuilder sb = new StringBuilder();
            sb.append(string.charAt(0));
            for (int i = 1; i < string.length(); i++) {
                char letter = string.charAt(i);
                if (letter == ','
                    && Character.isDigit(string.charAt(i - 1))
                    && Character.isDigit(string.charAt(i + 1))) {
                    sb.append('.');
                } else {
                    sb.append(letter);
                }
            }
            result = sb.toString().replaceAll(" , | ,|, |,", ", ").replaceAll(" ; | ;|; |;", "; ");
        }
        return result;
    }

    public static List<String> wordsSplit(String text) {
        return wordsSplit(text, " ");
    }

    public static List<String> wordsSplit(String text, String delimiter) {
        return Arrays.asList(text.split(delimiter));
    }

    public static int wordsCount(String text) {
        return wordsCount(text, " ");
    }

    public static int wordsCount(String text, String delimiter) {
        return wordsSplit(text, delimiter).size();
    }

    public static int wordsContains(Set<String> words, String text) {
        return wordsContains(words, text, " ");
    }

    public static int wordsContains(Set<String> words, String text, String delimiter) {
        return new TreeSet<>(wordsSplit(text, delimiter)).stream()
            .map(word -> words.contains(word) ? 1 : 0)
            .reduce(Integer::sum).orElse(0);
    }

    public static int wordsSimilarity(String word1, String word2) {
        int[][] distance = new int[word1.length() + 1][word2.length() + 1];

        for (int i = 0; i <= word1.length(); i++) {
            distance[i][0] = i;
        }
        for (int j = 0; j <= word2.length(); j++) {
            distance[0][j] = j;
        }

        for (int i = 1; i <= word1.length(); i++) {
            for (int j = 1; j <= word2.length(); j++) {
                int a = word1.charAt(i - 1) == word2.charAt(j - 1) ? 0 : 1;
                int d1 = distance[i - 1][j] + 1;
                int d2 = distance[i][j - 1] + 1;
                int d3 = distance[i - 1][j - 1] + a;
                distance[i][j] = Math.min(d1, Math.min(d2, d3));
            }
        }

        return distance[word1.length()][word2.length()];
    }

    public static boolean containsCyrillic(String text) {
        return CYRILLIC_ALPHABETIC_PATTERN.matcher(text).find();
    }

    public static String transliterate(String text) {
        return Text.transliterate(text, '-');
    }

    public static String transliterate(String text, char delimiter) {
        StringBuilder sb = new StringBuilder();
        for (char letter : text.toCharArray()) {
            if (TRANSLITERATION_CHARS.containsKey(letter)) {
                sb.append(TRANSLITERATION_CHARS.get(letter));
            } else if (Character.isAlphabetic(letter)) {
                sb.append(letter);
            } else if (Character.isDigit(letter)) {
                sb.append(letter);
            } else {
                sb.append(delimiter);
            }
        }
        String delimiterString = Character.toString(delimiter);
        String replacement = String.format("\\%s+", delimiter);
        String result = sb.toString().replaceAll(replacement, delimiterString);
        boolean needCut = false;
        int start = 0;
        if (result.startsWith(delimiterString)) {
            needCut = true;
            start++;
        }
        int end = result.length();
        if (result.endsWith(delimiterString)) {
            needCut = true;
            end--;
        }
        return needCut ? result.substring(start, end) : result;
    }

}
