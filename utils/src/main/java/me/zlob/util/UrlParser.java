
package me.zlob.util;

import java.nio.charset.*;
import java.util.*;

import static java.net.URLDecoder.*;
import static java.nio.charset.StandardCharsets.*;

import static me.zlob.util.Conditions.*;
import static me.zlob.util.Exceptions.*;

/**
 * Parse string url into different parts
 */
public class UrlParser {

    private static final Charset DEFAULT_ENCODING = UTF_8;

    private static final Map<String, String> KNOWN_PROTOCOLS;

    static {
        KNOWN_PROTOCOLS = new HashMap<>();
        KNOWN_PROTOCOLS.put("http", "80");
        KNOWN_PROTOCOLS.put("https", "443");
    }

    private final String url;

    private final Charset encoding;

    private String protocol = "";

    private String domain = "";

    private String port = "";

    private String path = "";

    private Map<String, String> params = new LinkedHashMap<>();

    private boolean parsed;

    public UrlParser(String url) {
        this(url, DEFAULT_ENCODING);
    }

    public UrlParser(String url, Charset encoding) {
        this.url = checkNotNull(url, "url");
        this.encoding = checkNotNull(encoding, "encoding");
    }

    public void ensureParsed() {
        if (!parsed) {
            parse();
            parsed = true;
        }
    }

    public String getProtocol() {
        ensureParsed();
        return protocol;
    }

    public String getDomain() {
        ensureParsed();
        return domain;
    }

    public String getPort() {
        ensureParsed();
        return port;
    }

    public String getPath() {
        ensureParsed();
        return path;
    }

    public Map<String, String> getParams() {
        ensureParsed();
        return params;
    }

    private void parse() {
        String pathPart;

        String remainPart = url;
        if (remainPart.startsWith("//")
            || !remainPart.startsWith("/")) {

            int pos = remainPart.indexOf("//");
            if (pos >= 1) {
                protocol = remainPart.substring(0, pos - 1);
                remainPart = remainPart.substring(pos + 2);
            } else if (pos >= 0) {
                protocol = remainPart.substring(0, pos);
                remainPart = remainPart.substring(pos + 2);
            }

            String domainPart;

            pos = remainPart.indexOf("/");
            if (pos > 0) {
                domainPart = remainPart.substring(0, pos);
                pathPart = remainPart.substring(pos);
            } else {
                domainPart = remainPart;
                pathPart = "/";
            }
            pos = domainPart.indexOf(":");
            if (pos > 0) {
                domain = domainPart.substring(0, pos);
                port = domainPart.substring(pos + 1);
            } else {
                domain = domainPart;
                if (existsNotEmpty(protocol)) {
                    port = getKnownPort(protocol);
                }
            }
        } else {
            pathPart = remainPart;
        }

        String queryPart = "";
        if (existsNotEmpty(pathPart)) {
            int pos = pathPart.indexOf("?");
            if (pos > 0) {
                path = pathPart.substring(0, pos);
                queryPart = pathPart.substring(pos + 1);
            } else {
                path = pathPart;
            }
        }
        if (existsNotEmpty(queryPart)) {
            params = new LinkedHashMap<>();
            for (String param : queryPart.split("&")) {
                String[] split = param.split("=");
                if (split.length == 2) {
                    String value = decodeValue(split[1]);
                    params.put(split[0], value);
                } else {
                    params.put(split[0], "");
                }
            }
        }
    }

    private String decodeValue(String value) {
        return rethrow(() -> decode(value, encoding.name()));
    }

    private String getKnownPort(String protocol) {
        return KNOWN_PROTOCOLS.getOrDefault(protocol, "");
    }

}
