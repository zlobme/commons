
package me.zlob.util;

import java.text.*;
import java.util.*;
import java.util.function.*;

import org.slf4j.*;

import static me.zlob.util.Conditions.*;

/**
 * Utility methods for work with old Date API
 */
public class Time {

    private static final Logger log = LoggerFactory.getLogger(Time.class);

    //<editor-fold defaultstate="collapsed" desc="Time unit constants">
    public static final long NANOS = 1_000_000;

    public static final long MILLIS = 1000;

    public static final long SECOND = 1;

    public static final long MINUTE = 60 * SECOND;

    public static final long HOUR = 60 * MINUTE;

    public static final long DAY = 24 * HOUR;

    public static final long WEEK = 7 * DAY;

    public static final long YEAR = 365 * DAY;
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Patterns">
    public static final String DATE_PATTERN = "dd.MM.yyyy";

    public static final String TIME_PATTERN = "HH:mm";

    public static final String DATE_TIME_PATTERN = "HH:mm dd.MM.yyyy";

    public static final String ISO_DATE_TIME_PATTERN = "yyyy-MM-dd'T'HH:mm:ssz";

    public static final String LAST_MODIFIED_DATE_PATTERN = "EEE, dd MMM yyyy HH:mm:ss ZZZ";
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Calendar">
    private static final ThreadLocal<Calendar> TL_CALENDAR;

    static {
        TL_CALENDAR = ThreadLocal.withInitial(Calendar::getInstance);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Formatters">
    private static final ThreadLocal<SimpleDateFormat> TL_DATE_FORMATTER;

    private static final ThreadLocal<SimpleDateFormat> TL_TIME_FORMATTER;

    private static final ThreadLocal<SimpleDateFormat> TL_DATE_TIME_FORMATTER;

    private static final ThreadLocal<SimpleDateFormat> TL_ISO_DATE_TIME_FORMATTER;

    private static final ThreadLocal<SimpleDateFormat> TL_LAST_MODIFIED_DATE_FORMATTER;

    static {
        TL_DATE_FORMATTER = ThreadLocal
            .withInitial(formatterOf(DATE_PATTERN));

        TL_TIME_FORMATTER = ThreadLocal
            .withInitial(formatterOf(TIME_PATTERN));

        TL_DATE_TIME_FORMATTER = ThreadLocal
            .withInitial(formatterOf(DATE_TIME_PATTERN));

        TL_ISO_DATE_TIME_FORMATTER = ThreadLocal
            .withInitial(formatterOf(ISO_DATE_TIME_PATTERN));

        TL_LAST_MODIFIED_DATE_FORMATTER = ThreadLocal
            .withInitial(formatterOf(LAST_MODIFIED_DATE_PATTERN));
    }

    private static Supplier<SimpleDateFormat> formatterOf(String format) {
        return () -> new SimpleDateFormat(format);
    }
    //</editor-fold>

    public static Date parse(String date, String pattern) {
        return parse(date, pattern, false);
    }

    public static Date parse(String date, SimpleDateFormat formatter, boolean throwException) {
        Date result = null;
        try {
            result = formatter.parse(date);
        } catch (ParseException ex) {
            if (throwException) {
                throw new RuntimeException(ex);
            } else {
                log.error(ex.getMessage(), ex);
            }
        }
        return result;
    }

    public static Date parse(String date, String pattern, boolean throwException) {
        return parse(date, new SimpleDateFormat(pattern), throwException);
    }

    public static Date parseDate(String date) {
        return parseDate(date, false);
    }

    public static Date parseDate(String date, boolean throwException) {
        return parse(date, TL_DATE_FORMATTER.get(), throwException);
    }

    public static Date parseTime(String date) {
        return parseTime(date, false);
    }

    public static Date parseTime(String date, boolean throwException) {
        return parse(date, TL_TIME_FORMATTER.get(), throwException);
    }

    public static Date parseDateTime(String date) {
        return parseDateTime(date, false);
    }

    public static Date parseDateTime(String date, boolean throwException) {
        return parse(date, TL_DATE_TIME_FORMATTER.get(), throwException);
    }

    public static Date parseIsoDate(String date) {
        return parseIsoDate(date, false);
    }

    public static Date parseIsoDate(String date, boolean throwException) {
        return parse(date, TL_ISO_DATE_TIME_FORMATTER.get(), throwException);
    }

    public static Date parseLastModifiedDate(String date) {
        return parseLastModifiedDate(date, false);
    }

    public static Date parseLastModifiedDate(String date, boolean throwException) {
        return parse(date, TL_LAST_MODIFIED_DATE_FORMATTER.get(), throwException);
    }

    public static String format(Date date, String pattern) {
        return format(date, new SimpleDateFormat(pattern));
    }

    public static String format(Date date, SimpleDateFormat formatter) {
        return formatter.format(date);
    }

    public static String formatDate(Date date) {
        return format(date, TL_DATE_FORMATTER.get());
    }

    public static String formatTime(Date date) {
        return format(date, TL_TIME_FORMATTER.get());
    }

    public static String formatDateTime(Date date) {
        return format(date, TL_DATE_TIME_FORMATTER.get());
    }

    public static String formatIsoDate(Date date) {
        return format(date, TL_ISO_DATE_TIME_FORMATTER.get());
    }

    public static String formatLastModifiedDate(Date date) {
        return format(date, TL_LAST_MODIFIED_DATE_FORMATTER.get());
    }

    /**
     * Set specific time to date
     *
     * @param time format 'HH:MM' or 'HH:mm:ss'
     */
    public static Date setTime(Date date, String time) {
        checkNotNull(time, "time");

        String[] splitTime = time.split(":");
        checkState(splitTime.length < 2, "Time must be 'HH:mm' or 'HH:mm:ss' format but was: %s", time);

        int hours = Integer.parseInt(splitTime[0]);
        checkState(hours < 0 || hours > 23, "Hour must be in [0, 23] but was: %s", splitTime[0]);

        int minutes = Integer.parseInt(splitTime[1]);
        checkState(0 > minutes || minutes > 59, "Minutes must be in [0, 59] but was: %s", splitTime[1]);

        int seconds = 0;
        if (splitTime.length > 2) {
            seconds = Integer.parseInt(splitTime[1]);
            checkState(0 > seconds || seconds > 59, "Seconds must be in [0, 59] but was: %s", splitTime[1]);
        }

        return setTime(date, hours, minutes, seconds);
    }

    public static Date setTime(Date date, int hours, int minutes, int seconds) {
        Calendar calendar = TL_CALENDAR.get();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, hours);
        calendar.set(Calendar.MINUTE, minutes);
        calendar.set(Calendar.SECOND, seconds);
        return calendar.getTime();
    }

    public static Date addMinute(Date date, int amount) {
        return add(date, Calendar.MINUTE, amount);
    }

    public static Date addHour(Date date, int amount) {
        return add(date, Calendar.HOUR_OF_DAY, amount);
    }

    public static Date addDay(Date date, int amount) {
        return add(date, Calendar.DAY_OF_YEAR, amount);
    }

    public static Date addMonth(Date date, int amount) {
        return add(date, Calendar.MONTH, amount);
    }

    private static Date add(Date date, int type, int amount) {
        Date result = null;
        if (date != null) {
            Calendar calendar = TL_CALENDAR.get();
            calendar.setTime(date);
            calendar.add(type, amount);
            result = calendar.getTime();
        }
        return result;
    }

    public static int getDayOfWeek(Date date) {
        Calendar calendar = TL_CALENDAR.get();
        calendar.setTime(date);
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        switch (day) {
            case Calendar.MONDAY:
                return 1;
            case Calendar.TUESDAY:
                return 2;
            case Calendar.WEDNESDAY:
                return 3;
            case Calendar.THURSDAY:
                return 4;
            case Calendar.FRIDAY:
                return 5;
            case Calendar.SATURDAY:
                return 6;
            case Calendar.SUNDAY:
                return 7;
            default:
                return 0;
        }
    }

    public static int getDayOfMonth(Date date) {
        Calendar calendar = TL_CALENDAR.get();
        calendar.setTime(date);
        return calendar.get(Calendar.DAY_OF_MONTH);
    }

    public static Date getTomorrow() {
        return getTomorrow(new Date());
    }

    public static Date getTomorrow(Date date) {
        return addDay(date, 1);
    }

    public static Date getYesterday() {
        return getYesterday(new Date());
    }

    public static Date getYesterday(Date date) {
        return addDay(date, -1);
    }

    public static Date truncateMillis(Date date) {
        return new Date(((date.getTime() / MILLIS)) * MILLIS);
    }

}
