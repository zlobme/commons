
package me.zlob.util;

import com.google.gson.*;

public class GsonFactory {

    public static Gson getGson() {
        return getGson(false);
    }

    public static Gson getGson(boolean pretty) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat(Time.ISO_DATE_TIME_PATTERN);
        if (pretty) {
            gsonBuilder.setPrettyPrinting();
        }
        return gsonBuilder.create();
    }

}
