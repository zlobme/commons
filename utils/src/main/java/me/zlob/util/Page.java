
package me.zlob.util;

import lombok.*;
import lombok.experimental.*;

@Accessors(chain = true)
public class Page {

    @Getter
    private int page;

    @Getter
    private int onPage;

    @Getter
    @Setter
    private int count = 0;

    public Page(int page, int onPage) {
        this.page = page;
        this.onPage = onPage;
    }

    public Page(int page, int onPage, int count) {
        this.page = page;
        this.onPage = onPage;
        this.count = count;
    }

    public int pages() {
        return (count / onPage) + (count % onPage > 0 ? 1 : 0);
    }

    public int offset() {
        int result = Math.max(page - 1, 0) * onPage;
        return count > 0 ? Math.min(result, count) : result;
    }

    public int limit() {
        int result = page * onPage;
        return count > 0 ? Math.min(result, count) : result;
    }

    public Page prev() {
        this.page--;
        return this;
    }

    public Page next() {
        this.page++;
        return this;
    }

}
