
package me.zlob.util;

import lombok.*;

import static lombok.AccessLevel.*;

import static me.zlob.util.Conditions.*;

public abstract class Resource<Type> implements AutoCloseable {

    @Getter(value = PROTECTED)
    private Type origin;

    public Resource(Type origin) {
        this.origin = checkNotNull(origin);
    }

    @Override
    public abstract void close();

}
