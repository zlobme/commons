package me.zlob.util;

import static me.zlob.util.Conditions.*;

/**
 * Utility to reduce wide of comparision expression, support operators ==, !=, >, >=, <, <=.
 * Example snippet:
 * <br/><br/>
 * <code><pre>
 * boolean condition = superPuperImportantVariable == anotherSuperPuperImportantVariable;
 *
 * boolean condition = Comparison
 *     .of(superPuperImportantVariable)
 *     .with(anotherSuperPuperImportantVariable)
 *     .isEqual();
 * </pre></code>
 */
public class Comparison<Type extends Comparable<Type>> {

    //<editor-fold defaultstate="collapsed" desc="ComparingValue">
    public static class ComparingValue<Type extends Comparable<Type>> {

        private Type origin;

        public ComparingValue(Type origin) {
            this.origin = checkNotNull(origin, "origin");
        }

        public Comparison<Type> with(Type competitor) {
            return new Comparison<>(origin, checkNotNull(competitor, "competitor"));
        }

    }
    //</editor-fold>

    private Type origin;

    private Type competitor;

    public static <Type extends Comparable<Type>> ComparingValue<Type> of(Type origin) {
        return new ComparingValue<>(origin);
    }

    public static <Type extends Comparable<Type>> Type min(Type first, Type second) {
        return first.compareTo(second) > 0 ? second : first;
    }

    public static <Type extends Comparable<Type>> Type max(Type first, Type second) {
        return first.compareTo(second) < 0 ? second : first;
    }

    public Comparison(Type origin, Type competitor) {
        this.origin = checkNotNull(origin, "origin");
        this.competitor = checkNotNull(competitor, "competitor");
    }

    public boolean isGreater() {
        return origin.compareTo(competitor) > 0;
    }

    public boolean isGreaterOrEqual() {
        return origin.compareTo(competitor) >= 0;
    }

    public boolean isLess() {
        return origin.compareTo(competitor) < 0;
    }

    public boolean isLessOrEqual() {
        return origin.compareTo(competitor) <= 0;
    }

    public boolean isEqual() {
        return origin.compareTo(competitor) == 0;
    }

    public boolean isNotEqual() {
        return origin.compareTo(competitor) != 0;
    }

    public Type min() {
        return isGreater() ? competitor : origin;
    }

    public Type max() {
        return isLess() ? competitor : origin;
    }

}
