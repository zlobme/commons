
package me.zlob.util;

import java.io.*;
import java.math.*;
import java.nio.charset.*;
import java.security.*;
import java.util.*;

import org.slf4j.*;

import lombok.experimental.*;

import static me.zlob.util.Conditions.*;

/**
 * Some digest and other utils
 */
@UtilityClass
public class Util {

    private static final Logger log = LoggerFactory.getLogger(Util.class);

    private static final Charset DEFAULT_CHARSET = StandardCharsets.UTF_8;

    private static final ThreadLocal<MessageDigest> TL_MD5;

    private static final ThreadLocal<MessageDigest> TL_SHA256;

    static {
        TL_MD5 = ThreadLocal.withInitial(Util::createMD5);
        TL_SHA256 = ThreadLocal.withInitial(Util::createSHA256);
    }

    /**
     * Use for debug, shortcut to {@code System.out.println(String.format(...));}
     */
    public static void printf(String format, Object... args) {
        printf(System.out, format, args);
    }

    /**
     * Use for debug, shortcut to {@code stream.println(String.format(...));}
     */
    public static void printf(PrintStream stream, String format, Object... args) {
        stream.println(String.format(format, args));
    }

    public static String base64Encode(String value) {
        return base64Encode(value, DEFAULT_CHARSET);
    }

    public static String base64Encode(String value, Charset charset) {
        byte[] valueBytes = value.getBytes(charset);
        return base64Encode(valueBytes, charset);
    }

    public static String base64Encode(byte[] valueBytes, Charset charset) {
        byte[] encodeBytes = Base64.getUrlEncoder().encode(valueBytes);
        String hash = new String(encodeBytes, charset);
        return hash;
    }

    public static String base64Decode(String value) {
        return base64Decode(value, DEFAULT_CHARSET);
    }

    public static String base64Decode(String value, Charset charset) {
        byte[] valueBytes = value.getBytes(charset);
        return base64Decode(valueBytes, charset);
    }

    public static String base64Decode(byte[] valueBytes, Charset charset) {
        byte[] encodeBytes = Base64.getUrlDecoder().decode(valueBytes);
        String hash = new String(encodeBytes, charset);
        return hash;
    }

    public static String md5(String value) {
        return md5(value, DEFAULT_CHARSET);
    }

    public static String md5(String value, Charset charset) {
        byte[] valueBytes = value.getBytes(charset);
        TL_MD5.get().update(valueBytes);
        byte[] encodedBytes = TL_MD5.get().digest();
        String hash = new BigInteger(1, encodedBytes).toString(16);
        return adjustSize(hash, 32);
    }

    public static String sha256(String value) {
        return sha256(value, DEFAULT_CHARSET);
    }

    public static String sha256(String value, Charset charset) {
        byte[] valueBytes = value.getBytes(charset);
        TL_SHA256.get().update(valueBytes);
        byte[] encodedBytes = TL_SHA256.get().digest();
        String hash = new BigInteger(1, encodedBytes).toString(16);
        return adjustSize(hash, 64);
    }

    public static String randomUUID() {
        return UUID.randomUUID().toString();
    }

    public static String formatUUID(UUID uuid) {
        return formatUUID(uuid.toString());
    }

    public static String formatUUID(String uuid) {
        return uuid.replaceAll("-", "");
    }

    public static Integer parseInt(String value) {
        return parseInt(value, null);
    }

    public static Integer parseInt(String value, Integer defaultValue) {
        if (existsNotEmpty(value)) {
            try {
                return Integer.parseInt(value);
            } catch (NumberFormatException ex) {
                if (defaultValue == null) {
                    log.error(ex.getMessage(), ex);
                }
            }
        }
        return defaultValue;
    }

    public static Long parseLong(String value) {
        return parseLong(value, null);
    }

    public static Long parseLong(String value, Long defaultValue) {
        if (existsNotEmpty(value)) {
            try {
                return Long.parseLong(value);
            } catch (NumberFormatException ex) {
                if (defaultValue == null) {
                    log.error(ex.getMessage(), ex);
                }
            }
        }
        return defaultValue;
    }

    public static Boolean parseBoolean(String value) {
        return parseBoolean(value, null);
    }

    public static Boolean parseBoolean(String value, Boolean defaultValue) {
        if (existsNotEmpty(value)) {
            return Boolean.parseBoolean(value);
        }
        return defaultValue;
    }

    private static MessageDigest createMD5() {
        MessageDigest md = Exceptions.rethrow(() ->
            MessageDigest.getInstance("MD5")
        );
        return md;
    }

    private static MessageDigest createSHA256() {
        MessageDigest md = Exceptions.rethrow(() ->
            MessageDigest.getInstance("SHA-256")
        );
        return md;
    }

    private static String adjustSize(String value, int size) {
        StringBuilder sb = new StringBuilder(value);
        while (sb.length() < size) {
            sb.insert(0, '0');
        }
        return sb.toString();
    }

}
