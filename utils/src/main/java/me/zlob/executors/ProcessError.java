
package me.zlob.executors;

import lombok.*;

import me.zlob.messages.*;

import static me.zlob.util.Conditions.*;

/**
 * Collection of expected errors with codes and template messages.
 */
public enum ProcessError implements MessageProvider {

    NO_ERRORS(0, 0, ""),
    UNKNOWN_ERROR(1000, 0, "Unknown exception occurs on execution of command '%s'."),
    TIMEOUT_ERROR(1001, 2, "Timeout occurs on execution of command '%s' with time limit %s."),
    INTERRUPTED_ERROR(1002, 1, "Interrupt execution of command '%s'."),
    ABORTED_ERROR(1003, 1, "Abort execution of command '%s'."),
    IO_ERROR(1004, 1, "Exception on creating process for command '%s'.");

    @Getter
    private int code;

    @Getter
    private int argsCount;

    @Getter
    private String template;

    ProcessError(int code, int argsCount, String template) {
        this.code = code;
        this.argsCount = argsCount;
        this.template = checkNotNull(template, "template");
    }

}
