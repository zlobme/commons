
package me.zlob.executors;

import lombok.*;

import static me.zlob.executors.ProcessError.*;
import static me.zlob.util.Conditions.*;

/**
 * Result of external process execution.
 */
public class ProcessResult {

    @Getter
    private int code;

    @Getter
    private String output;

    @Getter
    private String error;

    @Getter
    private Exception exception;

    public static ProcessResult of(int code, String output, String error) {
        return new ProcessResult(code, output, error, null);
    }

    public static ProcessResult onError(ProcessError error, Exception ex, String message) {
        return new ProcessResult(error.getCode(), null, message, ex);
    }

    protected ProcessResult(int code, String output, String error, Exception exception) {
        this.code = code;
        this.output = checkOutput(code, output);
        this.error = checkError(code, error);
        this.exception = exception;
    }

    private static String checkOutput(int code, String output) {
        String message = "Output argument is null with no error state.";
        return checkArg(output, code == 0 && output == null, message);
    }

    private static String checkError(int code, String error) {
        String message = "Error argument is null with error state.";
        return checkArg(error, code != 0 && error == null, message);
    }

    public boolean hasError() {
        return this.code != NO_ERRORS.getCode();
    }

}
