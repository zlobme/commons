
package me.zlob.executors;

import java.io.*;
import java.time.*;
import java.util.concurrent.*;

import me.zlob.util.*;

import static me.zlob.executors.ProcessError.*;
import static me.zlob.util.Conditions.*;

/**
 * Executor of external processes with thread pool support.
 */
public class ProcessExecutor {

    private ExecutorService pool;

    public static ProcessExecutor of(ExecutorService executorService) {
        return new ProcessExecutor(executorService);
    }

    public static ProcessExecutor withSingleThread() {
        return new ProcessExecutor(Executors.newSingleThreadExecutor());
    }

    protected ProcessExecutor(ExecutorService executorService) {
        this.pool = checkNotNull(executorService, "executorService");
    }

    public ProcessResult exec(String command, String workingFolder, Duration timeout) {
        ProcessResult result;
        try {
            Future<ProcessResult> future = exec(command, workingFolder);
            result = future.get(timeout.toMillis(), TimeUnit.MILLISECONDS);
        } catch (TimeoutException ex) {
            String message = TIMEOUT_ERROR.getMessage(command, timeout.toMillis());
            result = ProcessResult.onError(TIMEOUT_ERROR, ex, message);
        } catch (InterruptedException ex) {
            String message = INTERRUPTED_ERROR.getMessage(command);
            result = ProcessResult.onError(INTERRUPTED_ERROR, ex, message);
        } catch (ExecutionException ex) {
            String message = ABORTED_ERROR.getMessage(command);
            result = ProcessResult.onError(ABORTED_ERROR, ex, message);
        }
        return result;
    }

    public Future<ProcessResult> exec(String command, String workingFolder) {
        return pool.submit(() -> run(command, workingFolder));
    }

    public ProcessResult run(String command, String workingFolder) {
        ProcessResult result;
        try {
            Process process = createProcess(command, workingFolder);
            try {
                int code = process.waitFor();
                String output = IO.read(process.getInputStream());
                String error = IO.read(process.getErrorStream());
                result = ProcessResult.of(code, output, error);
            } catch (InterruptedException ex) {
                String message = INTERRUPTED_ERROR.getMessage(command);
                result = ProcessResult.onError(INTERRUPTED_ERROR, ex, message);
            }
        } catch (IOException ex) {
            String message = IO_ERROR.getMessage(command);
            result = ProcessResult.onError(IO_ERROR, ex, message);
        }
        return result;
    }

    private Process createProcess(String command, String workingFolder) throws IOException {
        return new ProcessBuilder()
            .command("/bin/sh", "-c", command)
            .directory(new File(workingFolder))
            .start();
    }

}
