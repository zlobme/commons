
package me.zlob.collections;

import java.util.*;

import lombok.experimental.*;

import static me.zlob.util.Conditions.*;

public class DelegatedMap<Key, Value> implements Map<Key, Value> {

    @Delegate
    private Map<Key, Value> origin;

    public DelegatedMap() {
        this.origin = new HashMap<>();
    }

    public DelegatedMap(Map<Key, Value> origin) {
        this.origin = checkNotNull(origin, "origin");
    }

    @Override
    public boolean equals(Object o) {
        return origin.equals(o);
    }

    @Override
    public int hashCode() {
        return origin.hashCode();
    }

    @Override
    public String toString() {
        return origin.toString();
    }

}
