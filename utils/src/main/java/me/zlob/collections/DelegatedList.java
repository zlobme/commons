package me.zlob.collections;

import java.util.*;

import lombok.experimental.*;

import static me.zlob.util.Conditions.*;

public class DelegatedList<Value> implements List<Value> {

    @Delegate
    private List<Value> origin;

    public DelegatedList() {
        this(new ArrayList<>());
    }

    public DelegatedList(Collection<Value> origin) {
        this(new ArrayList<>(origin));
    }

    public DelegatedList(List<Value> origin) {
        this.origin = checkNotNull(origin, "origin");
    }

    @Override
    public boolean equals(Object o) {
        return origin.equals(o);
    }

    @Override
    public int hashCode() {
        return origin.hashCode();
    }

    @Override
    public String toString() {
        return origin.toString();
    }

}
