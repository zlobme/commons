
package me.zlob.collections;

import java.util.*;
import java.util.Map.*;
import java.util.function.*;
import java.util.stream.*;

import lombok.experimental.*;

/**
 * Utility methods for collections
 */
@UtilityClass
public class Containers {

    /**
     * Use to map value to it self
     *
     * <pre>{@code
     * .stream()
     * ...
     * .collect(Collectors.toMap(value-> value, Containers::self))
     * }</pre>
     */
    public static <Type> Type same(Type value) {
        return value;
    }

    /**
     * Use to resolve conflict by selecting first value
     *
     * <pre>{@code
     * .stream()
     * ...
     * .collect(Collectors.toMap(
     *      value-> value.toString(),
     *      value-> value,
     *      Containers::first))
     * }</pre>
     */
    public static <Type> Type first(Type first, Type ignore) {
        return first;
    }

    /**
     * Use to resolve conflict by selecting second value
     *
     * <pre>{@code
     * .stream()
     * ...
     * .collect(Collectors.toMap(
     *      value-> value.toString(),
     *      value-> value,
     *      Containers::second))
     * }</pre>
     */
    public static <Type> Type second(Type ignore, Type second) {
        return second;
    }

    /**
     * Shortcut for creating new empty {@link Set} in {@link Map} methods
     *
     * <pre>{@code
     * Map<Key,<Set<...>>> map = ...
     * map.computeIfAbsent(key, Containers::newSet)
     * }</pre>
     */
    public static <MK, V> Set<V> newSet(MK ignore) {
        return new HashSet<>();
    }

    /**
     * Shortcut for creating new empty {@link List} in {@link Map} methods
     *
     * <pre>{@code
     * Map<Key,<List<...>>> map = ...
     * map.computeIfAbsent(key, Containers::newList)
     * }</pre>
     */
    public static <MK, V> List<V> newList(MK ignore) {
        return new ArrayList<>();
    }

    /**
     * Shortcut for creating new empty {@link Map} in {@link Map} methods
     *
     * <pre>{@code
     * Map<Key,<Map<...>>> map = ...
     * map.computeIfAbsent(key, Containers::newMap)
     * }</pre>
     */
    public static <MK, K, V> Map<K, V> newMap(MK ignore) {
        return new HashMap<>();
    }

    /**
     * Shortcut for getting {@link Set} from {@link Map}
     *
     * <pre>{@code
     * Map<String, Set<Value>> multimap = ...
     * Set<Value> item = Containers.getSet(multimap, "foo");
     * }</pre>
     */
    public static <MK, V> Set<V> getSet(Map<MK, Set<V>> map, MK key) {
        return map.computeIfAbsent(key, Containers::newSet);
    }

    /**
     * Shortcut for getting {@link List} from {@link Map}
     *
     * <pre>{@code
     * Map<String, List<Value>> multimap = ...
     * List<Value> item = Containers.getList(multimap, "foo");
     * }</pre>
     */
    public static <MK, V> List<V> getList(Map<MK, List<V>> map, MK key) {
        return map.computeIfAbsent(key, Containers::newList);
    }

    /**
     * Shortcut for getting {@link Map} from {@link Map}
     *
     * <pre>{@code
     * Map<String, Map<Key, Value>> multimap = ...
     * Map<Key, Value> item = Containers.getMap(multimap, "foo");
     * }</pre>
     */
    public static <MK, K, V> Map<K, V> getMap(Map<MK, Map<K, V>> map, MK key) {
        return map.computeIfAbsent(key, Containers::newMap);
    }

    /**
     * Convert Set to new List
     */
    public static <Type> List<Type> toList(Set<Type> origin) {
        return new ArrayList<>(origin);
    }

    /**
     * Convert List to new Set
     */
    public static <Type> Set<Type> toSet(List<Type> origin) {
        return toSet(origin, true);
    }

    /**
     * Convert List to new Set with optional save of items order
     */
    public static <Type> Set<Type> toSet(List<Type> origin, boolean saveOrder) {
        return saveOrder ? new LinkedHashSet<>(origin) : new HashSet<>(origin);
    }

    /**
     * Construct new List with element in head of list
     */
    public static <Type> List<Type> concat(Type element, List<Type> list) {
        List<Type> result = new ArrayList<>(list.size() + 1);
        result.add(element);
        result.addAll(list);
        return result;
    }

    /**
     * Collector to remap entry set stream to Map
     *
     * <pre>{@code
     * Map<Key, Value> originMap = ...;
     * Map<Key, Value> newMap = originMap
     *     .entrySet().stream()
     *     ...
     *     .collect(toMap())
     * }</pre>
     */
    public static <K, V> Collector<Entry<K, V>, ?, Map<K, V>> toMap() {
        return Collectors.toMap(Entry::getKey, Entry::getValue, Containers::first);
    }

    /**
     * Helper to sort items of multi-list
     *
     * <pre>{@code
     * List<List<T>> originMultiList = ...;
     * List<List<T>> newMultiList = originMap
     *     .entrySet().stream()
     *     .map(sortBy((Comparator<T>) comparator))
     *     .collect(toList())
     * }</pre>
     */
    public static <T extends Comparable<T>>
    Function<List<T>, List<T>> sortBy(Comparator<T> comparator) {
        return list -> {
            list.sort(comparator);
            return list;
        };
    }

    /**
     * Group values ono to one by mapper
     */
    public static <K, V> Map<K, V> groupBy(List<V> values, Function<V, K> mapper) {
        return values.stream().collect(groupBy(mapper));
    }

    /**
     * Collector which group values ono to one by mapper
     */
    public static <K, V> Collector<V, ?, Map<K, V>> groupBy(Function<V, K> mapper) {
        return Collectors.toMap(mapper, Containers::same, Containers::first);
    }

}
