
package me.zlob.collections;

import java.util.*;

/**
 * Container of [type, id] pairs for retrieve entities from some data store.
 */
public class Batch<Id> {

    private Map<String, Set<Id>> typeIdsMap = new HashMap<>();

    public Batch add(Id id, Class clazz) {
        add(id, clazz.getSimpleName());
        return this;
    }

    public Batch add(Id id, String clazz) {
        Containers.getSet(typeIdsMap, clazz).add(id);
        return this;
    }

    public Set<String> getClasses() {
        return typeIdsMap.keySet();
    }

    public Set<Id> getIds(Class clazz) {
        return getIds(clazz.getSimpleName());
    }

    public Set<Id> getIds(String clazz) {
        return typeIdsMap.get(clazz);
    }

}
