
package me.zlob.util;

import java.time.*;
import java.util.*;

import org.junit.*;
import static org.junit.Assert.*;

import static me.zlob.util.Time.*;

public class TimeTest {

    @Test
    public void duration_equals() {
        assertTrue(Duration.ZERO == Duration.ofMillis(0));
        assertEquals(Duration.ZERO, Duration.ofMillis(0));
    }

    @Test
    public void set_time_correct_test_1() {
        String time = "12:34";
        Date date = setTime(new Date(), time);
        assertTrue(formatDateTime(date).startsWith(time));
    }

    @Test
    public void set_time_correct_test_2() {
        String time = "00:00";
        Date date = setTime(new Date(), time);
        assertTrue(formatDateTime(date).startsWith(time));
    }

    @Test
    public void set_time_correct_test_3() {
        String time = "23:59";
        Date date = setTime(new Date(), time);
        assertTrue(formatDateTime(date).startsWith(time));
    }

    @Test(expected = IllegalStateException.class)
    public void set_time_incorrect_test_1() {
        String time = "24:34";
        Date date = setTime(new Date(), time);
        fail("Illegal state exception expected");
    }

    @Test(expected = IllegalStateException.class)
    public void set_time_incorrect_test_2() {
        String time = "23:60";
        Date date = setTime(new Date(), time);
        fail("Illegal state exception expected");
    }

    @Test(expected = IllegalStateException.class)
    public void set_time_incorrect_test_3() {
        String time = "-2:-35";
        Date date = setTime(new Date(), time);
        fail("Illegal state exception expected");
    }

    @Test
    public void add_day_test() {
        assertEquals("02.01.2013", formatDate(addDay(parseDate("01.01.2013"), 1)));
        assertEquals("01.02.2013", formatDate(addDay(parseDate("31.01.2013"), 1)));
        assertEquals("01.01.2013", formatDate(addDay(parseDate("02.01.2013"), -1)));
        assertEquals("31.01.2013", formatDate(addDay(parseDate("01.02.2013"), -1)));
    }

    @Test
    public void get_day_of_week_test() {
        assertEquals(6, getDayOfWeek(parseDate("01.01.2000")));
        assertEquals(7, getDayOfWeek(parseDate("02.01.2000")));
        assertEquals(1, getDayOfWeek(parseDate("03.01.2000")));
        assertEquals(2, getDayOfWeek(parseDate("04.01.2000")));
        assertEquals(3, getDayOfWeek(parseDate("05.01.2000")));
        assertEquals(4, getDayOfWeek(parseDate("06.01.2000")));
        assertEquals(5, getDayOfWeek(parseDate("07.01.2000")));
        assertEquals(6, getDayOfWeek(parseDate("08.01.2000")));
    }

    @Test
    public void get_day_of_month_test() {
        assertEquals(1, getDayOfMonth(parseDate("01.01.2000")));
        assertEquals(2, getDayOfMonth(parseDate("02.01.2000")));
        assertEquals(3, getDayOfMonth(parseDate("03.01.2000")));
        assertEquals(4, getDayOfMonth(parseDate("04.01.2000")));
        assertEquals(5, getDayOfMonth(parseDate("05.01.2000")));
        assertEquals(6, getDayOfMonth(parseDate("06.01.2000")));
        assertEquals(7, getDayOfMonth(parseDate("07.01.2000")));
        assertEquals(8, getDayOfMonth(parseDate("08.01.2000")));
    }

    @Test
    public void get_tomorrow_test() {
        assertEquals("02.01.2013", formatDate(getTomorrow(parseDate("01.01.2013"))));
        assertEquals("01.02.2013", formatDate(getTomorrow(parseDate("31.01.2013"))));
    }

    @Test
    public void get_yesterday_test() {
        assertEquals("01.01.2013", formatDate(getYesterday(parseDate("02.01.2013"))));
        assertEquals("31.01.2013", formatDate(getYesterday(parseDate("01.02.2013"))));
    }

    @Test
    public void truncate_millis_test() {
        assertEquals(1496007532000L, truncateMillis(new Date(1496007532384L)).getTime());
    }

}
