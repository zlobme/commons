
package me.zlob.util;

import java.io.*;
import java.nio.charset.*;

import org.slf4j.*;

import org.junit.*;
import static org.junit.Assert.*;

import static me.zlob.util.Conditions.*;

public class IOTest {

    private static final Logger log = LoggerFactory.getLogger(IOTest.class);

    private final String tmpDir = System.getProperty("java.io.tmpdir");

    private final String exampleFile = IO.folder(tmpDir) + "test.data";

    private final Charset utfCharset = StandardCharsets.UTF_8;

    private final Charset nonUtfCharset = Charset.forName("cp1251");

    @After
    public void tearDown() {
        try {
            File file = new File(exampleFile);
            if (file.exists()) {
                checkState(!file.delete(),
                    "Temporary file doesn't deleted: %s",
                    file.getAbsolutePath());
            }
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
    }

    @Test
    public void write_read_test() {
        String data = "qwerty йцукен";
        IO.writeFile(exampleFile, data);
        String result = IO.readFile(exampleFile);
        assertEquals(data, result);
    }

    @Test
    public void double_write_read_test() {
        String firstData = "first write";
        IO.writeFile(exampleFile, firstData);

        String secondData = "second write";
        IO.writeFile(exampleFile, secondData);

        String result = IO.readFile(exampleFile);
        assertEquals(secondData, result);
    }

    @Test
    public void write_read_data_test() {
        String data = "qwerty йцукен";
        byte[] bytes = data.getBytes(utfCharset);
        IO.writeDataFile(exampleFile, bytes);

        byte[] buffer = IO.readDataFile(exampleFile);
        String result = new String(buffer, utfCharset);

        assertEquals(data, result);
    }

    @Test
    public void write_read_enc_test() {
        String data = "qwerty йцукен";
        IO.writeFile(exampleFile, data, nonUtfCharset);
        String result = IO.readFile(exampleFile, nonUtfCharset);
        assertEquals(data, result);
    }

    @Test
    public void write_read_different_enc_test() {
        String data = "qwerty йцукен";
        IO.writeFile(exampleFile, data, utfCharset);
        String result = IO.readFile(exampleFile, nonUtfCharset);
        assertNotSame(data, result);
    }

    @Test
    public void end_path_with_slash_test() {
        String data = "/tmp";
        String path = IO.slash(data, "/");
        assertEquals("/tmp/", path);
        assertEquals("/tmp/", IO.slash(path, "/"));
    }

    @Test
    public void empty_path_slash_unslash_test() {
        String data = "";
        assertEquals("", IO.slash(data));
        assertEquals("", IO.unslash(data));
    }

    @Test
    public void get_folder_from_unix_path_test() {
        String data = "/tmp/abc.dat";
        String path = IO.folder(data, "/");
        assertEquals("/tmp/", path);
        assertEquals("/tmp/", IO.slash(path, "/"));
    }

    @Test
    public void get_folder_from_win_path_test() {
        String data = "C:\\tmp\\abc.dat";
        String path = IO.folder(data, "\\");
        assertEquals("C:\\tmp\\", path);
        assertEquals("C:\\tmp\\", IO.slash(path, "\\"));
    }

    @Test
    public void get_folder_from_wrong_path_test() {
        String data = "C:\\tmp\\abc.dat";
        String path = IO.folder(data, "/");
        assertEquals("", path);
        assertEquals("", IO.slash(path, "/"));
    }

}
