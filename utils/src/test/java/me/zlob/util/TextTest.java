
package me.zlob.util;

import java.util.*;

import org.junit.*;
import static org.junit.Assert.*;

import static me.zlob.util.Text.*;

public class TextTest {

    @Test
    public void truncate_short_test() {
        String text = "12345678901234567890";
        assertEquals("12345678901234567890", truncate(text, 25));
    }

    @Test
    public void truncate_long_test() {
        String text = "12345678901234567890";
        assertEquals("123456789012345", truncate(text, 15));
    }

    @Test
    public void extract_letters_test() {
        String text = "1 two 3 four";
        assertEquals("two four", extractLetters(text));
    }

    @Test
    public void extract_digits_test() {
        String text = "1 two 3 four";
        assertEquals("1 3", extractDigits(text));
    }

    @Test
    public void clear_non_print_test() {
        String text = "now you\u000Bsee me...  \r\n now you don't ";
        assertEquals("now you see me... now you don't", clearNonPrint(text));
    }

    @Test
    public void clear_spaces_test() {
        String text = "now you see me...\r\nnow you don't";
        assertEquals("nowyouseeme...nowyoudon't", clearSpaces(text));
    }

    @Test
    public void clear_special_test() {
        String text = "now you_see [me]...now (you) don't";
        assertEquals("now you see me now you don t", clearSpecial(text));
    }

    @Test
    public void clear_letters_test() {
        String text = "1, two, 3, four.";
        assertEquals("1, , 3, .", clearLetters(text));
    }

    @Test
    public void clear_digits_test() {
        String text = "1, two, 3, four.";
        assertEquals(", two, , four.", clearDigits(text));
    }

    @Test
    public void clear_punctuation_test() {
        String text = "1, two, 3, four.";
        assertEquals("1 two 3 four", clearPunctuation(text));
    }

    @Test
    public void split_letters_digits_test() {
        String text = "1two3four";
        assertEquals("1 two 3 four", splitLettersAndDigits(text));
    }

    @Test
    public void split_upper_case_test() {
        String text = "CamelCaseNamingRules";
        assertEquals("Camel Case Naming Rules", splitUpperCase(text));
    }

    @Test
    public void capitalize_test() {
        String text = "camel Case naming rules";
        assertEquals("Camel Case naming rules", capitalize(text));
    }

    @Test
    public void punctuation_test() {
        String text = "bla, bla , bla ;bla;bla ; bla. bla 2.0";
        assertEquals("bla, bla, bla; bla; bla; bla. bla 2.0", fixPunctuation(text));
    }

    @Test
    public void words_split_test() {
        String text = "Camel Case Naming Rules";
        assertEquals(Arrays.asList("Camel", "Case", "Naming", "Rules"), wordsSplit(text));
    }

    @Test
    public void words_count_test() {
        String text = "Camel Case Naming Rules";
        assertEquals(4, wordsCount(text));
    }

    @Test
    public void words_similarity_test() {
        assertEquals(2, wordsSimilarity("Carl", "Clara"));
    }

    @Test
    public void words_contains_test() {
        String text = "Carl stole corals from Clara and Clara stole a clarinet from Carl";
        Set<String> words = new TreeSet<>(Arrays.asList("Carl", "Clara"));
        assertEquals(2, wordsContains(words, text));
    }

    @Test
    public void contains_cyrillic_test() {
        assertFalse(containsCyrillic("R"));
        assertTrue(containsCyrillic("Я"));
    }

    @Test
    public void transliterate_test() {
        String text = "тест транслитерации текста example 1234567890";
        assertEquals("test transliteracii teksta example 1234567890", transliterate(text, ' '));
    }

    @Test
    public void transliterate_delimiter_trim_test() {
        String text = "=тест транслитерации текста 1234567890 test =";
        assertEquals("test=transliteracii=teksta=1234567890=test", transliterate(text, '='));
    }

    @Test
    public void transliterate_for_url_test() {
        String text = "тест транслитерации текста  для использования в урл 1234567890 ";
        assertEquals("test-transliteracii-teksta-dlya-ispolzovaniya-v-url-1234567890", transliterate(text));
    }

}
