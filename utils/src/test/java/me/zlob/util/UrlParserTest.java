
package me.zlob.util;

import java.util.*;

import org.junit.*;
import static org.junit.Assert.*;

import me.zlob.util.*;

public class UrlParserTest {

    @Test
    public void parse_empty_url() {
        UrlParser urlParser = new UrlParser("");
        assertEquals("", urlParser.getProtocol());
        assertEquals("", urlParser.getDomain());
        assertEquals("", urlParser.getPort());
        assertEquals("/", urlParser.getPath());
    }

    @Test
    public void parse_relative_root_url() {
        UrlParser urlParser = new UrlParser("/");
        assertEquals("", urlParser.getProtocol());
        assertEquals("", urlParser.getDomain());
        assertEquals("", urlParser.getPort());
        assertEquals("/", urlParser.getPath());
    }

    @Test
    public void parse_protocol_domain_url() {
        UrlParser urlParser = new UrlParser("http://example.com/");

        assertEquals("http", urlParser.getProtocol());
        assertEquals("example.com", urlParser.getDomain());
        assertEquals("80", urlParser.getPort());
        assertEquals("/", urlParser.getPath());
    }

    @Test
    public void parse_protocol_domain_port_url() {
        UrlParser urlParser = new UrlParser("http://example.com:8080/");

        assertEquals("http", urlParser.getProtocol());
        assertEquals("example.com", urlParser.getDomain());
        assertEquals("8080", urlParser.getPort());
        assertEquals("/", urlParser.getPath());
    }

    @Test
    public void parse_domain_url() {
        UrlParser urlParser = new UrlParser("example.com");

        assertEquals("", urlParser.getProtocol());
        assertEquals("example.com", urlParser.getDomain());
        assertEquals("", urlParser.getPort());
        assertEquals("/", urlParser.getPath());
    }

    @Test
    public void parse_dynamic_protocol_url() {
        UrlParser urlParser = new UrlParser("//example.com");

        assertEquals("", urlParser.getProtocol());
        assertEquals("example.com", urlParser.getDomain());
        assertEquals("", urlParser.getPort());
        assertEquals("/", urlParser.getPath());
    }

    @Test
    public void parse_relative_param_query_url() {
        UrlParser urlParser = new UrlParser("/?test=value");
        assertEquals("", urlParser.getProtocol());
        assertEquals("", urlParser.getDomain());
        assertEquals("", urlParser.getPort());
        assertEquals("/", urlParser.getPath());

        Map<String, String> expectedParams = new HashMap<>();
        expectedParams.put("test", "value");

        assertEquals(expectedParams, urlParser.getParams());
    }

    @Test
    public void parse_absolute_param_query_url() {
        String url = "http://example.com/?param1=value1&param2=value2";
        UrlParser urlParser = new UrlParser(url);

        assertEquals("http", urlParser.getProtocol());
        assertEquals("example.com", urlParser.getDomain());
        assertEquals("80", urlParser.getPort());
        assertEquals("/", urlParser.getPath());

        Map<String, String> expectedParams = new HashMap<>();
        expectedParams.put("param1", "value1");
        expectedParams.put("param2", "value2");

        assertEquals(expectedParams, urlParser.getParams());
    }

    @Test
    public void parse_empty_params_url() {
        String url = "http://example.com/?param1=value1&param2&param3=";
        UrlParser urlParser = new UrlParser(url);

        assertEquals("http", urlParser.getProtocol());
        assertEquals("example.com", urlParser.getDomain());
        assertEquals("80", urlParser.getPort());
        assertEquals("/", urlParser.getPath());

        Map<String, String> expectedParams = new HashMap<>();
        expectedParams.put("param1", "value1");
        expectedParams.put("param2", "");
        expectedParams.put("param3", "");

        assertEquals(expectedParams, urlParser.getParams());
    }

    @Test
    public void parse_path_url() {
        UrlParser urlParser = new UrlParser("/some/action");

        assertEquals("", urlParser.getProtocol());
        assertEquals("", urlParser.getDomain());
        assertEquals("", urlParser.getPort());
        assertEquals("/some/action", urlParser.getPath());
    }

    @Test
    public void parse_encoded_params() {
        UrlParser urlParser = new UrlParser("/?test=%D0%B9%D1%86%D1%83%D0%BA%D0%B5%D0%BD");
        assertEquals("", urlParser.getProtocol());
        assertEquals("", urlParser.getDomain());
        assertEquals("", urlParser.getPort());
        assertEquals("/", urlParser.getPath());

        Map<String, String> expectedParams = new HashMap<>();
        expectedParams.put("test", "йцукен");

        assertEquals(expectedParams, urlParser.getParams());
    }

    @Test
    public void parse_jdbc_url() {
        String url = "jdbc:mysql://127.0.0.1/";
        UrlParser urlParser = new UrlParser(url);

        assertEquals("jdbc:mysql", urlParser.getProtocol());
        assertEquals("127.0.0.1", urlParser.getDomain());
        assertEquals("", urlParser.getPort());
        assertEquals("/", urlParser.getPath());
    }

}
