
package me.zlob.util;

import org.junit.*;
import static org.junit.Assert.*;

import me.zlob.util.*;

public class UrlTest {

    @Test
    public void build_empty_url() {
        Url url = new Url();
        assertEquals("/", url.get());
    }

    @Test
    public void set_context() {
        Url url = Url.empty()
            .context("context");
        assertEquals("/context", url.get());
    }

    @Test
    public void set_context_and_path() {
        Url url = Url.empty()
            .context("context")
            .path("path");
        assertEquals("/context/path", url.get());
    }

    @Test
    public void set_slashes_context_and_path() {
        Url url = Url.empty()
            .context("/")
            .path("/");
        assertEquals("/", url.get());
    }

    @Test
    public void set_context_and_path_slashed() {
        Url url = Url.empty()
            .context("/context/")
            .path("/path/");
        assertEquals("/context/path/", url.get());
    }

    @Test
    public void set_and_clear_param() {
        Url url = Url.empty()
            .set("test", "value");
        assertEquals("/?test=value", url.get());
        assertEquals("/", url.clear("test").get());
    }

    @Test
    public void set_i18n_param() {
        Url url = Url.empty()
            .set("test", "йцукен");
        assertEquals("/?test=%D0%B9%D1%86%D1%83%D0%BA%D0%B5%D0%BD", url.get());
    }

}
