
package me.zlob.util;

import org.junit.*;
import static org.junit.Assert.*;

public class ComparisonTest {

    private Long lowerValue = 1L;

    private Long higherValue = 2L;

    @Test
    public void is_greater() {
        assertTrue(Comparison.of(higherValue).with(lowerValue).isGreater());
    }

    @Test
    public void is_greater_or_equals() {
        assertTrue(Comparison.of(higherValue).with(higherValue).isGreaterOrEqual());
    }

    @Test
    public void is_less() {
        assertTrue(Comparison.of(lowerValue).with(higherValue).isLess());
    }

    @Test
    public void is_less_or_equals() {
        assertTrue(Comparison.of(lowerValue).with(higherValue).isLessOrEqual());
        assertTrue(Comparison.of(higherValue).with(higherValue).isLessOrEqual());
    }

    @Test
    public void is_equals() {
        assertTrue(Comparison.of(higherValue).with(higherValue).isEqual());
    }

    @Test
    public void is_not_equals() {
        assertTrue(Comparison.of(lowerValue).with(higherValue).isNotEqual());
    }

    @Test
    public void comparison_min() {
        assertEquals(lowerValue, Comparison.of(lowerValue).with(higherValue).min());
    }

    @Test
    public void comparison_max() {
        assertEquals(higherValue, Comparison.of(lowerValue).with(higherValue).max());
    }

    @Test
    public void get_min() {
        assertEquals(lowerValue, Comparison.min(lowerValue, higherValue));
    }

    @Test
    public void get_max() {
        assertEquals(higherValue, Comparison.max(lowerValue, higherValue));
    }

}
