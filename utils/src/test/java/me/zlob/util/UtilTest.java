
package me.zlob.util;

import java.io.*;
import java.util.*;

import org.junit.*;
import static org.junit.Assert.*;

import static me.zlob.util.Conditions.*;
import static me.zlob.util.Util.*;

public class UtilTest {

    @Test
    public void from_var_test() {
        var origin = new HashMap<String, List<String>>();
        Map<String, List<String>> inferred = origin;

        assertEquals(0, inferred.size());
    }

    @Test
    public void to_var_test() {
        Map<String, List<String>> origin = new HashMap<>();
        var inferred = origin;

        assertEquals(0, inferred.size());
    }

    @Test
    public void base64_test() {
        String origin = "qwerty йцукен !\"#;%:?*()";
        String encoded = "cXdlcnR5INC50YbRg9C60LXQvSAhIiM7JTo_Kigp";
        assertEquals(encoded, base64Encode(origin));
        assertEquals(origin, base64Decode(encoded));
    }

    @Test
    public void md5_test() {
        assertEquals("900150983cd24fb0d6963f7d28e17f72", md5("abc"));
        assertEquals("9817de3f4bd1d1d149fc366d46e5e134", md5("абв"));
        assertEquals("08f8e0260c64418510cefb2b06eee5cd", md5("bbb"));
    }

    @Test
    public void sha256_test() {
        assertEquals("ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad", sha256("abc"));
        assertEquals("39e305d37ff46062cc8bfc3ca667491e07d17cfa2e71404c641381fa1b8cc2ca", sha256("абв"));
    }

    @Test(expected = RuntimeException.class)
    public void parse_integer_use() {
        String number = null;
        nullable(parseInt(number)).orElseThrow(() -> {
            String message = "Illegal number format: '%s'";
            return new RuntimeException(String.format(message, number));
        });
    }

    @Test
    public void parse_empty_integer() {
        String number = "";
        assertNull(parseInt(number));
    }

    @Test
    public void parse_good_integer() {
        String number = "100500";
        assertEquals(Integer.valueOf(100500), parseInt(number));
    }

    @Test
    public void parse_bad_integer() {
        String number = "r2d2";
        assertNull(parseInt(number));
    }

    @Test
    public void parse_alter_integer() {
        String number = "r2d2";
        assertEquals(Integer.valueOf(42), parseInt(number, 42));
    }

    @Test
    public void print_formatted() {
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            PrintStream printStream = new PrintStream(baos);
            Util.printf(printStream, "Hello %s", "Bob");
            String result = new String(baos.toByteArray());
            assertEquals("Hello Bob\n", result);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

}
