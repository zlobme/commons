
package me.zlob.util;

import java.util.*;

import org.junit.*;
import static org.junit.Assert.*;

import static me.zlob.util.Conditions.*;

public class ConditionsTest {

    @Test(expected = NullPointerException.class)
    public void check_variable_not_null() {
        Object obj = checkNotNull(null, () -> "var must be not null");
        assertNull(obj);
    }

    @Test(expected = IllegalArgumentException.class)
    public void check_argument_condition() {
        Object param = null;
        Object obj = checkArg(param, param == null, "param must be not null");
        assertNull(obj);
    }

    @Test(expected = IllegalArgumentException.class)
    public void check_argument_function() {
        Object param = null;
        Object obj = checkArg(param, Conditions::isNull, () -> "param must be not null");
        assertNull(obj);
    }

    @Test(expected = IllegalArgumentException.class)
    public void check_argument_function_numeric() {
        Integer param = 1;
        Object obj = checkArg(param, p -> !(p > 1), () -> "param must be greater than 1");
        assertNotNull(obj);
    }

    @Test(expected = IllegalStateException.class)
    public void check_state_condition() {
        Object param = null;
        checkState(param == null, "param must be not null");
    }

    @Test(expected = IllegalStateException.class)
    public void check_state_function() {
        Object param = null;
        checkState(() -> param == null, () -> "param must be not null");
    }

    @Test
    public void exists_not_empty_string_test() {
        assertTrue(existsNotEmpty("abc"));
        assertFalse(existsNotEmpty(""));
        assertFalse(existsNotEmpty((String) null));
    }

    @Test
    public void exists_not_empty_number_test() {
        assertTrue(existsNotEmpty(1));
        assertFalse(existsNotEmpty(0));
        assertFalse(existsNotEmpty((Integer) null));
    }

    @Test
    public void exists_not_empty_map_test() {
        Map<String, String> map = new HashMap<>();
        map.put("abc", "xyz");
        assertTrue(existsNotEmpty(map));
        assertFalse(existsNotEmpty(Collections.EMPTY_MAP));
        assertFalse(existsNotEmpty((Map<String, String>) null));
    }

    @Test
    public void exists_not_empty_collection_test() {
        assertTrue(existsNotEmpty(Arrays.asList("abc")));
        assertFalse(existsNotEmpty(Collections.EMPTY_LIST));
        assertFalse(existsNotEmpty((List<?>) null));
    }

    @Test
    public void exists_not_empty_array_test() {
        assertTrue(existsNotEmpty(new Object[]{"hello"}));
        assertTrue(existsNotEmpty(new Object[1]));
        assertFalse(existsNotEmpty(new Object[0]));
        assertFalse(existsNotEmpty((Object[]) null));
    }

    @Test
    public void not_exists_or_empty_string_test() {
        assertFalse(notExistsOrEmpty("abc"));
        assertTrue(notExistsOrEmpty(""));
        assertTrue(notExistsOrEmpty((String) null));
    }

    @Test
    public void not_exists_or_empty_number_test() {
        assertFalse(notExistsOrEmpty(1));
        assertTrue(notExistsOrEmpty(0));
        assertTrue(notExistsOrEmpty((Integer) null));
    }

    @Test
    public void not_exists_or_empty_map_test() {
        Map<String, String> map = new HashMap<>();
        map.put("abc", "xyz");
        assertFalse(notExistsOrEmpty(map));
        assertTrue(notExistsOrEmpty(Collections.EMPTY_MAP));
        assertTrue(notExistsOrEmpty((Map<?, ?>) null));
    }

    @Test
    public void not_exists_or_empty_collection_test() {
        assertFalse(notExistsOrEmpty(Arrays.asList("abc")));
        assertTrue(notExistsOrEmpty(Collections.EMPTY_LIST));
        assertTrue(notExistsOrEmpty((List<?>) null));
    }

    @Test
    public void not_exists_or_empty_array_test() {
        assertFalse(notExistsOrEmpty(new Object[]{"hello"}));
        assertFalse(notExistsOrEmpty(new Object[1]));
        assertTrue(notExistsOrEmpty(new Object[0]));
        assertTrue(notExistsOrEmpty((Object[]) null));
    }

    @Test
    public void exists_and_equal_string_test() {
        assertTrue(existsAndEqual("abc", "abc"));
        assertTrue(existsAndEqual("", ""));
        assertFalse(existsAndEqual("abc", ""));
        assertFalse(existsAndEqual("abc", "123"));
        assertFalse(existsAndEqual("abc", null));
        assertFalse(existsAndEqual(null, "abc"));
    }

    @Test
    public void exists_and_equal_number_test() {
        assertTrue(existsAndEqual(1, 1));
        assertTrue(existsAndEqual(0, 0));
        assertFalse(existsAndEqual(1, 0));
        assertFalse(existsAndEqual(1, null));
        assertFalse(existsAndEqual(null, 1));
    }

    @Test
    public void exists_not_equal_string_test() {
        assertFalse(existsNotEqual("abc", "abc"));
        assertFalse(existsNotEqual("", ""));
        assertFalse(existsNotEqual(null, "abc"));
        assertTrue(existsNotEqual("abc", ""));
        assertTrue(existsNotEqual("abc", "123"));
        assertTrue(existsNotEqual("abc", null));
    }

    @Test
    public void exists_not_equal_number_test() {
        assertFalse(existsNotEqual(1, 1));
        assertFalse(existsNotEqual(0, 0));
        assertFalse(existsNotEqual(null, 1));
        assertTrue(existsNotEqual(1, 0));
        assertTrue(existsNotEqual(1, null));
    }

}
