
package me.zlob.util;

import java.net.*;

import org.slf4j.*;

import org.junit.*;

import static org.junit.Assert.*;

public class ExceptionsTest {

    private static final Logger log = LoggerFactory.getLogger("ignored");

    @Test
    public void ignore() {
        URL result = Exceptions.ignore(() -> new URL("oops..."));
        assertNull(result);
    }

    @Test
    public void log() {
        URL result = Exceptions.log(() -> new URL("oops..."), log);
        assertNull(result);
    }

    @Test(expected = RuntimeException.class)
    public void rethrow() throws RuntimeException {
        URL result = Exceptions.rethrow(() -> new URL("oops..."));
        fail("must crash before");
    }

    @Test
    public void execute() throws RuntimeException {
        String url = "oops...";
        URL result = Exceptions.execute(
            () -> new URL(url),
            (ex) -> log.error(String.format("parse url '%s' failed", url), ex)
        );
        assertNull(result);
    }

    @Test(expected = IllegalStateException.class)
    public void throw_custom_exception() throws IllegalStateException {
        String url = "oops...";
        URL result = Exceptions.rethrow(
            () -> new URL(url),
            IllegalStateException::new
        );
        assertNull(result);
    }

}
