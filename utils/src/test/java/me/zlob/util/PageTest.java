
package me.zlob.util;

import org.junit.*;
import static org.junit.Assert.*;

public class PageTest {

    @Test
    public void pages_count_test() {
        int page = 1;
        int onPage = 5;
        assertEquals(4, new Page(page, onPage).setCount(19).pages());
        assertEquals(4, new Page(page, onPage).setCount(20).pages());
        assertEquals(5, new Page(page, onPage).setCount(21).pages());
    }

    @Test
    public void zero_page_offest_test() {
        assertEquals(0, new Page(0, 5, 10).offset());
    }

    @Test
    public void zero_page_offest_test1() {
        assertEquals(10, new Page(3, 5).offset());
        assertEquals(0, new Page(3, 5).getCount());
    }

    @Test
    public void zero_count_zero_pages() {
        int page = 1;
        int onPage = 5;
        assertEquals(0, new Page(page, onPage).pages());
        assertEquals(0, new Page(page, onPage, 0).pages());
        assertEquals(0, new Page(page, onPage).setCount(0).pages());
    }

    @Test
    public void increase_page_test() {
        Page p = new Page(3, 5, 20);
        assertEquals(10, p.offset());
        assertEquals(15, p.next().offset());
    }

    @Test
    public void iterate_page_test() {
        Page p = new Page(0, 5, 20);
        int count = 0;
        while (p.next().getPage() <= p.pages()) {
            count++;
        }
        assertEquals(4, count);
    }

    @Test
    public void get_offset_big_page_test() {
        int page = 7;
        int onPage = 20;
        assertEquals(100, new Page(page, onPage).setCount(100).offset());
        assertEquals(120, new Page(page, onPage).setCount(120).offset());
        assertEquals(120, new Page(page, onPage).setCount(140).offset());
    }

    @Test
    public void get_limit_big_page_test() {
        int page = 7;
        int onPage = 20;
        assertEquals(120, new Page(page, onPage).setCount(120).limit());
        assertEquals(140, new Page(page, onPage).setCount(140).limit());
        assertEquals(140, new Page(page, onPage).setCount(160).limit());
    }

}
