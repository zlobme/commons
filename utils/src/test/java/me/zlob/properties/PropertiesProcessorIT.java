
package me.zlob.properties;

import java.util.*;

import org.junit.*;
import static org.junit.Assert.*;

import static me.zlob.properties.PropertiesProcessors.*;

public class PropertiesProcessorIT {

    private String resource = "me/zlob/properties/test-load.properties";

    private Properties presets;

    @Before
    public void init() {
        presets = new Properties();
        presets.put("a", "0");
        presets.put("b", false);
        presets.put("c", "3");
        presets.put("d", 1);
        presets.put("f", "4");
    }

    @Test
    public void load_filter_transform() {
        PropertiesProcessor loader = fromEmpty()
            .joinWith(fromPreset(presets)
                .andThen(fromClasspath(resource))
                .andThen(filterByKey(key -> !key.equals("d")))
                .andThen(transformKeys(key -> key.equals("f"), key -> "e"))
                .andThen(replaceKeyPrefix("", "abc:")))
            .joinWith(fromEnvironment()
                .andThen(filterByKey("zcom.redis"))
                .andThen(replaceKeyPrefix("zcom.redis", "redis")));

        Map<String, String> expected = new HashMap<>();
        expected.put("abc:a", "1");
        expected.put("abc:b", "2");
        expected.put("abc:c", "3");
        expected.put("abc:e", "4");

        assertEquals(expected, loader.loadAsMap());
    }

}
