
package me.zlob.messages;

import java.util.*;

import lombok.*;

import org.junit.*;
import static org.junit.Assert.*;

import static me.zlob.messages.LocalizedMessageProviderTest.TestMessages.*;

public class LocalizedMessageProviderTest {

    @Test
    public void get_message_with_default_locale() {
        assertEquals("Hello %s", SAY_HELLO.getTemplate());
        assertEquals("Hello Bob", SAY_HELLO.getMessage("Bob"));
    }

    @Test
    public void get_message_with_locale() {
        Locale locale = new Locale("en", "US");
        assertEquals("Hi %s", SAY_HELLO.getTemplate(locale));
        assertEquals("Hi Bob", SAY_HELLO.getMessage(locale, "Bob"));
    }

    @Test(expected = IllegalStateException.class)
    public void get_parametrized_message_with_no_args() {
        SAY_HELLO.getMessage();
        fail("Exception must be thrown");
    }

    @Test(expected = IllegalStateException.class)
    public void get_plain_message_with_args() {
        SAY_GOOD_BAY.getMessage("Bob");
        fail("Exception must be thrown");
    }

    enum TestMessages implements LocalizedMessageProvider {

        SAY_HELLO(1, "Hello %s"),
        SAY_GOOD_BAY(0, "Good bay");

        @Getter
        private int argsCount;

        TestMessages(int argsCount, String ignored) {
            this.argsCount = argsCount;
        }

        @Override
        public String getBundleName() {
            return "messages/greetings";
        }

    }

}
