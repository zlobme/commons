
package me.zlob.messages;

import java.util.*;

import static java.util.ResourceBundle.*;

import org.junit.*;
import static org.junit.Assert.*;

public class MessageBundleTest {

    private String bundleName = "messages/greetings";

    private String messageKey = "SAY_HELLO";

    @Test
    public void printLocaleMessages() {
        Map<Locale, String> locales = Map.of(
            Locale.ROOT, "Greetings %s",
            Locale.ENGLISH, "Hello %s",
            Locale.US, "Hi %s",
            new Locale("Zu"), "Hello %s");

        locales.forEach((locale, expected) -> {
            assertEquals(expected, getTemplate(bundleName, locale, messageKey));
        });
    }

    private String getTemplate(String name, Locale locale, String key) {
        return getBundle(name, locale).getString(key);
    }

}
