
package me.zlob.messages;

import lombok.*;

import org.junit.*;
import static org.junit.Assert.*;

public class MessageProviderTest {

    @Test
    public void get_message() {
        String template = TestMessages.NO_ARGS.getTemplate();
        String message = TestMessages.NO_ARGS.getMessage();
        assertEquals("just text", template);
        assertEquals("just text", message);
    }

    @Test
    public void get_message_with_args() {
        String template = TestMessages.ONE_ARG.getTemplate();
        String message = TestMessages.ONE_ARG.getMessage("bob");
        assertEquals("hello %s", template);
        assertEquals("hello bob", message);
    }

    @Test(expected = IllegalStateException.class)
    public void get_parametrized_message_with_no_args() {
        TestMessages.ONE_ARG.getMessage();
        fail("Exception must be thrown");
    }

    @Test(expected = IllegalStateException.class)
    public void get_plain_message_with_args() {
        TestMessages.NO_ARGS.getMessage("boo");
        fail("Exception must be thrown");
    }

    private enum TestMessages implements MessageProvider {

        NO_ARGS(0, "just text"),
        ONE_ARG(1, "hello %s");

        @Getter
        private int argsCount;

        @Getter
        private String template;

        TestMessages(int argsCount, String template) {
            this.argsCount = argsCount;
            this.template = template;
        }

    }

}
