package me.zlob.collections;

import java.math.*;
import java.util.*;

import org.junit.*;
import static org.junit.Assert.*;

public class DelegatedMapTest {

    private static final Currency usd = Currency.getInstance("USD");

    @Test
    public void creation_test() {
        Wallet wallet = new Wallet();
        wallet.put(usd, BigDecimal.ZERO);
        assertEquals(1, wallet.size());
    }

    class Wallet extends DelegatedMap<Currency, BigDecimal> {

    }

}
