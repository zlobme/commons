
package me.zlob.collections;

import java.util.ArrayList;
import java.util.*;
import java.util.stream.*;

import static java.util.Arrays.*;

import org.junit.*;

import static org.junit.Assert.*;

import me.zlob.functional.*;

public class ContainersTest {

    private static final List<Integer> data = asList(1, 2, 3, 4, 5);

    private static final Comparator<Integer> integerComparator;

    private static final Map<String, Integer> expected;

    static {
        integerComparator = Comparator.comparingInt(i -> i);

        expected = new LinkedHashMap<>();
        expected.put("1", 1);
        expected.put("2", 2);
        expected.put("3", 3);
        expected.put("4", 4);
        expected.put("5", 5);
    }

    @Test
    public void compute_new_list_in_multi_map() {
        Map<String, List<List<?>>> multimap = new HashMap<>();
        multimap.computeIfAbsent("foo", Containers::newList);
        assertNotNull(multimap.get("foo"));
    }

    @Test
    public void get_list_from_multi_map() {
        Map<String, List<Integer>> multimap = new HashMap<>();
        multimap.put("foo", asList(1, 2, 3));
        assertNotNull(Containers.getList(multimap, "foo"));
        assertEquals(3, Containers.getList(multimap, "foo").size());
    }

    @Test
    public void get_empty_list_from_multi_map() {
        Map<String, List<Integer>> multimap = new HashMap<>();
        assertNotNull(Containers.getList(multimap, "foo"));
    }

    @Test
    public void entries_to_map() {
        Map<String, Integer> result = data.stream()
            .map(i -> Tuple.of(String.valueOf(i), i))
            .collect(Containers.toMap());
        assertEquals(expected, result);
    }

    @Test
    public void sortby_collection() {
        List<List<Integer>> values = asList(asList(3, 2, 4, 1, 5));
        List<List<Integer>> result = values.stream()
            .map(Containers.sortBy(integerComparator))
            .collect(Collectors.toList());
        assertEquals(asList(1, 2, 3, 4, 5), result.get(0));
    }

    @Test
    public void group_collection() {
        List<Integer> values = new ArrayList<>(data);
        Map<String, Integer> result = Containers
            .groupBy(values, Objects::toString);
        assertEquals(expected, result);
    }

    @Test
    public void group_stream() {
        Map<String, Integer> result = data.stream()
            .collect(Containers.groupBy(Objects::toString));
        assertEquals(expected, result);
    }

    @Test
    public void concat_stream() {
        Map<String, Integer> result = data.stream()
            .collect(Containers.groupBy(Objects::toString));
        assertEquals(expected, result);
    }

}
