
package me.zlob.functional;

import java.util.stream.*;

import org.junit.*;
import static org.junit.Assert.*;

public class AccumulatorTest {

    @Test
    public void sum_value() {
        Accumulator<Integer> reducer = new Accumulator<>(0, Integer::sum);

        int result = Stream.of(1, 2, 3)
            .peek(reducer)
            .reduce(0, Integer::sum);

        assertTrue(reducer.get().equals(6));
        assertTrue(reducer.get().equals(result));
    }

    @Test
    public void min_value() {
        Accumulator<Integer> accumulator = new Accumulator<>(Integer.MAX_VALUE, Integer::min);

        long sum = Stream.of(2, 1, 2, 3, 4)
            .peek(accumulator)
            .reduce(0, Integer::sum);

        assertTrue(accumulator.get().equals(1));
    }

    @Test
    public void max_value() {
        Accumulator<Integer> accumulator = new Accumulator<>(Integer.MIN_VALUE, Integer::max);

        long sum = Stream.of(1, 2, 3, 4, 2)
            .peek(accumulator)
            .reduce(0, Integer::sum);

        assertTrue(accumulator.get().equals(4));
    }

}
