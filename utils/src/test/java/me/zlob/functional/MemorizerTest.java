package me.zlob.functional;

import java.util.concurrent.atomic.*;

import org.junit.*;

import static org.assertj.core.api.Assertions.*;

public class MemorizerTest {

    @Test
    public void foo1() {
        var counter = new AtomicInteger(0);
        var memorizer = Memorizer.of(counter::incrementAndGet);

        assertThat(memorizer.get()).isEqualTo(1);
        assertThat(memorizer.get()).isEqualTo(1);
        assertThat(counter.get()).isEqualTo(1);
    }

}
