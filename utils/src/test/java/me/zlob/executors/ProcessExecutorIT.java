
package me.zlob.executors;

import java.time.*;
import java.util.concurrent.*;

import org.slf4j.*;

import org.junit.*;

import static me.zlob.executors.ProcessError.*;
import static me.zlob.util.Conditions.*;

public class ProcessExecutorIT {

    private static final Logger log = LoggerFactory.getLogger(ProcessExecutorIT.class);

    @Test
    public void exec_current_thread() {
        ProcessExecutor executor = ProcessExecutor.withSingleThread();
        ProcessResult result = executor.run("pwd", "/usr");
        Assert.assertEquals(0L, (long) result.getCode());
        Assert.assertEquals("/usr", result.getOutput());
    }

    @Test
    public void exec_through_pool() {
        Duration timeout = Duration.ofMillis(1000L);
        ProcessExecutor executor = ProcessExecutor.withSingleThread();
        ProcessResult result = executor.exec("pwd", "/usr", timeout);
        Assert.assertEquals(0L, (long) result.getCode());
        Assert.assertEquals("/usr", result.getOutput());
    }

    @Test
    public void exec_through_pool_by_future() throws Exception {
        ProcessExecutor executor = ProcessExecutor.withSingleThread();
        Future<ProcessResult> future = executor.exec("pwd", "/usr");
        ProcessResult result = future.get(1000L, TimeUnit.MILLISECONDS);
        Assert.assertEquals(0L, (long) result.getCode());
        Assert.assertEquals("/usr", result.getOutput());
    }

    @Test
    public void exec_long_task_with_timeout_external() {
        ProcessExecutor executor = ProcessExecutor.withSingleThread();
        Future future = executor.exec("sleep 2", ".");
        try {
            future.get(100L, TimeUnit.MILLISECONDS);
            Assert.fail("Must throw timeout exception on long command execution");
        } catch (Exception ex) {
            Assert.assertTrue(ex instanceof TimeoutException);
        }
    }

    @Test
    public void exec_long_task_with_timeout_internal() {
        Duration timeout = Duration.ofMillis(100L);
        ProcessExecutor executor = ProcessExecutor.withSingleThread();
        ProcessResult result = executor.exec("sleep 2", ".", timeout);
        Assert.assertNull(result.getOutput());
        Assert.assertNotNull(result.getError());
        Assert.assertNotNull(result.getException());
        Assert.assertEquals(result.getCode(), TIMEOUT_ERROR.getCode());
    }

    @Test
    public void exec_with_output() {
        Duration timeout = Duration.ofMillis(100L);
        ProcessExecutor executor = ProcessExecutor.withSingleThread();
        ProcessResult result = executor.exec("echo ok", ".", timeout);
        Assert.assertFalse(result.hasError());
        Assert.assertEquals("ok", result.getOutput());
        Assert.assertTrue(existsAndEmpty(result.getError()));
        Assert.assertNull(result.getException());
    }

    @Test
    public void exec_with_shell_error() {
        Duration timeout = Duration.ofMillis(100L);
        ProcessExecutor executor = ProcessExecutor.withSingleThread();
        ProcessResult result = executor.exec("bla-bla-command", ".", timeout);
        Assert.assertTrue(result.hasError());
        Assert.assertTrue(result.getOutput() != null);
        Assert.assertTrue(existsNotEmpty(result.getError()));
        Assert.assertNull(result.getException());
    }

}
